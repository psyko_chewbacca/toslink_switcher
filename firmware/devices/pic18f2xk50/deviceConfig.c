/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "deviceConfig.h"
#if 0
#include "ioMapping.h"
#endif
#include "../../HAL/gpioWrapper.h"
#include "fuses.h"
#include "gpio.h"

void baseconfig(void)
{
    //Oscillator tuning config register
    //bit7 : PLL 3X multiplier selected
    OSCTUNEbits.SPLLMULT = 1;
    //bit6-0 : tuning value set to 0
    OSCTUNEbits.TUN = 0b0000000;
            
    //Oscillator config register
    //bit7 : Enter idle mode on SLEEP instruction
    OSCCONbits.IDLEN = 1;
    //bit 6-4 : Internal RC set to 16MHz
    OSCCONbits.IRCF = 0b111;
    //Device running from internal oscillator
    OSCCONbits.OSTS = 0;
    //HF OSC is stable(read only)
    //OSCCONbits.HFIOFS = 1;
    //Clock source determined by FOSC<3:0> in CONFIG1H
    OSCCONbits.SCS = 0b00;

    //OSCCON2 = 0b00000011;
    //bit7: PLL run status bit (read only)
    //OSCCON2bits.PLLRDY = 1;
    //bit6: Clk comes from secondary OSC (read only)
    //OSCCON2bits.SOSCRUN = 0;
    //bit5: INTRC used as the 31.25 kHz system clock reference ? low power
    OSCCON2bits.INTSRC = 0;
    //Software PLL Enable bit (FOSC is set to internal oscillator block, 0b1000)
    OSCCON2bits.PLLEN = 1;
    //bit3 : Secondary oscillator OFF
    OSCCON2bits.SOSCGO = 0;
    //bit2 : Primary Oscillator drive circuit off (zero power)
    OSCCON2bits.PRISD = 0;
    //bit1 : HFINTOSC Status bit (read only)
    //OSCCON2bits.HFIOFR = 0;
    //bit0 : INTRC Frequency Stable bit (read only)
    //OSCCON2bits.LFIOFS = 0;
    
    //while (OSCCON2bits.PLLRDY != 1);   

    //Active clock tuning register
    //bit7 : ACT disabled
    ACTCONbits.ACTEN = 0;
    //bit6 : OSCTUNE from ACT enabled
    ACTCONbits.ACTUD = 1;
    //bit4 : HFINTOSC is tuned to match USB host tolerance
    //Must be set when ACTEN is OFF
    ACTCONbits.ACTSRC = 1;
    //bit3 : ACT lock status bit (read only)
    //ACTCONbits.ACTLOCK = 0;
    //bit1 : ACT out of range status bit (read only)
    //ACTCONbits.ACTORS = 0;
    
    //Enabled ACT
    ACTCONbits.ACTEN = 1;
    
    //Assuming PLLSEL config bit is set to 3X and CPUDIV is set to NODIV,
    //the settings above should produce a 48MHz system clock from HFINTOSC
    //that is stable enough for USB operations


    //Voltage regulator register
    //bit1-0 : High power mode for now*/
#ifdef __XC8
    VREGCONbits.VREGPM = 0b00;
#else
    VREGCON = 0;
#endif

    //Peripheral module disable register
    //bit6 : UART disabled
    PMD0bits.UARTMD = 1;
    //bit5 : USB disabled
    PMD0bits.USBMD = 1;
    //bit4 : Active clock tuning enabled
    PMD0bits.ACTMD = 0;
    //bit2 : Timer3 enabled
    PMD0bits.TMR3MD = 0;
    //bit1 : Timer2 enabled
    PMD0bits.TMR2MD = 0;
    //bit0 : Timer1 enabled
    PMD0bits.TMR1MD = 0;

    //Peripheral module disable register
    //bit6 : MSSP disabled
    PMD1bits.MSSPMD = 1;
    //bit5 : CTMU disabled
    PMD1bits.CTMUMD = 1;
    //bit4 : Comparator2 disabled
    PMD1bits.CMP2MD = 1;
    //bit3 : Comparator1 disabled
    PMD1bits.CMP1MD = 1;
    //bit2 : ADC disabled
    PMD1bits.ADCMD = 1;
    //bit1 : CCP2 disabled
    PMD1bits.CCP2MD = 1;
    //bit0 : CCP1 disabled
    PMD1bits.CCP1MD = 1;

    //Reset control register
    //bit7 : Interrupt priority diabled
    RCONbits.IPEN = 0;
    //bit6 : Brown-Out reset disabled
    RCONbits.SBOREN = 0;
    //bit4 : Reset instruction flag(active low)
    RCONbits.RI = 1;
    //bit3 : WatchDog timeout flag (active low)
    RCONbits.TO = 1;
    //bit2 : Power Down detection flash (active low)
    RCONbits.PD = 1;
    //bit1 : Power-On reset status flag (active low)
    RCONbits.POR = 1;
    //bit0 : Brown-out Reset status flag (active low)
    RCONbits.BOR = 1;


    //WatchDog timer control register
    //bit0 : Watchdog timer disbled
    WDTCONbits.SWDTEN = 0;
}

void timerconfig(void)
{
    //Timer 0 configuration register
    //bit7 : Enable timer
    T0CONbits.TMR0ON = 1;
     //bit6 : 16 bit timer
    T0CONbits.T08BIT = 0;
    //bit5 : internal instruction clock source (Fosc/4)
    T0CONbits.T0CS = 0;
    //bit4 : Don't care, count on high-to-low transitions
    T0CONbits.T0SE = 1;
    //bit3 : Prescaler used
    T0CONbits.PSA = 0;
    //bit2-0 : prescaler set to 1:8
    T0CONbits.T0PS = 0b010;
    /*With a Fosc of 48MHz, period of 20.83ns
     * 20.83ns * 4(Fosc/4) * 8(1:8) * 65536(16 bits) = 
     * Timer0 will overflow every ~44ms*/
    
    
    /*Just to make sure the timer register is initialised*/
    /*Write to TMR0H must occur before write to TMR0L*/
    TMR0H = 0b00000000;
    TMR0L = 0b00000000;

    //Timer1 configuration register
    //bit7-6 : Fosc used for counting
    T1CONbits.TMR1CS = 0b00;
    //bit5-4 : 1:2 Prescale
    T1CONbits.T1CKPS = 0b10;
    //bit3 : secondary oscillator circuit disabled
    T1CONbits.SOSCEN = 0;
    //bit2 : Do not sync external clock input (active low)
    T1CONbits.T1SYNC = 1;
    //bit1 : 8 bit operation on timer1 registers
    T1CONbits.RD16 = 0;
    //bit0 : timer1 OFF
    T1CONbits.TMR1ON = 0;

    //Timer1 gate control register
    //bit7 : Timer1 counts controlled by Timer1 gate function
    T1GCONbits.TMR1GE= 1;
    //bit6 : Timer1 gate is active-low(don't care)
    T1GCONbits.T1GPOL= 0;
    //bit5 : Timer1 Gate Toggle mode is disabled(don't care)
    T1GCONbits.T1GTM= 0;
    //bit4 : Timer1 gate Single-Pulse mode is disabled(don't care)
    T1GCONbits.T1GSPM= 0;
    //bit3 : Timer1 gate single-pulse acquisition has not been started(don't care)
    T1GCONbits.T1GGO_NOT_T1DONE= 0;
    //bit2 : Read only(don't care)
    T1GCONbits.T1GVAL= 0;
    //bit1-0 : Timer1 pin used as gate source(don't care)
    T1GCONbits.T1GSS= 0b00;

    /*Just to make sure the timer register is initialised*/
    TMR1H = 0b00000000;
    TMR1L = 0b00000000;

    //Timer3 configuration register
    //bit7-6 : Fosc used for counting
    T3CONbits.TMR3CS = 0b00;
    //bit5-4 : 1:1 Prescale
    T3CONbits.T3CKPS = 0b00;
    //bit3 : secondary oscillator circuit disabled
    T3CONbits.SOSCEN = 0;
    //bit2 : Do not sync external clock input (active low))
    T3CONbits.T3SYNC = 1;
    //bit1 : 8 bit operation on timer1 registers
    T3CONbits.RD16 = 0;
    //bit0 : timer3 OFF
    T3CONbits.TMR3ON = 0;

    //Timer3 gate control register
    //bit7 : Timer3 counts controlled by Timer3 gate function
    T3GCONbits.TMR3GE = 1;
    //bit6 : Timer3 gate is active-low(don't care)
    T3GCONbits.T3GPOL = 0;
    //bit5 : Timer3 Gate Toggle mode is disabled(don't care)
    T3GCONbits.T3GTM = 0;
    //bit4 : Timer3 gate Single-Pulse mode is disabled(don't care)
    T3GCONbits.T3GSPM = 0;
    //bit3 : Timer3 gate single-pulse acquisition has not been started(don't care)
    T3GCONbits.T3GGO_NOT_T3DONE = 0;
    //bit2 : Read only(don't care)
    T3GCONbits.T3GVAL = 0;
    //bit1-0 : Timer3 pin used as gate source(don't care)
    T3GCONbits.T3GSS = 0b00;

    /*Just to make sure the timer register is initialised*/
    TMR3H = 0b00000000;
    TMR3L = 0b00000000;
}

void interruptconfig(void)
{
     //Interrupts configuration register
     //bit7 : Enables all unmasked interrupts
    INTCONbits.GIE_GIEH = 1;
     //bit6 : Enable peripheral interrupts
    INTCONbits.PEIE_GIEL = 1;
     //bit5 : Timer0 overflow interrupt enabled
    INTCONbits.TMR0IE = 1;
     //bit4 : INT0 interrupt disabled
    INTCONbits.INT0IE = 0;
     //bit3 : Interrupt-on-change disabled
    INTCONbits.IOCIE = 0;
     //bit2 : Timer0 overflow flag cleared
    INTCONbits.TMR0IF = 0;
     //bit1 : INT0 interrupt flag cleared(why not)
    INTCONbits.INT0IF = 0;
     //bit0 : Interrupt-on-change bit cleared(IOCIF)
    INTCONbits.IOCIF = 0;


     //Interrupts configuration register 2
     //bit7 : PORTB pull-ups enabled (active low)
    INTCON2bits.RBPU = 0;
     //bit6 : INT0 on falling edge(not used)
    INTCON2bits.INTEDG0 = 0;
     //bit5 : INT1 on falling edge(not used)
    INTCON2bits.INTEDG1 = 0;
     //bit4 : INT2 on falling edge(not used)
    INTCON2bits.INTEDG2 = 0;
     //bit3 : Unimplemented
     //bit2 : Timer0 overflow high priority
    INTCON2bits.TMR0IP = 1;
     //bit1 : Unimplemented
     //bit0 : Port Change Interrupt low priority
    INTCON2bits.IOCIP = 0;
    

    /*Interrupts configuration register 3
     all bits are 0 in this register
     This is only for configuration and usage of INT2 and INT3
    */
    INTCON3bits.INT2IP = 0;
    INTCON3bits.INT1IP = 0;
    INTCON3bits.INT2IE = 0;
    INTCON3bits.INT1IE = 0;
    INTCON3bits.INT2IF = 0;
    INTCON3bits.INT1IF = 0;


    /*Peripheral interrupt flag register 1
     all bits are 0 in this register.
     bit0 : timer1 overflow interrupt flag cleared
     */
    PIR1bits.ACTIF = 0;
    PIR1bits.ADIF = 0;
    PIR1bits.RCIF = 0;
    PIR1bits.TXIF = 0;
    PIR1bits.SSPIF = 0;
    PIR1bits.CCP1IF = 0;
    PIR1bits.TMR2IF = 0;
    PIR1bits.TMR1IF = 0;


    /*Peripheral interrupt flag register 2
     all bits are 0 in this register
     */
    PIR2bits.OSCFIF = 0;
    PIR2bits.C1IF = 0;
    PIR2bits.C2IF = 0;
    PIR2bits.EEIF = 0;
    PIR2bits.BCLIF = 0;
    PIR2bits.HLVDIF = 0;
    PIR2bits.TMR3IF = 0;
    PIR2bits.CCP2IF = 0;


    /*Peripheral interrupt flag register 3
     all bits are 0 in this register
     */
    PIR3bits.CTMUIF = 0;
    PIR3bits.USBIF = 0;
    PIR3bits.TMR3GIF = 0;
    PIR3bits.TMR1GIF = 0;

    /*Peripheral interrupt enable register 1
     all bits are 0 in this register except bit0 to enable timer1 overflow
     interrupt.
     */
    PIE1bits.ACTIE = 0;
    PIE1bits.ADIE = 0;
    PIE1bits.RCIE = 0;
    PIE1bits.TXIE = 0;
    PIE1bits.SSPIE = 0;
    PIE1bits.CCP1IE = 0;
    PIE1bits.TMR2IE = 0;
    PIE1bits.TMR1IE = 1;


    /*Peripheral interrupt enable register 2
     all bits are 0 in this register.
     */
    PIE2bits.OSCFIE = 0;
    PIE2bits.C1IE = 0;
    PIE2bits.C2IE = 0;
    PIE2bits.EEIE = 0;
    PIE2bits.BCLIE = 0;
    PIE2bits.HLVDIE = 0;
    PIE2bits.TMR3IE = 0;
    PIE2bits.CCP2IE = 0;


    /*Peripheral interrupt enable register 3
     all bits are 0 in this register
     */
    PIE3bits.CTMUIE = 0;
    PIE3bits.USBIE = 0;
    PIE3bits.TMR3GIE = 0;
    PIE3bits.TMR1GIE = 0;

    /*Peripheral interrupt priority register 1
     all bits are 0 in this register except bit0 for timer1 in high priority
     interrupt.
     */
    IPR1bits.ACTIP = 0;
    IPR1bits.ADIP = 0;
    IPR1bits.RCIP = 0;
    IPR1bits.TXIP = 0;
    IPR1bits.SSPIP = 0;
    IPR1bits.CCP1IP = 0;
    IPR1bits.TMR2IP = 0;
    IPR1bits.TMR1IP = 1;


    /*Peripheral interrupt priority register 2
     all bits are 0 in this register
     */
    IPR2bits.OSCFIP = 0;
    IPR2bits.C1IP = 0;
    IPR2bits.C2IP = 0;
    IPR2bits.EEIP = 0;
    IPR2bits.BCLIP = 0;
    IPR2bits.HLVDIP = 0;
    IPR2bits.TMR3IP = 0;
    IPR2bits.CCP2IP = 0;


    /*Peripheral interrupt priority register 3
     all bits are 0 in this register
     */
    IPR3bits.CTMUIP = 0;
    IPR3bits.USBIP = 0;
    IPR3bits.TMR3GIP = 0;
    IPR3bits.TMR1GIP = 0;
    

    /*Interrupt-on-change enable register
     All disabled as ch0 is on portC
     */
    IOCBbits.IOCB7 = 0;
    IOCBbits.IOCB6 = 0;
    IOCBbits.IOCB5 = 0;
    IOCBbits.IOCB4 = 0;
    

    /*Interrupt-on-change enable register
     All disabled except ch0(portC2) because it is default channel.
     */
    IOCCbits.IOCC7 = 0;
    IOCCbits.IOCC6 = 0;
    IOCCbits.IOCC5 = 0;
    IOCCbits.IOCC4 = 0;
    IOCCbits.IOCC2 = 0;
    IOCCbits.IOCC1 = 0;
}

void IOconfig(void)
{
    /*Port direction registers
     1 = input
     0 = output
     */
    //Set everything to input
    TRISA = 0xFF;
	TRISB = 0xFF;
	TRISC = 0xFF;
    
    //Manually define output GPIOs
    _ch3ledTris = 0;
    _ch2ledTris = 0;
    _ch1ledTris = 0;
    _ch0ledTris = 0;
    
    _a0Tris = 0;
    _a1Tris = 0;
    _muxEnableTris = 0;
    _muxActiveChannelLEDOnTris = 0;
    
    _slave_rstTris = 0;
    _slave_PWMTris = 0;
    
    _i2c_sclTris = 0;
    _i2c_sdaTris = 0;
    
    _uart_rxTris = 0;
    _uart_txTris = 0;
    
    _autoledTris = 0;
    
    WPUB = 0xFF;

#if 0
    //Set outputs
    *LedsOutputs[ch0led]->tristate &= ~(1U << LedsOutputs[ch0led]->pinNumber);
    *LedsOutputs[ch1led]->tristate &= ~(1U << LedsOutputs[ch1led]->pinNumber);
    *LedsOutputs[ch2led]->tristate &= ~(1U << LedsOutputs[ch2led]->pinNumber);
    *LedsOutputs[ch3led]->tristate &= ~(1U << LedsOutputs[ch3led]->pinNumber);
    *LedsOutputs[autoled]->tristate &= ~(1U << LedsOutputs[autoled]->pinNumber);
    
    *muxControlOutputs[a0]->tristate &= ~(1U << LedsOutputs[a0]->pinNumber);
    *muxControlOutputs[a1]->tristate &= ~(1U << LedsOutputs[a1]->pinNumber);
    *muxControlOutputs[muxEnable]->tristate &= ~(1U << LedsOutputs[muxEnable]->pinNumber);
    *muxControlOutputs[muxActiveChannelLEDOn]->tristate &= ~(1U << LedsOutputs[muxActiveChannelLEDOn]->pinNumber);
#endif
    
    
#if 0
    //Set inputs
    *chInputs[ch0Input]->tristate |= 1U << chInputs[ch0Input]->pinNumber;
    *chInputs[ch1Input]->tristate |= 1U << chInputs[ch1Input]->pinNumber;
    *chInputs[ch2Input]->tristate |= 1U << chInputs[ch2Input]->pinNumber;
    *chInputs[ch3Input]->tristate |= 1U << chInputs[ch3Input]->pinNumber;
    
    *switchInputs[chswitch]->tristate |= 1U << switchInputs[chswitch]->pinNumber;
    *switchInputs[automan]->tristate |= 1U << switchInputs[automan]->pinNumber;
    *switchInputs[funcSwitch]->tristate |= 1U << switchInputs[funcSwitch]->pinNumber;
#endif

    /*PortA analog select register
     all set to 0 since no analog function used in this design
     */
    ANSELA = 0b00000000;


    /*PortB analog select register
     all set to 0 since no analog function used in this design
     */
    ANSELB = 0b00000000;


    /*PortC analog select register
     all set to 0 since no analog function used in this design
     */
    ANSELC = 0b00000000;


    /*All outputs set to '0' at start*/
    /*Will be overriden soon enough*/
    LATA = 0;
    LATB = 0;
    LATC = 0;
    _muxActiveChannelLEDOn = 1;

}

void PeripheralConfig(void)
{
    //Data EEPROM Control Register 1
    //bit7: Access Data EEPROM memory
    EECON1bits.EEPGD = 0;
    //bit6: Access Flash program/dataEEPROM memory
    EECON1bits.CFGS = 0;
    //bit5: Unimplemented
    //bit4: Erase before write
    EECON1bits.FREE = 1;
    //bit3: WriteError bit clear
    EECON1bits.WRERR = 0;
    //bit2: Disable EEPROM write for now
    EECON1bits.WREN = 0;
    //bit1: Do not initiate write cycle
    EECON1bits.WR = 0;
    //bit0: Do not initiate read cycle*/
    EECON1bits.RD = 0;
}

void CommConfig(void)
{
    SSP1CON1bits.SSPEN = 0;  //Disable SSP1(I2C)
    SSP1STAT = 0x00;
    
    SSP1CON1bits.SSPM = 0b1000; //I2C Master mode, FOSC / (4 * (SSPxADD+1))
    
    SSP1CON2bits.GCEN = 0;  //Only for slave mode
    SSP1CON2bits.ACKDT = 0; //Ack bit is logic low
    
    SSP1CON3bits.PCIE = 0; //No interrupt on stop condition
    SSP1CON3bits.SCIE = 0; //No interrupt on start condition
    SSP1CON3bits.SDAHT = 0; //100ns hold time on SDA after falling edge of SCL
    
    //I2C clock period. We want 400KHz.
    //Formula is Fclk = Fosc/((SSP1ADD + 1) * 4)
    //SSP1ADD = Fosc/(Fclk * 4) - 1
    //Fosc is 48MHz
    //Fclk is 400KHz
    //Fosc is then 29
    SSP1ADD = 29;
    
    
    SSP1CON1bits.SSPEN = 1;  //Enable SSP1(I2C)
}

void clearWatchdogTimer(void)
{
    ClrWdt();
}

void softReset(void)
{
    Reset();
}