/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#ifndef DEVICECONFIG_H
#define	DEVICECONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#ifdef __XC8
#include <xc.h>
#else
#include <pic18fregs.h>
#endif

void baseconfig(void);
void timerconfig(void);
void interruptconfig(void);
void IOconfig(void);
void PeripheralConfig(void);
void CommConfig(void);
void clearWatchdogTimer(void);
void softReset(void);

#ifdef	__cplusplus
}
#endif

#endif	/* DEVICECONFIG_H */

