/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "gpio.h"
#include "ioMapping.h"
#include "../../HAL/gpioWrapper.h"
#include "../../HAL/gpioGroups.h"

#if 0
#ifdef BOARD_REV1
/*Green leds port define*/
    const GPIOMapping ch3ledMapping = { &TRISA, &LATA, _PIN0_POS };
    const GPIOMapping ch2ledMapping = { &TRISA, &LATA, _PIN1_POS };
    const GPIOMapping ch1ledMapping = { &TRISA, &LATA, _PIN2_POS };
    const GPIOMapping ch0ledMapping = { &TRISA, &LATA, _PIN3_POS };
    const GPIOMapping autoledMapping = { &TRISB, &LATB, _PIN2_POS };
    
/*Mux-Demux control port define*/
    volatile unsigned char dummyWrite;
    const GPIOMapping a0Mapping = { &TRISB, &LATB, _PIN6_POS };
    const GPIOMapping a1Mapping = { &TRISB, &LATB, _PIN7_POS };
    const GPIOMapping muxEnableMapping = { &dummyWrite, &dummyWrite, 0 };
    const GPIOMapping muxActiveChannelLEDOnMapping = { &dummyWrite, &dummyWrite, 0 };

/*EXP-IO port define (used as debug for now)*/
    const GPIOMapping debug1Mapping = { &TRISC, &LATC, _PIN6_POS };
    const GPIOMapping debug2Mapping = { &TRISC, &LATC, _PIN7_POS };
    const GPIOMapping debug3Mapping = { &TRISB, &LATB, _PIN0_POS };
    const GPIOMapping debug4Mapping = { &TRISB, &LATB, _PIN1_POS };
    const GPIOMapping debug5Mapping = { &TRISB, &LATB, _PIN3_POS };
    const GPIOMapping debug6Mapping = { &TRISA, &LATA, _PIN5_POS };

/*Channel input port define*/
    const GPIOMapping ch0Mapping = { &TRISC, &PORTC, _PIN2_POS };
    const GPIOMapping ch1Mapping = { &TRISC, &PORTC, _PIN1_POS };
    const GPIOMapping ch2Mapping = { &TRISB, &PORTB, _PIN5_POS };
    const GPIOMapping ch3Mapping = { &TRISB, &PORTB, _PIN4_POS };

/*Pushbutton port define*/
    volatile unsigned char dummyRead = 1;
    const GPIOMapping chswitchMapping = { &TRISC, &PORTC, _PIN0_POS };
    const GPIOMapping automanMapping = { &TRISA, &PORTA, _PIN4_POS };
    const GPIOMapping funcSwitchMapping = { &dummyRead, &dummyRead, 0 };



#elif BOARD_REV2
/*Green leds port define*/
    const GPIOMapping ch3ledMapping = { &TRISC, &LATC, _PIN0_POS };
    const GPIOMapping ch2ledMapping = { &TRISA, &LATA, _PIN6_POS };
    const GPIOMapping ch1ledMapping = { &TRISA, &LATA, _PIN7_POS };
    const GPIOMapping ch0ledMapping = { &TRISA, &LATA, _PIN4_POS };
#define USE_PWM1
    const GPIOMapping autoledMapping = { &TRISC, &LATC, _PIN2_POS };  //PWM1

/*Mux-Demux control port define*/
    const GPIOMapping a0Mapping = { &TRISB, &LATB, _PIN7_POS };
    const GPIOMapping a1Mapping = { &TRISB, &LATB, _PIN6_POS };
    const GPIOMapping muxEnableMapping = { &TRISA, &LATA, _PIN0_POS };
    #define USE_PWM2
    const GPIOMapping muxActiveChannelLEDOnMapping = { &TRISC, &LATC, _PIN1_POS };  //PWM2

/*EXP-IO port define (used as debug for now)*/
    const GPIOMapping debug1Mapping = { &TRISC, &LATC, _PIN6_POS };
    const GPIOMapping debug2Mapping = { &TRISC, &LATC, _PIN7_POS };
    const GPIOMapping debug3Mapping = { &TRISB, &LATB, _PIN0_POS };
    const GPIOMapping debug4Mapping = { &TRISB, &LATB, _PIN1_POS };
    const GPIOMapping debug5Mapping = { &TRISB, &LATB, _PIN3_POS };
    const GPIOMapping debug6Mapping = { &TRISA, &LATA, _PIN5_POS };

/*Channel input port define*/
    const GPIOMapping ch0Mapping = { &TRISB, &PORTB, _PIN2_POS };
    const GPIOMapping ch1Mapping = { &TRISA, &PORTA, _PIN3_POS };
    const GPIOMapping ch2Mapping = { &TRISA, &PORTA, _PIN2_POS };
    const GPIOMapping ch3Mapping = { &TRISA, &PORTA, _PIN1_POS };

/*Pushbutton port define*/
    volatile unsigned char dummyTrisE;
    const GPIOMapping chswitchMapping = { &TRISB, &LATB, _PIN4_POS };
    const GPIOMapping automanMapping = { &TRISB, &PORTB, _PIN5_POS };
    const GPIOMapping funcSwitchMapping = { &dummyTrisE, &PORTE, _PIN3_POS };

#else
#error Specify a board revision to build proper firmware.
#endif
    
const GPIOMapping * LedsOutputs[]= 
{
    &ch0ledMapping, 
    &ch1ledMapping, 
    &ch2ledMapping, 
    &ch3ledMapping,
    &autoledMapping
};
const GPIOMapping * muxControlOutputs[] = 
{ 
    &a0Mapping, 
    &a1Mapping, 
    &muxEnableMapping, 
    &muxActiveChannelLEDOnMapping 
};

const GPIOMapping * chInputs[] =
{
    &ch0Mapping,
    &ch1Mapping,
    &ch2Mapping,
    &ch3Mapping
};

const GPIOMapping * switchInputs[] =
{
    &chswitchMapping,
    &automanMapping,
    &funcSwitchMapping
};
#endif

void setPinState(unsigned char GPIOGroupsEnum, unsigned char pinEnum, bool state)
{
#if 0
    const GPIOMapping** gpioGroupArray[] = 
    {
        LedsOutputs,
        muxControlOutputs,
        chInputs,
        switchInputs
    };
    
    unsigned char portCopy;
    unsigned char pinNumber;
    
    portCopy = *gpioGroupArray[GPIOGroupsEnum][pinEnum]->port;
    pinNumber = gpioGroupArray[GPIOGroupsEnum][pinEnum]->pinNumber;
    
    if(state == true)
    {
        portCopy |= (1U << pinNumber);
    }
    else
    {
        portCopy &= ~(1U << pinNumber);
    }
    
    *gpioGroupArray[GPIOGroupsEnum][pinEnum]->port = portCopy
#endif
    switch(GPIOGroupsEnum)
    {
        case LEDSGpioGroup:
        {
            switch(pinEnum)
            {
                case ch0led:
                    _ch0led = state;
                    break;
                case ch1led:
                    _ch1led = state;
                    break;
                case ch2led:
                    _ch2led = state;
                    break;
                case ch3led:
                    _ch3led = state;
                    break;
                case autoled:
                    _autoled = state;
                    break;
                default:
                    break;
            }
        }
        break;
        case MUXGpioGroup:
        {
            switch(pinEnum)
            {
                case a0:
                    _a0 = state;
                    break;
                case a1:
                    _a1 = state;
                    break;
                case muxEnable:
                    _muxEnable = state;
                    break;
                case muxActiveChannelLEDOn:
                    _muxActiveChannelLEDOn = state;
                    break;
                default:
                    break;
            }
        }
        break;
        case SlaveControlGPIOGroup:
        {
            switch(pinEnum)
            {
                case slaveReset:
                default:
                    _slave_rst = state;
                    break;
            }
        }
        break;
        default:
            break;
    }
}

bool getPinState(unsigned char GPIOGroupsEnum, unsigned char pinEnum)
{
#if 0
    const GPIOMapping** gpioGroupArray[] = 
    {
        LedsOutputs,
        muxControlOutputs,
        chInputs,
        switchInputs
    };
    
    unsigned char port;
    unsigned char pinNumber;
    
    portCopy = *gpioGroupArray[GPIOGroupsEnum][pinEnum]->port;
    pinNumber = gpioGroupArray[GPIOGroupsEnum][pinEnum]->pinNumber;
    
    return (portCopy & (1U << pinNumber)) >> pinNumber;
#endif
    switch(GPIOGroupsEnum)
    {
        case ChannelsInputGpioGroup:
        {
            switch(pinEnum)
            {
                case ch0Input:
                    return _ch0;
                case ch1Input:
                    return _ch1;
                case ch2Input:
                    return _ch2;
                case ch3Input:
                    return _ch3;
                default:
                    break;
            }
        }
        break;
        case SwitchInputGPIOGroup:
        {
            switch(pinEnum)
            {
                case chswitch:
                    return _chswitch;
                case a1:
                    return _automan;
                case muxEnable:
                    return _funcSwitch;
                default:
                    break;
            }
        }
        break;
        default:
            break;
    }
    return false;
}
