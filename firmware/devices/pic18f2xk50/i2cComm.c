#include <pic18f25k50.h>

#include "i2cComm.h"

static void startBit(void);
static void ackBit(void);
static void stopCondition(void);
static unsigned char i2cBufferFull(void);
static unsigned char waitEndOfTransfer(void);

static void sendByte(unsigned char byte);
static unsigned char readByte(void);


unsigned char inReceptionSlaveAddress;

unsigned char receiveI2CData(unsigned char *receptionBuffer, unsigned char expectedSize)
{
    return 0;   //no device, no new byte
}

unsigned char sendI2CData(unsigned char *dataToSend, unsigned char size, unsigned char slaveAddress)
{
    if(waitEndOfTransfer())
    {
        return 0;   //busy
    }
    inReceptionSlaveAddress = slaveAddress;
    
    return 1;
}



//Static functions
static void startBit(void)
{
    inReceptionSlaveAddress = 0;
    SSP1CON2bits.SEN = 1;
	while(SSP1CON2bits.SEN);  //Will be cleared by hardware 
}

static void ackBit(void)
{
    SSP1CON2bits.ACKEN = 1;
	while(SSP1CON2bits.ACKEN);
}

static void stopCondition(void)
{
    SSP1CON2bits.PEN = 1;
	while(SSP1CON2bits.PEN);  //Will be cleared by hardware 
}

static unsigned char i2cBufferFull(void)
{
    return SSP1STATbits.BF;
}

static unsigned char waitEndOfTransfer(void)
{
#define SSPCON2BusyBits (_SSPCON2_SEN_MASK | _SSPCON2_RSEN_MASK | _SSPCON2_PEN_MASK | _SSPCON2_RCEN_MASK | _SSPCON2_ACKEN_MASK)
    return (SSP1CON2 & SSPCON2BusyBits || SSP1STATbits.R_nW);
}


static void sendByte(unsigned char byte)
{
    SSP1BUF = byte;
}

static unsigned char readByte(void)
{
    return SSP1BUF;
}
