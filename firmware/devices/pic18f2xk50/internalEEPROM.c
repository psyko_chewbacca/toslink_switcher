/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "internalEEPROM.h"
#ifdef __XC8
#include <xc.h>
#else
#include <pic18fregs.h>
#endif

/*******************************Global variables*******************************/
typedef struct
{
    unsigned char wearLevelMarker : 1;
    unsigned char firstPassDone : 1;
    unsigned char unused : 6;
}wearLevelingManageStruct;

static unsigned char latestSavedDataSlot[persistedSettingsTypeCount];
static wearLevelingManageStruct currentWearLvlMarkerValue[persistedSettingsTypeCount] = 
{ 
    {0, 0, 0}
};
/******************************************************************************/

/*****************************Function prototypes******************************/
static unsigned char calculatedHeaderAddressInEEPROM(unsigned char settingsTypeEnum);
static bool getLastestSaveSlot(unsigned char settingsTypeEnum);
static bool readEEPROM(unsigned char startAddress, unsigned char length, unsigned char* dataBuffer);
static bool writeEEPROM(unsigned char startAddress, unsigned char length, unsigned char* dataBuffer);

//EEPROM accessor functions
//Do not use XC8 implementation. They seem to change things around from time to time.
static unsigned char readEEPROMByte(unsigned char addr);
static bool writeEEPROMByte(unsigned char addr, unsigned char data);

//EEPROM operations helper functions
static void toggleWriteEnableBit(bool enable);
static bool writeError(void);
static bool readInProgress(void);
static bool writeInProgress(void);
/******************************************************************************/

bool getLastestEEPROMData(unsigned char settingsTypeEnum, unsigned char* output)
{
    unsigned char startAddress;
    SaveEEPROMHeaderStruct headerRead;
    if(currentWearLvlMarkerValue[settingsTypeEnum].firstPassDone == 0)
    {
        if(getLastestSaveSlot(settingsTypeEnum) == false)
        {
            return false;
        }
    }
    
    startAddress = calculatedHeaderAddressInEEPROM(settingsTypeEnum);
    
    if(readEEPROM(startAddress, sizeof(SaveEEPROMHeaderStruct), (unsigned char *)&headerRead) == false)
    {
        return false;
    }
    
    if(headerRead.saveHeaderContent.markerByteStruct.alwaysZero != 0)
    {
        return false;
    }
    
    if(readEEPROM(startAddress + sizeof(SaveEEPROMHeaderStruct), headerRead.saveHeaderContent.payloadSize, output) == false)
    {
        return false;
    }
    
    return true;
}

//This function assumes "getLastestEEPROMData()" has been previously called to set
//"latestSavedDataSlot" and "currentWearLvlMarkerValue" values
void saveLastestEEPROMData(unsigned char settingsTypeEnum, unsigned char* input)
{
    SaveEEPROMHeaderStruct header;
    unsigned char startAddress;
    //1.Check if last saved data was in the last slot
    if(currentWearLvlMarkerValue[settingsTypeEnum].firstPassDone == 0)
    {
        if(getLastestSaveSlot(settingsTypeEnum) == false)
        {
            return;
        }
    }
    if(latestSavedDataSlot[settingsTypeEnum] >= (SETTINGS_PERSISTENCE_NB_SLOTS - 1))
    {
        //Move new save to first slot
        latestSavedDataSlot[settingsTypeEnum] = 0;
        //Invert wear level marker
        currentWearLvlMarkerValue[settingsTypeEnum].wearLevelMarker = !currentWearLvlMarkerValue[settingsTypeEnum].wearLevelMarker;
    }
    else
    {
        latestSavedDataSlot[settingsTypeEnum]++;
    }
    startAddress = calculatedHeaderAddressInEEPROM(settingsTypeEnum);
    //2.Construct header data to save in EEPROM.
    header.saveHeaderContent.markerByteStruct.alwaysZero = 0;
    header.saveHeaderContent.markerByteStruct.settingType = settingsTypeEnum;
    header.saveHeaderContent.markerByteStruct.wearLvlMarker = currentWearLvlMarkerValue[settingsTypeEnum].wearLevelMarker;
    header.saveHeaderContent.payloadSize = persistedSettingsTypesSizes[settingsTypeEnum];
    //3.Save structure to slot in EEPROM.
    //header data
    writeEEPROM(startAddress, sizeof(SaveEEPROMHeaderStruct), (unsigned char *)&header);
    //paypal data
    writeEEPROM(startAddress + sizeof(SaveEEPROMHeaderStruct), persistedSettingsTypesSizes[settingsTypeEnum], input);
}

static unsigned char calculatedHeaderAddressInEEPROM(unsigned char settingsTypeEnum)
{
    unsigned char startSectionOffset = 0, inSectionOffset = 0;
    unsigned char i;
    
    for(i = 0; i < settingsTypeEnum; i++)
    {
        startSectionOffset += (SETTINGS_PERSISTENCE_NB_SLOTS * (sizeof(SaveEEPROMHeaderStruct) + persistedSettingsTypesSizes[i]));
    }
    
    inSectionOffset = latestSavedDataSlot[settingsTypeEnum] * (sizeof(SaveEEPROMHeaderStruct) + persistedSettingsTypesSizes[settingsTypeEnum]);
    
    return (startSectionOffset + inSectionOffset);
}

static bool getLastestSaveSlot(unsigned char settingsTypeEnum)
{
    SaveEEPROMHeaderStruct dataStruct;
    SaveEEPROMHeaderStruct previousReadData;
    unsigned char i, firstPass = 0;
    
    latestSavedDataSlot[settingsTypeEnum] = SETTINGS_PERSISTENCE_NB_SLOTS - 1;
    unsigned char Address = calculatedHeaderAddressInEEPROM(settingsTypeEnum);
    
    if(readEEPROM(Address, sizeof(previousReadData.saveHeaderRawBytes.firstByte), &(previousReadData.saveHeaderRawBytes.firstByte)) == false) //Read last possible entry slot
    {
        return false;                                       //This is to handle roll over to start situation
    }
    
    if(previousReadData.saveHeaderRawBytes.firstByte == 0xFF)    //byte is blank. System never wrote on last slot.
    {
        //Start from the first slot
        latestSavedDataSlot[settingsTypeEnum] = firstPass;
        Address = calculatedHeaderAddressInEEPROM(settingsTypeEnum);
    
        firstPass++;
        if(readEEPROM(Address, sizeof(previousReadData.saveHeaderRawBytes.firstByte), &(previousReadData.saveHeaderRawBytes.firstByte)) == false) //Read very first entry
        {
            return false; 
        }
        //We'll compare slot 0 (possibly old) to slot 1 (possibly new)
    }
    //Else, if byte not blank, we start comparing last slot(possibly old) to slot 0 (possibily new)
    
    //Read first byte of all possible slots.
    for(i = firstPass; i < (SETTINGS_PERSISTENCE_NB_SLOTS - 1); i++)
    {
        latestSavedDataSlot[settingsTypeEnum] = i;
        Address = calculatedHeaderAddressInEEPROM(settingsTypeEnum);
        if(readEEPROM(Address, sizeof(dataStruct.saveHeaderRawBytes.firstByte), (unsigned char *)&(dataStruct.saveHeaderRawBytes.firstByte)) == false)
        {
           return false;
        }
        //If both slots have identical marker, it means they are part of the same loop
        //(A loop is decscribed as being a single sequence where you write data in all
        // save slots reserved in EEPROM, starting from first to last slot. Rolling over
        //from last slot back to slot 0 starts a new loop.)
        //If both markers are equal up to last slot (end of "for" loop), it means last slot
        //contains the latest saved data.
        if(dataStruct.saveHeaderContent.markerByte != previousReadData.saveHeaderContent.markerByte)
        {
            //If markers are not the same, then dataStruct points to an old saved data and
            //previousReadData contains the latest saved data.
            i--;
            dataStruct.saveHeaderContent.markerByte = previousReadData.saveHeaderContent.markerByte;
            break;
        }        
    }
    
    //Save important info for when we'll save new data.
    latestSavedDataSlot[settingsTypeEnum] = i;
    currentWearLvlMarkerValue[settingsTypeEnum].wearLevelMarker = dataStruct.saveHeaderContent.markerByte;
    currentWearLvlMarkerValue[settingsTypeEnum].firstPassDone = 1;
    
    return true;
}

static bool readEEPROM(unsigned char startAddress, unsigned char length, unsigned char* dataBuffer)
{
    unsigned char i, timeout = 0;
    
    if((unsigned short)(startAddress + length) >= MAXEEPROMSIZE)   //If you try to read outside of the valid range. (EEPROM is only 256 bytes large)
    {
        return false;
    }
    
    if(readInProgress())
    {
        return false;
    }
    
    for(i = 0; i < length; i++)    //Read byte per byte
    {
        while(writeInProgress())
        {
            if(timeout++ == 250)
            {
                return false;
            }
        }
        
        dataBuffer[i] = readEEPROMByte(startAddress + i);
    }
    
    return true;
}

static bool writeEEPROM(unsigned char startAddress, unsigned char length, unsigned char* dataBuffer)
{
    unsigned char i;
    
    
    if((unsigned short)(startAddress + length) >= MAXEEPROMSIZE)
    {
        return false;
    }    
    
    for(i = 0; i < length; i++)    //Write byte per byte
    {
        while(writeInProgress());
        writeEEPROMByte(startAddress + i, dataBuffer[i]);
    }
    return true;
}

static unsigned char readEEPROMByte(unsigned char addr)
{
    EEADR = addr;
    EECON1 &= 0x3F; //Set CFGS and EEPGD bits to 0.
    EECON1 |= 0x01; //Set RD bit to initiate read.
    
    while(readInProgress());
    
    return EEDATA;
}

static bool writeEEPROMByte(unsigned char addr, unsigned char data)
{
    unsigned char INTCONStatus;
    toggleWriteEnableBit(true);
    EEADR = addr;
    EEDATA = data;
    EECON1 &= 0x3F; //Set CFGS and EEPGD bits to 0.
    EECON1 |= 0x04; //Set WREN bit to enabel write
    INTCONStatus = INTCON;  //Save interrupt status
    INTCON &= 0x7F; //Disable interrupts
    EECON2 = 0x55; //Required to write
	EECON2 = 0xAA;
    EECON1 |= 0x02; //Set WR bit to initiate write
    
    while(writeInProgress());

    INTCON = INTCONStatus; //Restore interrupts if that was the case.
    toggleWriteEnableBit(false);
    
    return writeError();
}

static void toggleWriteEnableBit(bool enable)
{
    if(enable)
    {
        EECON1 |= 0x04;
    }
    else
    {
        EECON1 &= 0xFB;
    }
}

static bool writeError(void)
{
    bool result = (EECON1 & 0x08);
    
    EECON1 &= 0xF7;
    
    return result;
}

static bool readInProgress(void)
{
    return (EECON1 & 0x01);
}

static bool writeInProgress(void)
{
    return (EECON1 & 0x02); //WR bit
}