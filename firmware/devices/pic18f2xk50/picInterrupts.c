
#ifdef __XC8
#include <xc.h>
#else
#include <pic18fregs.h>
#endif
#include <stdbool.h>

#define HIGH_INT 1
#define LOW_INT 0

bool timer0Overflow = 0;

#ifdef __XC8
void __interrupt inthandler(void)
#else
void inthandler(void) __interrupt HIGH_INT
#endif
{
    if (INTCONbits.TMR0IE && INTCONbits.TMR0IF) 
    {
        timer0Overflow = true;   /*Flag used to minimize time spent in ISR*/
        INTCONbits.TMR0IF=0;          /*Reset int flag*/
        return;
    }

/*    if (TMR1IE && TMR1IF) {
        checkvalidstate = 1;
        TMR1IF = 0;
    }

    if (IOCIE && IOCIF) {
        if(validChannelsArray[chState]){
            TMR1L = 0;
            TMR1H = 0;
        }
        IOCIF = 0;
    }*/
// process other interrupt sources here, if required
}

bool getTimer0OverflowFlag(void)
{
    //disable ints
    return timer0Overflow;
    //enable ints
}

void setTimer0OverflowFlag(bool value)
{
    //disable ints
    timer0Overflow = value;
    //enable ints
}