#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-18F24K50_Board_Rev2.mk)" "nbproject/Makefile-local-18F24K50_Board_Rev2.mk"
include nbproject/Makefile-local-18F24K50_Board_Rev2.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=18F24K50_Board_Rev2
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../vectors.asm ../boot_asm.asm ../usb_desc.asm ../usb.asm ../boot.asm

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1472/vectors.o ${OBJECTDIR}/_ext/1472/boot_asm.o ${OBJECTDIR}/_ext/1472/usb_desc.o ${OBJECTDIR}/_ext/1472/usb.o ${OBJECTDIR}/_ext/1472/boot.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1472/vectors.o.d ${OBJECTDIR}/_ext/1472/boot_asm.o.d ${OBJECTDIR}/_ext/1472/usb_desc.o.d ${OBJECTDIR}/_ext/1472/usb.o.d ${OBJECTDIR}/_ext/1472/boot.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1472/vectors.o ${OBJECTDIR}/_ext/1472/boot_asm.o ${OBJECTDIR}/_ext/1472/usb_desc.o ${OBJECTDIR}/_ext/1472/usb.o ${OBJECTDIR}/_ext/1472/boot.o

# Source Files
SOURCEFILES=../vectors.asm ../boot_asm.asm ../usb_desc.asm ../usb.asm ../boot.asm


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-18F24K50_Board_Rev2.mk dist/${CND_CONF}/${IMAGE_TYPE}/bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18f24k50
MP_LINKER_DEBUG_OPTION= 
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1472/vectors.o: ../vectors.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/vectors.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/vectors.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/vectors.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_SIMULATOR=1 -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/vectors.lst\" -e\"${OBJECTDIR}/_ext/1472/vectors.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/vectors.o\" \"../vectors.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/vectors.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/vectors.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_ext/1472/boot_asm.o: ../boot_asm.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/boot_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/boot_asm.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/boot_asm.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_SIMULATOR=1 -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/boot_asm.lst\" -e\"${OBJECTDIR}/_ext/1472/boot_asm.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/boot_asm.o\" \"../boot_asm.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/boot_asm.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/boot_asm.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_ext/1472/usb_desc.o: ../usb_desc.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/usb_desc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/usb_desc.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/usb_desc.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_SIMULATOR=1 -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/usb_desc.lst\" -e\"${OBJECTDIR}/_ext/1472/usb_desc.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/usb_desc.o\" \"../usb_desc.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/usb_desc.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/usb_desc.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_ext/1472/usb.o: ../usb.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/usb.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/usb.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/usb.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_SIMULATOR=1 -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/usb.lst\" -e\"${OBJECTDIR}/_ext/1472/usb.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/usb.o\" \"../usb.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/usb.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/usb.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_ext/1472/boot.o: ../boot.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/boot.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/boot.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/boot.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -d__DEBUG -d__MPLAB_DEBUGGER_SIMULATOR=1 -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/boot.lst\" -e\"${OBJECTDIR}/_ext/1472/boot.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/boot.o\" \"../boot.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/boot.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/boot.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
else
${OBJECTDIR}/_ext/1472/vectors.o: ../vectors.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/vectors.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/vectors.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/vectors.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/vectors.lst\" -e\"${OBJECTDIR}/_ext/1472/vectors.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/vectors.o\" \"../vectors.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/vectors.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/vectors.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_ext/1472/boot_asm.o: ../boot_asm.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/boot_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/boot_asm.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/boot_asm.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/boot_asm.lst\" -e\"${OBJECTDIR}/_ext/1472/boot_asm.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/boot_asm.o\" \"../boot_asm.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/boot_asm.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/boot_asm.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_ext/1472/usb_desc.o: ../usb_desc.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/usb_desc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/usb_desc.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/usb_desc.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/usb_desc.lst\" -e\"${OBJECTDIR}/_ext/1472/usb_desc.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/usb_desc.o\" \"../usb_desc.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/usb_desc.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/usb_desc.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_ext/1472/usb.o: ../usb.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/usb.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/usb.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/usb.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/usb.lst\" -e\"${OBJECTDIR}/_ext/1472/usb.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/usb.o\" \"../usb.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/usb.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/usb.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
${OBJECTDIR}/_ext/1472/boot.o: ../boot.asm  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/boot.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/boot.o 
	@${FIXDEPS} dummy.d -e "${OBJECTDIR}/_ext/1472/boot.err" $(SILENT) -c ${MP_AS} $(MP_EXTRA_AS_PRE) -q -p$(MP_PROCESSOR_OPTION)  -l\"${OBJECTDIR}/_ext/1472/boot.lst\" -e\"${OBJECTDIR}/_ext/1472/boot.err\" $(ASM_OPTIONS)-c- -dPROCESSOR_HEADER=p18f24k50.inc -dBOARD_REV2   -o\"${OBJECTDIR}/_ext/1472/boot.o\" \"../boot.asm\" 
	@${DEP_GEN} -d "${OBJECTDIR}/_ext/1472/boot.o"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/boot.o.d" $(SILENT) -rsi ${MP_AS_DIR} -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../bootloader.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\bootloader.lkr"  -p$(MP_PROCESSOR_OPTION)  -w -x -u_DEBUG -z__ICD2RAM=1 -m"${DISTDIR}/${PROJECTNAME}.bootloader.X.${IMAGE_TYPE}.map"   -z__MPLAB_BUILD=1  -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_SIMULATOR=1 $(MP_LINKER_DEBUG_OPTION) -odist/${CND_CONF}/${IMAGE_TYPE}/bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
else
dist/${CND_CONF}/${IMAGE_TYPE}/bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../bootloader.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\bootloader.lkr"  -p$(MP_PROCESSOR_OPTION)  -w  -m"${DISTDIR}/${PROJECTNAME}.bootloader.X.${IMAGE_TYPE}.map"   -z__MPLAB_BUILD=1  -odist/${CND_CONF}/${IMAGE_TYPE}/bootloader.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/18F24K50_Board_Rev2
	${RM} -r dist/18F24K50_Board_Rev2

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
