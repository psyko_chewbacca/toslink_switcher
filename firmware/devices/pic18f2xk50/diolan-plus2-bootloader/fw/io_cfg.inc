;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  BootLoader.                                                             ;;
;;  Copyright (C) 2007 Diolan ( http://www.diolan.com )                     ;;
;;                                                                          ;;
;;  This program is free software: you can redistribute it and/or modify    ;;
;;  it under the terms of the GNU General Public License as published by    ;;
;;  the Free Software Foundation, either version 3 of the License, or       ;;
;;  (at your option) any later version.                                     ;;
;;                                                                          ;;
;;  This program is distributed in the hope that it will be useful,         ;;
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of          ;;
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           ;;
;;  GNU General Public License for more details.                            ;;
;;                                                                          ;;
;;  You should have received a copy of the GNU General Public License       ;;
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; I/O configuration
;-----------------------------------------------------------------------------
; Constants
;-----------------------------------------------------------------------------
; LED
#define USE_LED	  ; Undef/comment if you not want to use FW LED
#ifdef	USE_LED
#ifdef BOARD_REV1
#define LED   		LATB
#define LED_TRIS  	TRISB
#define LED_PIN		RB2
#endif
#ifdef BOARD_REV2
#define LED   		LATC
#define LED_TRIS  	TRISC
#define LED_PIN		RC0
#endif
	
; This macro is used to test (and skip) if the TMR0 as had and overflow
; - If so, change output state of the LED bit, reset TMR0
; - If not, do nothing
;
TIMER0_OVERFLOW_CHECK_TOGGLE_LED	macro
	; check if timer0 has overflown
	btfss INTCON, TMR0IF
	; if not set, skip flag reset and led toggle
	bra endOverflowCheck
        bcf INTCON, TMR0IF
	; Using btg(bit toggle) instruction since bootloader is only supporting 18F series PIC
	btg LED, LED_PIN, 0
endOverflowCheck
	endm
#endif
;-----------------------------------------------------------------------------
; Jumpers
;
; Note! Ensure that the jumper *is* in digital input mode, see vectors.asm/pre_main
;
#define USE_JP_BOOTLOADER_EN	; Undef/comment if you not want to use FW Jumper
;
#ifdef	USE_JP_BOOTLOADER_EN
;
#ifdef BOARD_REV1
#define JP_BOOTLOADER_ANSEL	ANSELA
#define JP_BOOTLOADER_TRIS	TRISA
#define JP_BOOTLOADER_PORT	PORTA
#define JP_BOOTLOADER_PIN	RA4
#endif
#ifdef BOARD_REV2
#define JP_BOOTLOADER_ANSEL	ANSELA
#define JP_BOOTLOADER_TRIS	TRISA
#define JP_BOOTLOADER_PORT	PORTA
#define JP_BOOTLOADER_PIN	RA4
#define JP_BOOTLOADER_GLOBAL_PULLUP INTCON2
#define JP_BOOTLOADER_PULLUP_BIT  WPUB7
#define JP_BOOTLOADER_IND_PULLUP WPUB
#endif
;
; This macro is used to test (and skip) if the jumper is 'set'
; - modify to configure the port/pin as digital input for your PIC
; - modify to suit polarity (jumper 'set' == 1 or 0)
;
JP_BOOTLOADER_SKIP_IF_SET	macro

#ifdef __18F25K50
	movlb	0xF		
	bcf	JP_BOOTLOADER_ANSEL,JP_BOOTLOADER_PIN	
	movlb	0x0	
	bsf	JP_BOOTLOADER_TRIS, JP_BOOTLOADER_PIN
	btfsc	JP_BOOTLOADER_PORT, JP_BOOTLOADER_PIN
#endif
;
#ifdef __18F24K50
	movlb	0xF		
	bcf	JP_BOOTLOADER_ANSEL,JP_BOOTLOADER_PIN	
	movlb	0x0	
#ifdef BOARD_REV2
	bcf	JP_BOOTLOADER_GLOBAL_PULLUP, JP_BOOTLOADER_PULLUP_BIT ;Global pull-ups on PORTB enabled.
	bsf	JP_BOOTLOADER_IND_PULLUP, JP_BOOTLOADER_PIN ;Specifically enable pull-up on pin
#endif
;	
	bsf	JP_BOOTLOADER_TRIS, JP_BOOTLOADER_PIN
	btfsc	JP_BOOTLOADER_PORT, JP_BOOTLOADER_PIN
#endif
;
	endm

#endif
;-----------------------------------------------------------------------------
