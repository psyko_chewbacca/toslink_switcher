#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-24K50_Rev2_RELEASE.mk)" "nbproject/Makefile-local-24K50_Rev2_RELEASE.mk"
include nbproject/Makefile-local-24K50_Rev2_RELEASE.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=24K50_Rev2_RELEASE
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../../HAL/gpioWrapper.c ../../HAL/interrupts.c ../../HAL/settingsPersistence.c ../../HAL/deviceAcessor.c deviceConfig.c gpio.c internalEEPROM.c picInterrupts.c i2cComm.c ../../TOSLINK_Application/channelValidityManager.c ../../TOSLINK_Application/muxManager.c ../../TOSLINK_Application/priorityConfig.c ../../TOSLINK_Application/settingsManager.c ../../TOSLINK_Application/TOSLINK_switcher.c ../../main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1 ${OBJECTDIR}/_ext/1445231827/interrupts.p1 ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1 ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1 ${OBJECTDIR}/deviceConfig.p1 ${OBJECTDIR}/gpio.p1 ${OBJECTDIR}/internalEEPROM.p1 ${OBJECTDIR}/picInterrupts.p1 ${OBJECTDIR}/i2cComm.p1 ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1 ${OBJECTDIR}/_ext/837154813/muxManager.p1 ${OBJECTDIR}/_ext/837154813/priorityConfig.p1 ${OBJECTDIR}/_ext/837154813/settingsManager.p1 ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1 ${OBJECTDIR}/_ext/43898991/main.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1.d ${OBJECTDIR}/_ext/1445231827/interrupts.p1.d ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1.d ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1.d ${OBJECTDIR}/deviceConfig.p1.d ${OBJECTDIR}/gpio.p1.d ${OBJECTDIR}/internalEEPROM.p1.d ${OBJECTDIR}/picInterrupts.p1.d ${OBJECTDIR}/i2cComm.p1.d ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1.d ${OBJECTDIR}/_ext/837154813/muxManager.p1.d ${OBJECTDIR}/_ext/837154813/priorityConfig.p1.d ${OBJECTDIR}/_ext/837154813/settingsManager.p1.d ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1.d ${OBJECTDIR}/_ext/43898991/main.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1 ${OBJECTDIR}/_ext/1445231827/interrupts.p1 ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1 ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1 ${OBJECTDIR}/deviceConfig.p1 ${OBJECTDIR}/gpio.p1 ${OBJECTDIR}/internalEEPROM.p1 ${OBJECTDIR}/picInterrupts.p1 ${OBJECTDIR}/i2cComm.p1 ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1 ${OBJECTDIR}/_ext/837154813/muxManager.p1 ${OBJECTDIR}/_ext/837154813/priorityConfig.p1 ${OBJECTDIR}/_ext/837154813/settingsManager.p1 ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1 ${OBJECTDIR}/_ext/43898991/main.p1

# Source Files
SOURCEFILES=../../HAL/gpioWrapper.c ../../HAL/interrupts.c ../../HAL/settingsPersistence.c ../../HAL/deviceAcessor.c deviceConfig.c gpio.c internalEEPROM.c picInterrupts.c i2cComm.c ../../TOSLINK_Application/channelValidityManager.c ../../TOSLINK_Application/muxManager.c ../../TOSLINK_Application/priorityConfig.c ../../TOSLINK_Application/settingsManager.c ../../TOSLINK_Application/TOSLINK_switcher.c ../../main.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-24K50_Rev2_RELEASE.mk dist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F24K50
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1: ../../HAL/gpioWrapper.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1445231827" 
	@${RM} ${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1  ../../HAL/gpioWrapper.c 
	@-${MV} ${OBJECTDIR}/_ext/1445231827/gpioWrapper.d ${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1445231827/interrupts.p1: ../../HAL/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1445231827" 
	@${RM} ${OBJECTDIR}/_ext/1445231827/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1445231827/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1445231827/interrupts.p1  ../../HAL/interrupts.c 
	@-${MV} ${OBJECTDIR}/_ext/1445231827/interrupts.d ${OBJECTDIR}/_ext/1445231827/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1445231827/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1: ../../HAL/settingsPersistence.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1445231827" 
	@${RM} ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1  ../../HAL/settingsPersistence.c 
	@-${MV} ${OBJECTDIR}/_ext/1445231827/settingsPersistence.d ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1: ../../HAL/deviceAcessor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1445231827" 
	@${RM} ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1  ../../HAL/deviceAcessor.c 
	@-${MV} ${OBJECTDIR}/_ext/1445231827/deviceAcessor.d ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/deviceConfig.p1: deviceConfig.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/deviceConfig.p1.d 
	@${RM} ${OBJECTDIR}/deviceConfig.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/deviceConfig.p1  deviceConfig.c 
	@-${MV} ${OBJECTDIR}/deviceConfig.d ${OBJECTDIR}/deviceConfig.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/deviceConfig.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/gpio.p1: gpio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/gpio.p1.d 
	@${RM} ${OBJECTDIR}/gpio.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/gpio.p1  gpio.c 
	@-${MV} ${OBJECTDIR}/gpio.d ${OBJECTDIR}/gpio.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/gpio.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/internalEEPROM.p1: internalEEPROM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/internalEEPROM.p1.d 
	@${RM} ${OBJECTDIR}/internalEEPROM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/internalEEPROM.p1  internalEEPROM.c 
	@-${MV} ${OBJECTDIR}/internalEEPROM.d ${OBJECTDIR}/internalEEPROM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/internalEEPROM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/picInterrupts.p1: picInterrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/picInterrupts.p1.d 
	@${RM} ${OBJECTDIR}/picInterrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/picInterrupts.p1  picInterrupts.c 
	@-${MV} ${OBJECTDIR}/picInterrupts.d ${OBJECTDIR}/picInterrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/picInterrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2cComm.p1: i2cComm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/i2cComm.p1.d 
	@${RM} ${OBJECTDIR}/i2cComm.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2cComm.p1  i2cComm.c 
	@-${MV} ${OBJECTDIR}/i2cComm.d ${OBJECTDIR}/i2cComm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2cComm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/channelValidityManager.p1: ../../TOSLINK_Application/channelValidityManager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/channelValidityManager.p1  ../../TOSLINK_Application/channelValidityManager.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/channelValidityManager.d ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/muxManager.p1: ../../TOSLINK_Application/muxManager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/muxManager.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/muxManager.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/muxManager.p1  ../../TOSLINK_Application/muxManager.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/muxManager.d ${OBJECTDIR}/_ext/837154813/muxManager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/muxManager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/priorityConfig.p1: ../../TOSLINK_Application/priorityConfig.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/priorityConfig.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/priorityConfig.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/priorityConfig.p1  ../../TOSLINK_Application/priorityConfig.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/priorityConfig.d ${OBJECTDIR}/_ext/837154813/priorityConfig.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/priorityConfig.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/settingsManager.p1: ../../TOSLINK_Application/settingsManager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/settingsManager.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/settingsManager.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/settingsManager.p1  ../../TOSLINK_Application/settingsManager.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/settingsManager.d ${OBJECTDIR}/_ext/837154813/settingsManager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/settingsManager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1: ../../TOSLINK_Application/TOSLINK_switcher.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1  ../../TOSLINK_Application/TOSLINK_switcher.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.d ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/43898991/main.p1: ../../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/main.p1.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/43898991/main.p1  ../../main.c 
	@-${MV} ${OBJECTDIR}/_ext/43898991/main.d ${OBJECTDIR}/_ext/43898991/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/43898991/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1: ../../HAL/gpioWrapper.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1445231827" 
	@${RM} ${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1  ../../HAL/gpioWrapper.c 
	@-${MV} ${OBJECTDIR}/_ext/1445231827/gpioWrapper.d ${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1445231827/gpioWrapper.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1445231827/interrupts.p1: ../../HAL/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1445231827" 
	@${RM} ${OBJECTDIR}/_ext/1445231827/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1445231827/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1445231827/interrupts.p1  ../../HAL/interrupts.c 
	@-${MV} ${OBJECTDIR}/_ext/1445231827/interrupts.d ${OBJECTDIR}/_ext/1445231827/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1445231827/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1: ../../HAL/settingsPersistence.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1445231827" 
	@${RM} ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1  ../../HAL/settingsPersistence.c 
	@-${MV} ${OBJECTDIR}/_ext/1445231827/settingsPersistence.d ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1445231827/settingsPersistence.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1: ../../HAL/deviceAcessor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1445231827" 
	@${RM} ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1.d 
	@${RM} ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1  ../../HAL/deviceAcessor.c 
	@-${MV} ${OBJECTDIR}/_ext/1445231827/deviceAcessor.d ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/1445231827/deviceAcessor.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/deviceConfig.p1: deviceConfig.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/deviceConfig.p1.d 
	@${RM} ${OBJECTDIR}/deviceConfig.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/deviceConfig.p1  deviceConfig.c 
	@-${MV} ${OBJECTDIR}/deviceConfig.d ${OBJECTDIR}/deviceConfig.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/deviceConfig.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/gpio.p1: gpio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/gpio.p1.d 
	@${RM} ${OBJECTDIR}/gpio.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/gpio.p1  gpio.c 
	@-${MV} ${OBJECTDIR}/gpio.d ${OBJECTDIR}/gpio.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/gpio.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/internalEEPROM.p1: internalEEPROM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/internalEEPROM.p1.d 
	@${RM} ${OBJECTDIR}/internalEEPROM.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/internalEEPROM.p1  internalEEPROM.c 
	@-${MV} ${OBJECTDIR}/internalEEPROM.d ${OBJECTDIR}/internalEEPROM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/internalEEPROM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/picInterrupts.p1: picInterrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/picInterrupts.p1.d 
	@${RM} ${OBJECTDIR}/picInterrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/picInterrupts.p1  picInterrupts.c 
	@-${MV} ${OBJECTDIR}/picInterrupts.d ${OBJECTDIR}/picInterrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/picInterrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2cComm.p1: i2cComm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/i2cComm.p1.d 
	@${RM} ${OBJECTDIR}/i2cComm.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2cComm.p1  i2cComm.c 
	@-${MV} ${OBJECTDIR}/i2cComm.d ${OBJECTDIR}/i2cComm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2cComm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/channelValidityManager.p1: ../../TOSLINK_Application/channelValidityManager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/channelValidityManager.p1  ../../TOSLINK_Application/channelValidityManager.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/channelValidityManager.d ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/channelValidityManager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/muxManager.p1: ../../TOSLINK_Application/muxManager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/muxManager.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/muxManager.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/muxManager.p1  ../../TOSLINK_Application/muxManager.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/muxManager.d ${OBJECTDIR}/_ext/837154813/muxManager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/muxManager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/priorityConfig.p1: ../../TOSLINK_Application/priorityConfig.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/priorityConfig.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/priorityConfig.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/priorityConfig.p1  ../../TOSLINK_Application/priorityConfig.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/priorityConfig.d ${OBJECTDIR}/_ext/837154813/priorityConfig.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/priorityConfig.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/settingsManager.p1: ../../TOSLINK_Application/settingsManager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/settingsManager.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/settingsManager.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/settingsManager.p1  ../../TOSLINK_Application/settingsManager.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/settingsManager.d ${OBJECTDIR}/_ext/837154813/settingsManager.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/settingsManager.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1: ../../TOSLINK_Application/TOSLINK_switcher.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/837154813" 
	@${RM} ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1.d 
	@${RM} ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1  ../../TOSLINK_Application/TOSLINK_switcher.c 
	@-${MV} ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.d ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/837154813/TOSLINK_switcher.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/_ext/43898991/main.p1: ../../main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/43898991" 
	@${RM} ${OBJECTDIR}/_ext/43898991/main.p1.d 
	@${RM} ${OBJECTDIR}/_ext/43898991/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/_ext/43898991/main.p1  ../../main.c 
	@-${MV} ${OBJECTDIR}/_ext/43898991/main.d ${OBJECTDIR}/_ext/43898991/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/_ext/43898991/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.map  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"       --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    diolan-plus2-bootloader/fw/bootloader.X/dist/18F24K50_Board_Rev2/production/bootloader.X.production.hex
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.map  --double=24 --float=24 --emi=wordwrite --rom=default,-0-7FF --opt=default,+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -DBOARD_REV2 -P -N255 --strict --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x800 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,-config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
	@echo "Creating unified hex file"
	@"C:/Program Files (x86)/Microchip/MPLABX/v3.00/mplab_ide/mplab_ide/modules/../../bin/hexmate" --edf="C:/Program Files (x86)/Microchip/MPLABX/v3.00/mplab_ide/mplab_ide/modules/../../dat/en_msgs.txt" dist/${CND_CONF}/${IMAGE_TYPE}/pic18f2xk50.${IMAGE_TYPE}.hex diolan-plus2-bootloader/fw/bootloader.X/dist/18F24K50_Board_Rev2/production/bootloader.X.production.hex -odist/${CND_CONF}/production/pic18f2xk50.production.unified.hex

endif


# Subprojects
.build-subprojects:
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
	cd /D diolan-plus2-bootloader/fw/bootloader.X && ${MAKE}  -f Makefile CONF=18F24K50_Board_Rev2 TYPE_IMAGE=DEBUG_RUN
else
	cd /D diolan-plus2-bootloader/fw/bootloader.X && ${MAKE}  -f Makefile CONF=18F24K50_Board_Rev2
endif


# Subprojects
.clean-subprojects:
	cd /D diolan-plus2-bootloader/fw/bootloader.X && rm -rf "build/18F24K50_Board_Rev2" "dist/18F24K50_Board_Rev2"

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/24K50_Rev2_RELEASE
	${RM} -r dist/24K50_Rev2_RELEASE

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
