/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#ifndef GPIO_H
#define	GPIO_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <stdbool.h>
#include <xc.h>
    
#ifdef BOARD_REV1
/*Green leds port define*/
#define _ch3ledTris TRISAbits.TRISA0
#define _ch3led  LATAbits.LATA0
#define _ch2ledTris TRISAbits.TRISA1
#define _ch2led  LATAbits.LATA1
#define _ch1ledTris TRISAbits.TRISA2
#define _ch1led  LATAbits.LATA2
#define _ch0ledTris TRISAbits.TRISA3
#define _ch0led  LATAbits.LATA3

/*Mux-Demux control port define*/
#define _a0Tris  TRISBbits.TRISB6
#define _a0  LATBbits.LATB6
#define _a1Tris  TRISBbits.TRISB7
#define _a1  LATBbits.LATB7
unsigned dummyWrite;
#define _muxEnableTris  dummyWrite
#define _muxEnable dummyWrite
#define _muxActiveChannelLEDOnTris dummyWrite
#define _muxActiveChannelLEDOn dummyWrite

/*EXP-IO port define (used as debug for now)*/
#define _debug1  LATCbits.LATC6
#define _debug2  LATCbits.LATC7
#define _debug3  LATBbits.LATB0
#define _debug4  LATBbits.LATB1
#define _debug5  LATBbits.LATB3
#define _debug6  LATAbits.LATA5

/*Channel input port define*/
#define _ch0 PORTCbits.RC2
#define _ch1 PORTCbits.RC1
#define _ch2 PORTBbits.RB5
#define _ch3 PORTBbits.RB4

/*Pushbutton port define*/
#define _chswitch  PORTCbits.RC0
#define _automan PORTAbits.RA4
const unsigned dummyRead = 1;
#define _funcSwitch dummyRead

#define _autoledTris TRISBbits.TRISB2
#define _autoled LATBbits.LATB2
#elif BOARD_REV2
/*Green leds port define*/
#define _ch0ledTris TRISAbits.TRISA6
#define _ch0led  LATAbits.LATA6
#define _ch1ledTris TRISAbits.TRISA7
#define _ch1led  LATAbits.LATA7
#define _ch2ledTris TRISAbits.TRISA2
#define _ch2led  LATAbits.LATA2
#define _ch3ledTris TRISAbits.TRISA1
#define _ch3led  LATAbits.LATA1

/*Mux-Demux control port define*/
#define _a0Tris  TRISBbits.TRISB7
#define _a0  LATBbits.LATB7
#define _a1Tris  TRISBbits.TRISB6
#define _a1  LATBbits.LATB6
#define _muxEnableTris  TRISAbits.TRISA0
#define _muxEnable LATAbits.LATA0
#define USE_PWM2
#define _muxActiveChannelLEDOnTris  TRISCbits.TRISC1
#define _muxActiveChannelLEDOn LATCbits.LATC1  //PWM2

/*EXP-IO port define*/
#define _slave_rst  LATAbits.LATA3
#define _slave_rstTris  TRISAbits.TRISA3
#define USE_PWM1
#define _slave_PWM LATCbits.LATC2  //PWM1
#define _slave_PWMTris TRISCbits.TRISC2
#else
#error Specify a board revision to build proper firmware.
#endif
#define _i2c_scl LATBbits.LATB1
#define _i2c_sclTris TRISBbits.TRISB1
#define _i2c_sda LATBbits.LATB0
#define _i2c_sdaTris TRISBbits.TRISB0
#define _uart_rx LATCbits.LATC7
#define _uart_rxTris TRISCbits.TRISC7
#define _uart_tx LATCbits.LATC6
#define _uart_txTris TRISCbits.TRISC6

/*Channel input port define*/
#define _ch0 PORTBbits.RB5
#define _ch1 PORTAbits.RA4
#define _ch2 PORTAbits.RA3
#define _ch3 PORTAbits.RA2

/*Pushbutton port define*/
#define _chswitch  PORTAbits.RA5
#define _automan PORTAbits.RA4
#define _funcSwitch PORTEbits.RE3

#define _autoledTris TRISCbits.TRISC0
#define _autoled LATCbits.LATC0



void setPinState(unsigned char GPIOGroupsEnum, unsigned char pinEnum, bool state);
bool getPinState(unsigned char GPIOGroupsEnum, unsigned char pinEnum);

#ifdef	__cplusplus
}
#endif

#endif	/* GPIO_H */

