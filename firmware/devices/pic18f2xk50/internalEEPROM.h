/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#ifndef INTERNALEEPROM_H
#define	INTERNALEEPROM_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "../../HAL/persistedSettingsDefs.h"
#include <stdbool.h>
    
//18F is little endian
//wearLvlMarker in "SettingsPersistenceStruct" bit will be MSB, and first bit in the 16bit word.
typedef union {
    struct saveHeaderContent
    {
        union
        {
            unsigned char markerByte;
            struct markerByteStruct
            {
                unsigned char wearLvlMarker : 1;
                unsigned char alwaysZero : 1;
                unsigned char settingType : 6; 
            }markerByteStruct;
        };
        unsigned char payloadSize;
    }saveHeaderContent;
    
    struct saveHeaderRawBytes
    {
        unsigned char firstByte;
        unsigned char secondByte;
    }saveHeaderRawBytes;
    //Actual Payload data comes right after this struct.
}SaveEEPROMHeaderStruct;

#define MAXEEPROMSIZE 0x100 //256B EEPROM
    
//Settings persistence specifics
#define SETTINGS_PERSISTENCE_START_ADDR 0x00
#define SETTINGS_PERSISTENCE_NB_SLOTS  8  //Will rotate on bytes available
#define SETTINGS_PERSISTENCE_RANGE_SIZE (SETTINGS_PERSISTENCE_NB_SLOTS * sizeof(_PersistentSaveEEPROMStruct))
    
bool getLastestEEPROMData(unsigned char settingsTypeEnum, unsigned char* output);
void saveLastestEEPROMData(unsigned char settingsTypeEnum, unsigned char* input);


#ifdef	__cplusplus
}
#endif

#endif	/* INTERNALEEPROM_H */

