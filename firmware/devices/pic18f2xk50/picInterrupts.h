/* 
 * File:   interrupts.h
 * Author: benjaminfd
 *
 * Created on 9 f�vrier 2016, 12:56
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#ifdef __XC8
#include <xc.h>
#else
#include <pic18f25k50.h>
#endif

void interrupt inthandler(void);


#ifdef	__cplusplus
}
#endif

#endif	/* INTERRUPTS_H */

