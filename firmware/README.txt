PIC18F25K50 & PIC18F24K50
Requirements:
-Microchip MPLABX IDE(confirmed with v3.26)
-Microchip XC8 compiler suite (confirmed with v1.37 Free)

Use Rev2 project configuration matching your MCU. Rev1 is for Beta units only.