/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#ifndef SETTINGSPERSISTENCE_H
#define	SETTINGSPERSISTENCE_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#define CHANNEL0DEFAULTPRIORITY 0
#define CHANNEL1DEFAULTPRIORITY 1
#define CHANNEL2DEFAULTPRIORITY 2
#define CHANNEL3DEFAULTPRIORITY 3
#define CHANNELUSED 1
#define CHANNELNOTUSED 0

//Holds on 2 bytes
typedef struct {
    unsigned ch0Used:1;
    unsigned ch1Used:1;
    unsigned ch2Used:1;
    unsigned ch3Used:1;
    unsigned unusedBits:4;

    unsigned ch0PriorityValue:2;
    unsigned ch1PriorityValue:2;
    unsigned ch2PriorityValue:2;
    unsigned ch3PriorityValue:2;
}SettingsPersistenceStruct;

void getPersistedSettings(SettingsPersistenceStruct* output);
void savePersistedSettings(SettingsPersistenceStruct* inputSettings);
void generateDefaultSettings(SettingsPersistenceStruct* output);


#ifdef	__cplusplus
}
#endif

#endif	/* SETTINGSPERSISTENCE_H */

