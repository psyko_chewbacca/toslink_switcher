/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#ifndef DEVICEIMPLEMENTATION_H
#define	DEVICEIMPLEMENTATION_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "settingsPersistence.h"
#include <stdbool.h>

extern void baseconfig(void);
extern void interruptconfig(void);
extern void timerconfig(void);
extern void IOconfig(void);
extern void PeripheralConfig(void);
extern void clearWatchdogTimer(void);
extern void softReset(void);

extern void setPinState(unsigned char GPIOGroupsEnum, unsigned char pinEnum, bool state);
extern bool getPinState(unsigned char GPIOGroupsEnum, unsigned char pinEnum);

extern bool getTimer0OverflowFlag(void);
extern void setTimer0OverflowFlag(unsigned char value);

extern void saveLastestEEPROMData(unsigned char settingsTypeEnum, unsigned char* output);
extern bool getLastestEEPROMData(unsigned char settingsTypeEnum, unsigned char* input);

extern unsigned char receiveI2CData(unsigned char *receptionBuffer, unsigned char expectedSize);
extern unsigned char sendI2CData(unsigned char *dataToSend, unsigned char size, unsigned char slaveAddress);


#ifdef	__cplusplus
}
#endif

#endif	/* DEVICEIMPLEMENTATION_H */

