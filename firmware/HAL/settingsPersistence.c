/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "settingsPersistence.h"
#include "persistedSettingsDefs.h"
#include "deviceImplementation.h"
#include <stdbool.h>

void getPersistedSettings(SettingsPersistenceStruct* output)
{
    if(getLastestEEPROMData(channelSettings, (unsigned char *)output) == false)
    {
        generateDefaultSettings(output);
    }
}

void savePersistedSettings(SettingsPersistenceStruct* inputSettings)
{
    saveLastestEEPROMData(channelSettings, (unsigned char *)inputSettings);
}

void generateDefaultSettings(SettingsPersistenceStruct* output)
{
    //No persisted settings found. Returning default values.
    (*output).ch0Used = CHANNELUSED;
    (*output).ch0PriorityValue = CHANNEL0DEFAULTPRIORITY;
    
    (*output).ch1Used = CHANNELUSED;
    (*output).ch1PriorityValue = CHANNEL1DEFAULTPRIORITY;
    
    (*output).ch2Used = CHANNELUSED;
    (*output).ch2PriorityValue = CHANNEL2DEFAULTPRIORITY;
    
    (*output).ch3Used = CHANNELUSED;
    (*output).ch3PriorityValue = CHANNEL3DEFAULTPRIORITY;
}



