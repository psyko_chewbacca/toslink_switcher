/* 
 * File:   gpioGroups.h
 * Author: benjaminfd
 *
 * Created on 18 f�vrier 2016, 11:55
 */

#ifndef GPIOGROUPS_H
#define	GPIOGROUPS_H

#ifdef	__cplusplus
extern "C" {
#endif

enum GPIOGroups
{
    LEDSGpioGroup = 0U,
    MUXGpioGroup,
    ChannelsInputGpioGroup,
    SwitchInputGPIOGroup,
    SlaveControlGPIOGroup,
    GroupCount
};


#ifdef	__cplusplus
}
#endif

#endif	/* GPIOGROUPS_H */

