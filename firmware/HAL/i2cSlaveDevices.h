/* 
 * File:   i2cSlaveDevices.h
 * Author: benjaminfd
 *
 * Created on 6 mai 2016, 13:55
 */

#ifndef I2CSLAVEDEVICES_H
#define	I2CSLAVEDEVICES_H

#ifdef	__cplusplus
extern "C" {
#endif


enum i2cSlaveDeviceAdress
{
    SlaveDeviceAddress_LCD_Addon = 0xA0,
    SlaveDevicesCount
};

#ifdef	__cplusplus
}
#endif

#endif	/* I2CSLAVEDEVICES_H */

