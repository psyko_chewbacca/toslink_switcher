/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#ifndef PERSISTEDSETTINGSDEFS_H
#define	PERSISTEDSETTINGSDEFS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "settingsPersistence.h"

enum
{
    channelSettings = 0,
    persistedSettingsTypeCount
}persistedSettingsTypes;

const unsigned char persistedSettingsTypesSizes[] = 
{
    sizeof(SettingsPersistenceStruct)
};

#ifdef	__cplusplus
}
#endif

#endif	/* PERSISTEDSETTINGSDEFS_H */

