/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "priorityConfig.h"

void cyclePriorityForCurrentChannel(unsigned char currentChannel)
{                  
    //Increase priority value up to top possible value and circle back to 0(lowest possible value)
    unsigned char currentChannelPriority = TOSLINKSwitcherSettings.channelPriorityValue[currentChannel];
    TOSLINKSwitcherSettings.channelPriorityValue[currentChannel] = (currentChannelPriority + 1) % NBINPUTCHANNELS;
}

void displayPriorityForCurrentChannel(unsigned char currentChannel)
{
    //Display currently selected channel numerical priority value using the green LEDs
    //on the device. As priority values goes from 0 to (NBINPUTCHANNELS -1) in number,
    //There is just enough LEDs to display both the selected channel (in red) and
    //the numerical priority value of said channel. 
    //
    //This function will fetch numerical priority of the input channel and light up
    //the amount of green LEDs to display the priority value. It will "jump" over the
    //red LED of the channel and light up the next green LED in line instead and continue
    //from there if need be.
    //For example, if channel 1(channels are numbered from 0 to 3) is set with a priority 
    //of 2, LEDs will light up with the following pattern, starting with the leftmost 
    //dual colored LED: Green, Red, Green, Off.
    unsigned char currentChannelPriority = TOSLINKSwitcherSettings.channelPriorityValue[currentChannel];
    unsigned char i;
    bool greenLedsArray[NBINPUTCHANNELS] = {LEDOFF, LEDOFF, LEDOFF, LEDOFF};
    unsigned char currentActiveChannelSkip = 0;
    
    for(i = 0; i < currentChannelPriority; i++)
    {
        if(i == currentChannel)
        {
            currentActiveChannelSkip = 1;    //To signal current LED is used to display active channel
        }
        
        if(i + currentActiveChannelSkip < NBINPUTCHANNELS)
        {
            greenLedsArray[i + currentActiveChannelSkip] = LEDON;
        }
    }
    
    //update real leds
    setLEDGPIOState(ch0led, greenLedsArray[0]);
    setLEDGPIOState(ch1led, greenLedsArray[1]);
    setLEDGPIOState(ch2led, greenLedsArray[2]);
    setLEDGPIOState(ch3led, greenLedsArray[3]);
}