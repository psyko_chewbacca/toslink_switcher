/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "TOSLINK_switcher.h"
#include "settingsManager.h"
//#include "eepromPreProgrammedData.h"

static void updateSystemFields(SettingsPersistenceStruct* dataStruct);

void restorePriorityConfigFromPersistence(void)
{
    SettingsPersistenceStruct latestPersistentSaveData;
    //1.Load last data from persistence settings location (typically EEPROM))
    getPersistedSettings(&latestPersistentSaveData);
    //2.Update system fields according to retreived data.
    updateSystemFields(&latestPersistentSaveData);
}

void savePriorityConfigToPersistence(void)
{
    SettingsPersistenceStruct input;
    
    input.ch0Used = TOSLINKSwitcherSettings.channelUsed[0];
    input.ch0PriorityValue = TOSLINKSwitcherSettings.channelPriorityValue[0];
    
    input.ch1Used = TOSLINKSwitcherSettings.channelUsed[1];
    input.ch1PriorityValue = TOSLINKSwitcherSettings.channelPriorityValue[1];
    
    input.ch2Used = TOSLINKSwitcherSettings.channelUsed[2];
    input.ch2PriorityValue = TOSLINKSwitcherSettings.channelPriorityValue[2];
    
    input.ch3Used = TOSLINKSwitcherSettings.channelUsed[3];
    input.ch3PriorityValue = TOSLINKSwitcherSettings.channelPriorityValue[3];
    
    savePersistedSettings(&input);
}

void restoreDefaultPriorityConfigSettings(void)
{
    SettingsPersistenceStruct settings;
    generateDefaultSettings(&settings);
    updateSystemFields(&settings);
    savePriorityConfigToPersistence();
}

static void updateSystemFields(SettingsPersistenceStruct* dataStruct)
{
    TOSLINKSwitcherSettings.channelPriorityValue[0] = dataStruct->ch0PriorityValue;
    TOSLINKSwitcherSettings.channelPriorityValue[1] = dataStruct->ch1PriorityValue;
    TOSLINKSwitcherSettings.channelPriorityValue[2] = dataStruct->ch2PriorityValue;
    TOSLINKSwitcherSettings.channelPriorityValue[3] = dataStruct->ch3PriorityValue;
    
    TOSLINKSwitcherSettings.channelUsed[0] = dataStruct->ch0Used;
    TOSLINKSwitcherSettings.channelUsed[1] = dataStruct->ch1Used;
    TOSLINKSwitcherSettings.channelUsed[2] = dataStruct->ch2Used;
    TOSLINKSwitcherSettings.channelUsed[3] = dataStruct->ch3Used;
}

