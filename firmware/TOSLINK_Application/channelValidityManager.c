/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "channelValidityManager.h"
#include "priorityConfig.h"
#include "muxManager.h"

void updateChannelTransitionsCount(void)
{
    static unsigned char prevch0, prevch1, prevch2, prevch3;
   /*Check all 4 channels for signal transitions*/
    if(prevch0 != getChannelInputGPIOState(ch0Input)) /*Channel input value changed since last iteration*/
    {
        prevch0 = !prevch0;
        if(TOSLINKSwitcherSettings.channelUsed[0])
        {
            TOSLINKSwitcherSettings.channelsValidityCounter[0]++;	/*Increment the number of successful registered value changes*/
        }
    }

    if(prevch1 != getChannelInputGPIOState(ch1Input))
    {
        prevch1 = !prevch1;
        if(TOSLINKSwitcherSettings.channelUsed[1])
        {
            TOSLINKSwitcherSettings.channelsValidityCounter[1]++;
        }
    }

    if(prevch2 != getChannelInputGPIOState(ch2Input))
    {
        prevch2 = !prevch2;
        if(TOSLINKSwitcherSettings.channelUsed[2])
        {
            TOSLINKSwitcherSettings.channelsValidityCounter[2]++;
        }
    }

    if(prevch3 != getChannelInputGPIOState(ch3Input))
    {
        prevch3 = !prevch3;
        if(TOSLINKSwitcherSettings.channelUsed[3])
        {
            TOSLINKSwitcherSettings.channelsValidityCounter[3]++;
        }
    }
}

char  nextValidChannel(bool prioritize)
{
    unsigned char i;
    unsigned char channelSelector = TOSLINKSwitcherSettings.chState; //Start with current channel.
    
    for(i = 0; i < NBINPUTCHANNELS; i++)
    {
        if(prioritize) //Next automatic channel selection is based on input channel priorities?
        {
            if(TOSLINKSwitcherSettings.validChannelsArray[i] == true && (TOSLINKSwitcherSettings.validChannelsArray[channelSelector] == false || 
               TOSLINKSwitcherSettings.channelPriorityValue[i] > TOSLINKSwitcherSettings.channelPriorityValue[channelSelector]))
            {   
                //If new channel is valid and priority is set higher than previously selected channel.
                channelSelector = i;
            }
            //If no channel is valid, function will exit with same channel as function's input.
        }
        else
        {
            channelSelector = (TOSLINKSwitcherSettings.chState + i + 1) % NBINPUTCHANNELS;  //Don't check same channel at first.
            if(TOSLINKSwitcherSettings.validChannelsArray[channelSelector] == true) //If no channel is valid. It will simply roll back to selected channel when we entered that function.
            {
                break;
            }
        }
    }
    return channelSelector;
}

void refreshChannelsValidStates(void)
{
    unsigned char i;
	bool CurrentChannel;
	
    for(i = 0; i < NBINPUTCHANNELS; i++) //Check all input channels
    {
		CurrentChannel = LEDOFF; //Arbitrary threshold to consider an input channel valid.
        
        if(TOSLINKSwitcherSettings.channelsValidityCounter[i] >= CHANNELVALIDITYTHRESHOLD)
        {
            CurrentChannel = LEDON;
        }
        
        TOSLINKSwitcherSettings.validChannelsArray[i] = CurrentChannel;                 //Update to indicate if said channel is valid or not.
        
        if(i == TOSLINKSwitcherSettings.chState) //Update corresponding green LED accordingly but skip currently active channel.
        {
            CurrentChannel = LEDOFF;                                //Force OFF green LED of active channel
        }
        
        switch(i)
        {                                                 
            case 0:
                setLEDGPIOState(ch0led, CurrentChannel);
                break;
            case 1:
                setLEDGPIOState(ch1led, CurrentChannel);
                break;
            case 2:
                setLEDGPIOState(ch2led, CurrentChannel);
                break;
            case 3:
                setLEDGPIOState(ch3led, CurrentChannel);
                break;
            default : break;
        }
		
		TOSLINKSwitcherSettings.channelsValidityCounter[i] = 0;                             //Reset corresponding counter for next channel vailidity verification pass
	}
}

void toggleChannelActivation(bool desiredState)
{
    if(desiredState == false && TOSLINKSwitcherSettings.nbChannelsTurnedOff < (NBINPUTCHANNELS - 1))
    {
        if(TOSLINKSwitcherSettings.channelUsed[TOSLINKSwitcherSettings.chState] != desiredState)
        {
            TOSLINKSwitcherSettings.channelUsed[TOSLINKSwitcherSettings.chState] = desiredState;
            TOSLINKSwitcherSettings.nbChannelsTurnedOff++;
        }
    }
    else if(desiredState == true)
    {
        if(TOSLINKSwitcherSettings.channelUsed[TOSLINKSwitcherSettings.chState] != desiredState)
        {
            TOSLINKSwitcherSettings.channelUsed[TOSLINKSwitcherSettings.chState] = desiredState;
            TOSLINKSwitcherSettings.nbChannelsTurnedOff--;
        }
    }
}