/* 
Automatic TOSLINK audio switcher
Copyright (C) 2016  Benjamin Fiset-Deschênes
contact@benjaminfd.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "TOSLINK_switcher.h"
#include "muxManager.h"
#include "channelValidityManager.h"
#include "../HAL/gpioWrapper.h"

#ifdef BOARD_REV1
#define channel0Active setMuxGPIOState(a0, false); setMuxGPIOState(a1, false); //a0 = 0; a1 = 0;
#define channel1Active setMuxGPIOState(a0, true); setMuxGPIOState(a1, false); //a0 = 1; a1 = 0;
#define channel2Active setMuxGPIOState(a0, false); setMuxGPIOState(a1, true); //a0 = 0; a1 = 1;
#define channel3Active setMuxGPIOState(a0, true); setMuxGPIOState(a1, true); //a0 = 1; a1 = 1;
#elif BOARD_REV2
#define channel0Active setMuxGPIOState(a0, true); setMuxGPIOState(a1, true); //a0 = 1; a1 = 1;
#define channel1Active setMuxGPIOState(a0, false); setMuxGPIOState(a1, false); //a0 = 0; a1 = 0;
#define channel2Active setMuxGPIOState(a0, true); setMuxGPIOState(a1, false); //a0 = 1; a1 = 0;
#define channel3Active setMuxGPIOState(a0, false); setMuxGPIOState(a1, true); //a0 = 0; a1 = 1;
#else
#error Specify a board revision to build proper firmware.
#endif

static unsigned char lockedChannelStateFromManualOp = false;    /*Will turn true when in Auto mode and manual channel 
                                                  selection switch has been used. Will turn back to 
                                                  false when selected channel becomes invalid.*/

void muxControl(void)
{
    setMuxGPIOState(muxEnable, false);  //Active low
	switch(TOSLINKSwitcherSettings.chState)
    {
		case 1:
            channel1Active
			break;

		case 2:
			channel2Active
			break;

		case 3:
			channel3Active
			break;
        case 0:
		default :
			channel0Active
			break;
	}
}

void userRequestChangeChannel(void)
{
    unsigned char newChState;
    
    if(TOSLINKSwitcherSettings.systemState == SystemState_PriorityConfig) //If not in priorityConfig system state
    {
        TOSLINKSwitcherSettings.chState = (TOSLINKSwitcherSettings.chState + 1) % NBINPUTCHANNELS;            //Simply move to next channel in line.
    }
    else if(TOSLINKSwitcherSettings.mode == AUTOOFF)//If not in Auto mode
    {
        for(newChState = 0; newChState < NBINPUTCHANNELS; newChState++) //Re-use variables
        {
            TOSLINKSwitcherSettings.chState = (TOSLINKSwitcherSettings.chState + 1) % NBINPUTCHANNELS;//Next channel
            if(TOSLINKSwitcherSettings.channelUsed[TOSLINKSwitcherSettings.chState]) //Is used
            {
                break;                                                              //We're done here
            }
        }
    }
    else
    {
        newChState = nextValidChannel(false);
        if(newChState != TOSLINKSwitcherSettings.chState) //Is it the same channel we're currently on?
        {
            lockedChannelStateFromManualOp = true;  //Only lock if there's another valid channel to select
            TOSLINKSwitcherSettings.chState = newChState;
        }
    }
}

void autoModeChangeChannelAssert(void)
{
    if(TOSLINKSwitcherSettings.mode == AUTOON)/*Only if in automatic mode*/
    {
        if(lockedChannelStateFromManualOp) //If in locked mode
        {
            if(TOSLINKSwitcherSettings.validChannelsArray[TOSLINKSwitcherSettings.chState] == false) //Current channel is no longer valid
            {
                TOSLINKSwitcherSettings.chState = nextValidChannel(true);	/*Re-evaluate new valid channel based on priority*/
                lockedChannelStateFromManualOp = false; //Unlock automatic mode.
            }
        }
        else
        {
            TOSLINKSwitcherSettings.chState = nextValidChannel(true);	/*Re-evaluate new valid channel based on priority*/
        }		
    }
}
