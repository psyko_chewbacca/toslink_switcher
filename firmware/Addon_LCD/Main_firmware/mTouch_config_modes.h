/*************************************************************************
 *  � 2012 Microchip Technology Inc.                                       
 *  
 *  Project Name:    mTouch Framework v2.3
 *  FileName:        mTouch_config_modes.h
 *  Dependencies:    
 *  Processor:       See documentation for supported PIC� microcontrollers 
 *  Compiler:        HI-TECH Ver. 9.81 or later
 *  IDE:             MPLAB� IDE v8.50 (or later) or MPLAB� X                        
 *  Hardware:         
 *  Company:         
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Description:     mTouch Framework Scanning Modes Configuration File
 *                   - See documentation for better explanations of all
 *                     configuration options.
 *************************************************************************/
/***********************************************************************
 * MICROCHIP SOFTWARE NOTICE AND DISCLAIMER: You may use this software, and 
 * any derivatives created by any person or entity by or on your behalf, 
 * exclusively with Microchip's products in accordance with applicable
 * software license terms and conditions, a copy of which is provided for
 * your referencein accompanying documentation. Microchip and its licensors 
 * retain all ownership and intellectual property rights in the 
 * accompanying software and in all derivatives hereto. 
 * 
 * This software and any accompanying information is for suggestion only. 
 * It does not modify Microchip's standard warranty for its products. You 
 * agree that you are solely responsible for testing the software and 
 * determining its suitability. Microchip has no obligation to modify, 
 * test, certify, or support the software. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE, ITS INTERACTION WITH 
 * MICROCHIP'S PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY 
 * APPLICATION. 
 * 
 * IN NO EVENT, WILL MICROCHIP BE LIABLE, WHETHER IN CONTRACT, WARRANTY, 
 * TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), STRICT 
 * LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, 
 * SPECIAL, PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, 
 * FOR COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, 
 * HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY 
 * OR THE DAMAGES ARE FORESEEABLE. TO THE FULLEST EXTENT ALLOWABLE BY LAW, 
 * MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS 
 * SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID 
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE. 
 * 
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF 
 * THESE TERMS. 
 *************************************************************************/
/** @file   mTouch_config_modes.h
*   @brief  Framework Configuration! Scanning mode options.
*/
 
 
    #define MTOUCH_NUM_MODES                    0
    
        #define MTOUCH_MODE0_NUM_SENSORS        3
        
            #define MTOUCH_MODE0_SENSOR0        0
            #define MTOUCH_MODE0_SENSOR1        1
            #define MTOUCH_MODE0_SENSOR2        2
    
    
        #define MTOUCH_MODE1_NUM_SENSORS        1
    
            #define MTOUCH_MODE1_SENSOR0        0
            #define MTOUCH_MODE1_SENSOR1        1
            #define MTOUCH_MODE1_SENSOR2        2
            

        #define MTOUCH_MODE2_NUM_SENSORS        2
    
            #define MTOUCH_MODE2_SENSOR0        0
            #define MTOUCH_MODE2_SENSOR1        1
            #define MTOUCH_MODE2_SENSOR2        2
    
    
        #define MTOUCH_MODE3_NUM_SENSORS        1
    
            #define MTOUCH_MODE3_SENSOR0        0
            #define MTOUCH_MODE3_SENSOR1        1
            #define MTOUCH_MODE3_SENSOR2        2
            
            

        #define MTOUCH_MODE4_NUM_SENSORS        0
    
            #define MTOUCH_MODE4_SENSOR0        0
            #define MTOUCH_MODE4_SENSOR1        1
            #define MTOUCH_MODE4_SENSOR2        2
            
            
        #define MTOUCH_MODE5_NUM_SENSORS        0
    
            #define MTOUCH_MODE5_SENSOR0        0
            #define MTOUCH_MODE5_SENSOR1        1
            #define MTOUCH_MODE5_SENSOR2        2


        #define MTOUCH_MODE6_NUM_SENSORS        0
    
            #define MTOUCH_MODE6_SENSOR0        0
            #define MTOUCH_MODE6_SENSOR1        1
            #define MTOUCH_MODE6_SENSOR2        2
            

        #define MTOUCH_MODE7_NUM_SENSORS        0
    
            #define MTOUCH_MODE7_SENSOR0        0
            #define MTOUCH_MODE7_SENSOR1        1
            #define MTOUCH_MODE7_SENSOR2        2
