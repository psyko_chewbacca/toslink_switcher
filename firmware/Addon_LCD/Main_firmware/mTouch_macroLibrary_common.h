/*************************************************************************
 *  � 2012 Microchip Technology Inc.                                       
 *  
 *  Project Name:    mTouch Framework v2.3
 *  FileName:        mTouch_macroLibrary_common.h
 *  Dependencies:    mTouch.h
 *  Processor:       See documentation for supported PIC� microcontrollers 
 *  Compiler:        HI-TECH Ver. 9.81 or later
 *  IDE:             MPLAB� IDE v8.50 (or later) or MPLAB� X                        
 *  Hardware:         
 *  Company:         
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Description:     mTouch Framework Common Macro Library Header File
 *                   - Editting this file will make customer support for
 *                     your application extremely difficult. Be careful,
 *                     as it can also have a negative impact on your
 *                     system's noise susceptibility.
 *************************************************************************/
 /***********************************************************************
 * MICROCHIP SOFTWARE NOTICE AND DISCLAIMER: You may use this software, and 
 * any derivatives created by any person or entity by or on your behalf, 
 * exclusively with Microchip's products in accordance with applicable
 * software license terms and conditions, a copy of which is provided for
 * your referencein accompanying documentation. Microchip and its licensors 
 * retain all ownership and intellectual property rights in the 
 * accompanying software and in all derivatives hereto. 
 * 
 * This software and any accompanying information is for suggestion only. 
 * It does not modify Microchip's standard warranty for its products. You 
 * agree that you are solely responsible for testing the software and 
 * determining its suitability. Microchip has no obligation to modify, 
 * test, certify, or support the software. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE, ITS INTERACTION WITH 
 * MICROCHIP'S PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY 
 * APPLICATION. 
 * 
 * IN NO EVENT, WILL MICROCHIP BE LIABLE, WHETHER IN CONTRACT, WARRANTY, 
 * TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), STRICT 
 * LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, 
 * SPECIAL, PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, 
 * FOR COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, 
 * HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY 
 * OR THE DAMAGES ARE FORESEEABLE. TO THE FULLEST EXTENT ALLOWABLE BY LAW, 
 * MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS 
 * SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID 
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE. 
 * 
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF 
 * THESE TERMS. 
 *************************************************************************/
/** @file   mTouch_macroLibrary_common.h
*   @brief  Implements non-core-specific acquisition macros
*/

/**
* @def      MASKBANK(var,bank)
* @param    var     the variable address to mask
* @param    bank    the bank location of the variable
* @brief    Masks the bank out of the variable address for ASM code usage. 
*           Requires that the actual bank bits being masked equal the bank 
*           value provided.
* @hideinitializer
*/
#define MASKBANK(var,bank)      ((var)^((bank)*80h))
/**
* @def      NOBANK(var)
* @param    var     the variable address to mask
* @brief    Masks the bank out of the variable address for ASM code usage.
* @hideinitializer
*/
#define NOBANK(var)             (var & 0x7F)

/** @name Housekeeping Macros
*
*   These macros perform basic ISR and timer module functions such as checking
*   that the interrupt flag is set and reloading the TMR0 counter.
*/
//@{

#if defined(MTOUCH_ERROR_DETECTION_ENABLED)         

    #define MTOUCH_onDETECTACTION()                  mTouch_state.error = 1     

    #define MTOUCH_CHECKforSHORTonSENSOR(index)                                                                             \
        __paste2(MTOUCH_LAT_C_SENSOR(index),MTOUCH_PIN_SENSOR(index))   = 1;        /* Translates to: LATA0  = 1;       */  \
        __paste2(MTOUCH_TRIS_C_SENSOR(index),MTOUCH_PIN_SENSOR(index))  = 0;        /* Translates to: TRISA0 = 0;       */  \
        asm("NOP"); asm("NOP");                                                     /*                                  */  \
        __paste2(MTOUCH_TRIS_C_SENSOR(index),MTOUCH_PIN_SENSOR(index))  = 1;        /* Translates to: TRISA0 = 1;       */  \
        if (__paste2(MTOUCH_PORT_C_SENSOR(index),MTOUCH_PIN_SENSOR(index)) == 0)    /* Translates to: if(PORTA0 == 0)   */  \
            MTOUCH_onDETECTACTION();                                                /*                                  */  \
                                                                                    /*                                  */  \
        __paste2(MTOUCH_LAT_C_SENSOR(index),MTOUCH_PIN_SENSOR(index))   = 0;        /* Translates to: LATA0  = 0;       */  \
        __paste2(MTOUCH_TRIS_C_SENSOR(index),MTOUCH_PIN_SENSOR(index))  = 0;        /* Translates to: TRISA0 = 0;       */  \
        asm("NOP"); asm("NOP");                                                     /*                                  */  \
        __paste2(MTOUCH_TRIS_C_SENSOR(index),MTOUCH_PIN_SENSOR(index))  = 1;        /* Translates to: TRISA0 = 1;       */  \
        if (__paste2(MTOUCH_PORT_C_SENSOR(index),MTOUCH_PIN_SENSOR(index)) == 1)    /* Translates to: if(PORTA0 == 1)   */  \
            MTOUCH_onDETECTACTION();

        
#endif

/**
* @def          SAVE_STATE()
* @brief        Executes any required actions to save the current main-loop 
*               process
* @hideinitializer
*/
#define SAVE_STATE()                    \
    do {                                \
        asm("movwf  _int_w");           \
        asm("swapf  _int_w, F");        \
        asm("movf   STATUS, W");        \
        asm("clrf   STATUS");           \
        asm("movwf  _int_status");      \
        asm("movf   PCLATH, W");        \
        asm("clrf   PCLATH");           \
        asm("movwf  _int_pclath");      \
        asm("movf   FSR, W");           \
        asm("movwf  _int_fsr");         \
    } while (0)
// The Enhanced Midrange Core does not require saving/restoring state on entering
// the ISR, so these macros will be undefined later for those parts. 
// They remain implemented in the non-enhanced core version.

/**
* @def          RESTORE_STATE()
* @brief        Executes any required actions to save the current main-loop process
* @hideinitializer
*/
#define RESTORE_STATE()                 \
    do {                                \
        asm("movf   _int_fsr,W");       \
        asm("movwf  FSR");              \
        asm("movf   _int_pclath,W");    \
        asm("movwf  PCLATH");           \
        asm("movf   _int_status,W");    \
        asm("movwf  STATUS");           \
        asm("swapf  _int_w, W");        \
    } while (0)
// The Enhanced Midrange Core does not require saving/restoring state on entering
// the ISR, so these macros will be undefined later for those parts. 
// They remain implemented in the non-enhanced core version.

/**
* @def          MTOUCH_SLEEP_STABILIZE_OSC()
* @brief        Causes the system to wait until the oscillator's frequency has stabilized
* @hideinitializer
*/
#define MTOUCH_SLEEP_STABILIZE_OSC()            \
    do {                                        \
        while (!HFIOFL);                        \
    } while(0)


#if defined(MTOUCH_JITTER_ENABLE)

    #if !defined(MTOUCH_EEPROM_ENABLED)
        /**
        * @def      JITTER_START_TIME()
        * @brief    Calculates a random value and uses it to seed the mTouch interrupt timer
        *
        *   Implements a linear feedback shift register algorithm to increase the randomness 
        *   of the jitter function. This implementation costs one byte of RAM.
        *
        * @hideinitializer
        */
        #define JITTER_START_TIME()                                                                                             \
            do {                                                                                                                \
                asm("BANKSEL    _mTouch_jitter"                     );  /* Make sure we're starting in the correct bank     */  \
                asm("bcf    "   ___mkstr(STATUS)            ",0"    );  /* Clear the carry bit                              */  \
                asm("rrf    "   ___mkstr(_mTouch_jitter)    ",W"    );  /* Right shift the current jitter seed value        */  \
                asm("btfsc  "   ___mkstr(STATUS)            ",0"    );  /* Check the carry bit - if set, perform XOR        */  \
                asm("xorlw      0xB4"                               );  /* (cond) XOR the jitter seed with 0xB4             */  \
                asm("movwf  "   ___mkstr(_mTouch_jitter)            );  /* Store the result as the new jitter seed value    */  \
                asm("andlw  "   ___mkstr(MTOUCH_JITTER_MASK)        );  /* Mask the seed value to limit the number of bits  */  \
                asm("BANKSEL "  ___mkstr(MTOUCH_ISR_TMR_ASM)        );  /* Move to Bank 0 to access timer SFR               */  \
                asm("clrf   "   ___mkstr(NOBANK(MTOUCH_ISR_TMR_ASM)));  /* Clear timer                                      */  \
                asm("addwf  "   ___mkstr(NOBANK(MTOUCH_ISR_TMR_ASM)));  /* Add the masked LFSR value as an offset to timer  */  \
            } while (0)

            
        /**
        * @def      JITTER_MAIN_LOOP() 
        * @brief    Calculates a random value and uses it to jitter the mTouch scanning 
        *           routine when operating out of the main loop.
        *
        *   Implements a linear feedback shift register algorithm to increase the randomness 
        *   of the jitter function. This implementation costs one byte of RAM.
        *
        * @hideinitializer
        */
        #define JITTER_MAIN_LOOP()                                                                                          \
            do {                                                                                                            \
                asm("BANKSEL    _mTouch_jitter"                 );  /* Make sure we're starting in the correct bank     */  \
                asm("bcf    "   ___mkstr(STATUS)            ",0");  /* Clear the carry bit                              */  \
                asm("rrf    "   ___mkstr(_mTouch_jitter)    ",W");  /* Right shift the current jitter seed value        */  \
                asm("btfsc  "   ___mkstr(STATUS)            ",0");  /* Check the carry bit - if set, perform XOR        */  \
                asm("xorlw      0xB4"                           );  /* (cond) XOR the jitter seed with 0xB4             */  \
                asm("movwf  "   ___mkstr(_mTouch_jitter)        );  /* Store the result as the new jitter seed value    */  \
                for (uint8_t i = (mTouch_jitter & MTOUCH_JITTER_MASK); i > 0; i--); /* Delay loop                       */  \
            } while (0)
    
    #else
    
        #define JITTER_START_TIME()                                                                 \
            do {                                                                                    \
                if (mTouch_jitter & 0x01) {                                                         \
                    mTouch_jitter = (uint8_t)(mTouch_jitter >> 1) ^ 0xB4;                           \
                } else {                                                                            \
                    mTouch_jitter = (uint8_t)(mTouch_jitter >> 1);                                  \
                }                                                                                   \
                MTOUCH_ISR_TMR_C = mTouch_jitter & MTOUCH_EEPROM_read(MTOUCH_EEPROM_JITTER_ADR);    \
            } while(0)
            
        #define JITTER_MAIN_LOOP()                                          \
            do {                                                            \
                if (mTouch_jitter & 0x01) {                                 \
                    mTouch_jitter = (uint8_t)(mTouch_jitter >> 1) ^ 0xB4;   \
                } else {                                                    \
                    mTouch_jitter = (uint8_t)(mTouch_jitter >> 1);          \
                }                                                           \
                for (uint8_t i = mTouch_jitter & MTOUCH_EEPROM_read(MTOUCH_EEPROM_JITTER_ADR); i > 0; i--); \
            } while(0)
            
    #endif
        
#else                                                       // Do not implement the jittering function
    #define JITTER_START_TIME()     \
        do {                        \
            MTOUCH_ISR_TMR_C = 0;   \
        } while(0) 
    #define JITTER_MAIN_LOOP()
#endif


#define MTOUCH_SCANFUNCTIONA(index)             __paste(mTouch_ScanA_, index)
#define MTOUCH_SCANFUNCTIONB(index)             __paste(mTouch_ScanB_, index)

/**
* @def          CVD_SCANA_GEN(index, indexRef)
* @param[in]    index       the index of the sensor to be scanned
* @param[in]    indexRef    the index of the sensor to be used as the reference
* @brief        Function-generating macro called by MTOUCH_SCAN_FUNCTION(i). Do not use directly.
*
*   This macro creates a new CVD 'Scan A' function based on the index value provided.
*   The function's name will be mTouch_ScanA_#().
*
* @hideinitializer
*/
#define CVD_SCANA_GEN(index, indexRef)          void MTOUCH_SCANFUNCTIONA(index)(void)                                                  \
                                                {                                                                                       \
                                                    CVD_RS_MUTUAL_A(index);                 /*  Set up mutual sensor, if enabled    */  \
                                                    CVD_RS_INIT_A(indexRef);                /*  Set up charge reference             */  \
                                                    CVD_RS_MUXSELECT(index, indexRef);      /*  ADC Mux --> Reference               */  \
                                                    CVD_RS_PREPARE(index);                  /*  Initialize FSRs                     */  \
                                                    CVD_DELAY_CHOLD();                      /*  Delay while CHOLD charges           */  \
                                                    CVD_RS_CONNECT_A(index);                /*  Connect CHOLD and external sensor   */  \
                                                    CVD_DELAY_SETTLE();                     /*  Delay while voltage averages        */  \
                                                    CVD_RS_SET_GODONE();                    /*  Begin ADC conversion                */  \
                                                    CVD_GO_DONE_DELAY();                    /*  Delay while CHOLD disconnects       */  \
                                                    CVD_RS_EXIT_A(index);                   /*  Prepare sensor for Scan B           */  \
                                                }
/**
* @def          CVD_SCANB_GEN(index, indexRef)
* @param[in]    index       the index of the sensor to be scanned
* @param[in]    indexRef    the index of the sensor to be used as the reference
* @brief        Function-generating macro called by MTOUCH_SCAN_FUNCTION(i). Do not use directly.
*
*   This macro creates a new CVD 'Scan B' function based on the index value provided.
*   The function's name will be mTouch_ScanB_#().
*
* @hideinitializer
*/
#define CVD_SCANB_GEN(index, indexRef)          void MTOUCH_SCANFUNCTIONB(index)(void)                                                  \
                                                {                                                                                       \
                                                    CVD_RS_MUTUAL_B(index);                 /*  Set up mutual sensor, if enabled    */  \
                                                    CVD_RS_INIT_B(indexRef);                /*  Set up charge reference             */  \
                                                    CVD_RS_MUXSELECT(index, indexRef);      /*  ADC Mux --> Reference               */  \
                                                    CVD_RS_PREPARE(index);                  /*  Initialize FSRs                     */  \
                                                    CVD_DELAY_CHOLD();                      /*  Delay while CHOLD charges           */  \
                                                    CVD_RS_CONNECT_B(index);                /*  Connect CHOLD and external sensor   */  \
                                                    CVD_DELAY_SETTLE();                     /*  Delay while voltage averages        */  \
                                                    CVD_RS_SET_GODONE();                    /*  Begin ADC conversion                */  \
                                                    CVD_GO_DONE_DELAY();                    /*  Delay while CHOLD disconnects       */  \
                                                    CVD_RS_EXIT_B(index);                   /*  Prepare sensor for exitting ISR     */  \
                                                }

#define MTOUCH_SCAN_FUNCTION(index)             CVD_SCANA_GEN(index, __paste(CVD_REFSENSOR_, index));  \
                                                CVD_SCANB_GEN(index, __paste(CVD_REFSENSOR_, index))

#define MTOUCH_SCAN_PROTOTYPE(index)            void MTOUCH_SCANFUNCTIONA(index)(void);   \
                                                void MTOUCH_SCANFUNCTIONB(index)(void);

#if     MTOUCH_NUMBER_SENSORS == 1
#define MTOUCH_SCANSEQUENCE_VAR_INIT    { {&MTOUCH_SCANFUNCTIONA(0), &MTOUCH_SCANFUNCTIONB(0)} }
#define MTOUCH_SCANA_VAR_INIT           { &MTOUCH_SCANFUNCTIONA(0) }
#define MTOUCH_SCANB_VAR_INIT           { &MTOUCH_SCANFUNCTIONB(0) }
#define MTOUCH_SCANMODE_VAR_INIT        { 1, {&mTouch_sensorScans[0]} }
#define MTOUCH_SCAN_PROTOTYPES()        MTOUCH_SCAN_PROTOTYPE(0)
#define MTOUCH_SCAN_FUNCTIONS()         MTOUCH_SCAN_FUNCTION(0)
#elif   MTOUCH_NUMBER_SENSORS == 2
#define MTOUCH_SCANSEQUENCE_VAR_INIT    { {&MTOUCH_SCANFUNCTIONA(0), &MTOUCH_SCANFUNCTIONB(0)}, {&MTOUCH_SCANFUNCTIONA(1), &MTOUCH_SCANFUNCTIONB(1)} }
#define MTOUCH_SCANA_VAR_INIT           { &MTOUCH_SCANFUNCTIONA(0), &MTOUCH_SCANFUNCTIONA(1) }
#define MTOUCH_SCANB_VAR_INIT           { &MTOUCH_SCANFUNCTIONB(0), &MTOUCH_SCANFUNCTIONB(1) }
#define MTOUCH_SCANMODE_VAR_INIT        { 2, {&mTouch_sensorScans[0], &mTouch_sensorScans[1]} }
#define MTOUCH_SCAN_PROTOTYPES()        MTOUCH_SCAN_PROTOTYPE(0); MTOUCH_SCAN_PROTOTYPE(1)
#define MTOUCH_SCAN_FUNCTIONS()         MTOUCH_SCAN_FUNCTION(0); MTOUCH_SCAN_FUNCTION(1)
#elif   MTOUCH_NUMBER_SENSORS == 3
#define MTOUCH_SCANSEQUENCE_VAR_INIT    { {&MTOUCH_SCANFUNCTIONA(0), &MTOUCH_SCANFUNCTIONB(0)}, {&MTOUCH_SCANFUNCTIONA(1), &MTOUCH_SCANFUNCTIONB(1)}, {&MTOUCH_SCANFUNCTIONA(2), &MTOUCH_SCANFUNCTIONB(2)} }
#define MTOUCH_SCANA_VAR_INIT           { &MTOUCH_SCANFUNCTIONA(0), &MTOUCH_SCANFUNCTIONA(1), &MTOUCH_SCANFUNCTIONA(2) }
#define MTOUCH_SCANB_VAR_INIT           { &MTOUCH_SCANFUNCTIONB(0), &MTOUCH_SCANFUNCTIONB(1), &MTOUCH_SCANFUNCTIONB(2) }
#define MTOUCH_SCANMODE_VAR_INIT        { 3, {&mTouch_sensorScans[0], &mTouch_sensorScans[1], &mTouch_sensorScans[2]} }
#define MTOUCH_SCAN_PROTOTYPES()        MTOUCH_SCAN_PROTOTYPE(0); MTOUCH_SCAN_PROTOTYPE(1); MTOUCH_SCAN_PROTOTYPE(2)
#define MTOUCH_SCAN_FUNCTIONS()         MTOUCH_SCAN_FUNCTION(0); MTOUCH_SCAN_FUNCTION(1); MTOUCH_SCAN_FUNCTION(2)
#endif



#if !defined(PIC_ADC_BITS)
    #warning The hardware profile for this device has not defined the ADC resolution in bits. Defaulting to 10.
    #define PIC_ADC_BITS 10
#endif
//@}

/** @name   Timing Critical CVD Scanning Routine Macros
*
*   These macros implement the CVD scanning routine and basic acquisition-level filtering 
*   techniques. Editting these macros could reduce the noise immunity of the system.
*/
//@{


    
//@}


/** @name       Polishing/Finishing Macros
*
*   These macros perform scaling and final operations before setting the 
*   #mTouchCVD_dataReady flag and allowing the main loop application to process
*   the new data.
*/
//@{

/**
* @def          STORE_SENSOR_SCALE(index, shift)
* @brief        Do not use directly. Expanded utility macro called by STORE_SENSOR(index). 
* @param[in]    index   the index of the sensor to be scaled and stored
* @param[in]    shift   the number of times to shift the accumulator value to the right before storing its value in the mTouch_sensorData array
* @hideinitializer
*/
#define STORE_SENSOR_SCALE(index, shift)                                                                \
    do {                                                                                                \
        mTouch_prevSensor           = &mTouch_acqData[index];                                           \
        mTouch_sensorData[index-1]  = (uint16_t)(mTouch_GetAccumulator(mTouch_prevSensor) >> shift);    \
        mTouch_ClearAccumulator(mTouch_prevSensor);                                                     \
    } while(0)
    
/**
* @def          STORE_LAST_SENSOR_SCALE(index, shift)
* @brief        Do not use directly. Expanded utility macro called by STORE_SENSOR(index). 
* @param[in]    index   the index of the sensor to be scaled and stored
* @param[in]    shift   the number of times to shift the accumulator value to the right before storing its value in the mTouch_sensorData array
* @hideinitializer
*/
#define STORE_LAST_SENSOR_SCALE(index, shift)                                                                       \
    do {                                                                                                            \
        mTouch_prevSensor    = &mTouch_acqData[index];                                                              \
        mTouch_sensorData[MTOUCH_NUMBER_SENSORS-1] = (uint16_t)(mTouch_GetAccumulator(mTouch_prevSensor) >> shift); \
        mTouch_ClearAccumulator(mTouch_prevSensor);                                                                 \
    } while(0)
    
/**
* @def          STORE_SINGLE_SENSOR_SCALE(index, shift)
* @brief        Do not use directly. Expanded utility macro called by STORE_SENSOR(index) if there is only one sensor in the mTouch application. 
* @param[in]    index   the index of the sensor to be scaled and stored
* @param[in]    shift   the number of times to shift the accumulator value to the right before storing its value in the mTouch_sensorData array
* @hideinitializer
*/
#define STORE_SINGLE_SENSOR_SCALE(index, shift)                                                 \
    do {                                                                                        \
        mTouch_sensorData[0] = (uint16_t)(mTouch_GetAccumulator(mTouch_prevSensor) >> shift);   \
        mTouch_ClearAccumulator(mTouch_prevSensor);                                             \
    } while(0)

/**
* @def          STORE_SENSOR(index)
* @ingroup      Acquisition
* @param[in]    index   the index of the sensor to be scaled and stored
* @brief        Scales the accumulated result of the CVD acquisition and stores it for main-application access
*
*   This macro uses the MTOUCH_SCALING configuration option to determine which scaling option to choose.
* 
* @hideinitializer
*/
#if (MTOUCH_NUMBER_SENSORS > 1)
    #if (MTOUCH_SCALING >= 0) && (MTOUCH_SCALING <= 8)
        #define STORE_SENSOR(index)             STORE_SENSOR_SCALE(index, MTOUCH_SCALING_VALUE) 
        #define STORE_LAST_SENSOR(index)        STORE_LAST_SENSOR_SCALE(index, MTOUCH_SCALING_VALUE) 
    #else
        #error MTOUCH_SCALING must be set to a value between 0 and 8.
    #endif
#else
    #if (MTOUCH_SCALING >= 0) && (MTOUCH_SCALING <= 8)
        #define STORE_SENSOR(index)             STORE_SINGLE_SENSOR_SCALE(index, MTOUCH_SCALING_VALUE) 
    #else
        #error MTOUCH_SCALING must be set to a value between 0 and 8.
    #endif
#endif

/**
* @def          EXIT_SENSOR(index)
* @ingroup      Acquisition
* @param[in]    index   the index variable to increment
* @brief        Increments the given index variable and exits the ISR
* @hideinitializer
*/
#if (MTOUCH_ADC_CONTROL == MTOUCH_ALWAYS_CONTROLS_ADC)
    #define EXIT_SENSOR(index)                                  \
        do {                                                    \
            index++;                                            \
            asm("ljmp END_MTOUCH_SCAN");    /* Exit the ISR */  \
        } while(0)
#else
    #define EXIT_SENSOR(index)                                  \
        do {                                                    \
            index++;                                            \
            WAIT_FOR_GODONE_BIT();                              \
            MTOUCH_STORE_SCAN_B();                              \
            asm("ljmp END_MTOUCH_SCAN");    /* Exit the ISR */  \
        } while(0)
#endif

/**
* @def          EXIT_LAST_SENSOR(index)
* @ingroup      Acquisition
* @param[in]    index   the index variable to reset to 0
* @brief        Performs several important functions after all sensors have been scanned
*
*   Resets the index variable to 0 for the next scan, decrements the oversampling
*   counter to determine if it is time to complete the sample and store the result,
*   and then exits the ISR.
*
* @hideinitializer
*/
/**
* @def          EXIT_LAST_SENSOR_DECINDEX(index) 
* @param[in]    index   the index variable to reset to 0
* @brief        Do not use directly. Utility macro used by EXIT_LAST_SENSOR() to reset the index variable.
* @hideinitializer
*/
/**
* @def          EXIT_LAST_SENSOR_MAIN()
* @brief        Do not use directly. Utility macro used by EXIT_LAST_SENSOR() to decrement the oversampling counter. 
* @hideinitializer
*/
#if (MTOUCH_ADC_CONTROL == MTOUCH_ALWAYS_CONTROLS_ADC)
    #if (MTOUCH_NUMBER_SENSORS > 1)
        #define EXIT_LAST_SENSOR(index)     EXIT_LAST_SENSOR_DECINDEX(index);   \
                                            EXIT_LAST_SENSOR_MAIN()
    #else
        #define EXIT_LAST_SENSOR(index)     EXIT_LAST_SENSOR_MAIN()             // Only one sensor, so only updates the sample counter
    #endif
#else
    #if (MTOUCH_NUMBER_SENSORS > 1)
        #define EXIT_LAST_SENSOR(index)     EXIT_LAST_SENSOR_DECINDEX(index);   \
                                            WAIT_FOR_GODONE_BIT();              \
                                            MTOUCH_STORE_SCAN_B();              \
                                            EXIT_LAST_SENSOR_MAIN()
    #else
        #define EXIT_LAST_SENSOR(index)     WAIT_FOR_GODONE_BIT();      \
                                            MTOUCH_STORE_SCAN_B();      \
                                            EXIT_LAST_SENSOR_MAIN()                                     
    #endif
#endif

#define EXIT_LAST_SENSOR_MAIN()                                                                                                                 \
do {                                                                                                                                            \
    if (--mTouch_stateVars.active.sampleCounter != 0) {                 /* Decrement the sample counter and check if it's equal to 0        */  \
        asm("ljmp END_MTOUCH_SCAN");                                    /* If not equal to 0, exit the ISR                                  */  \
    }                                                                                                                                           \
    mTouch_stateVars.active.sampleCounter = MTOUCH_SpS_VALUE;           /* If equal to 0, reset the sample counter continue with storage    */  \
} while(0)
    
#define EXIT_LAST_SENSOR_DECINDEX(index)        index = 0;

#define MTOUCH_EXIT_SCAN_FUNCTION(index)        __3paste(MTOUCH_EXIT_SENSOR,index,())

#define MTOUCH_INC_INDEX_AND_STORE_RESULT()                                     \
                mTouch_currentSensor++;                                         \
                mTouch_prevSensor    = &mTouch_acqData[mTouch_currentSensor];   \
                WAIT_FOR_GODONE_BIT();                                          \
                MTOUCH_STORE_SCAN_B();


/**
* @def          SET_DATA_READY_FLAG()
* @ingroup      Acquisition
* @brief        Sets the dataReady flag to signal the main application of a new reading
* @hideinitializer
*/
#define SET_DATA_READY_FLAG()               mTouch_state.dataReady = 1
//@}

#if (MTOUCH_NUMBER_SENSORS > 1)

    
    #define MTOUCH_CHECK_SHORT_0        MTOUCH_CHECKforSHORTonSENSOR(0)
    #define MTOUCH_CHECK_SHORT_1        MTOUCH_CHECKforSHORTonSENSOR(1)
    #define MTOUCH_CHECK_SHORT_2        MTOUCH_CHECKforSHORTonSENSOR(2)

    #if MTOUCH_NUMBER_SENSORS < 4  
        #undef  MTOUCH_CHECK_SHORT_3
        #define MTOUCH_CHECK_SHORT_3
    #endif
    #if MTOUCH_NUMBER_SENSORS < 3
        #undef  MTOUCH_CHECK_SHORT_2
        #define MTOUCH_CHECK_SHORT_2
    #endif
    #if MTOUCH_NUMBER_SENSORS < 2
        #undef  MTOUCH_CHECK_SHORT_1
        #define MTOUCH_CHECK_SHORT_1
    #endif
    
        
    #define MTOUCH_GENERATE_SHORTCHECKS()       \
        do {                                    \
            MTOUCH_CHECK_SHORT_0;               \
            MTOUCH_CHECK_SHORT_1;               \
            MTOUCH_CHECK_SHORT_2;               \
            MTOUCH_CHECK_SHORT_3;               \
        } while (0)
        
#endif

