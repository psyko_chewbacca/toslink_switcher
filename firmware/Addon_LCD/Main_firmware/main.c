/*************************************************************************
 *  � 2012 Microchip Technology Inc.                                       
 *  
 *  Project Name:    mTouch Framework v2.3
 *  FileName:        main.c
 *  Dependencies:    mTouch.h
 *  Processor:       See documentation for supported PIC� microcontrollers 
 *  Compiler:        HI-TECH Ver. 9.81 or later
 *  IDE:             MPLAB� IDE v8.50 (or later) or MPLAB� X                        
 *  Hardware:         
 *  Company:         
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Description:     Main routine
 *                   - Example implementation of the framework's API calls
 *                   - All application code should be implemented in this
 *                     or some other user-created file. This will allow
 *                     for better customer support and easier upgrades to
 *                     later firmware versions.
 *                   - See the documentation located in the docs/ folder
 *                     for a detailed guide on getting started with your
 *                     application and the mTouch framework.
 *************************************************************************/
/*************************************************************************
 * MICROCHIP SOFTWARE NOTICE AND DISCLAIMER: You may use this software, and
 * any derivatives created by any person or entity by or on your behalf,
 * exclusively with Microchip's products in accordance with applicable
 * software license terms and conditions, a copy of which is provided for
 * your referencein accompanying documentation. Microchip and its licensors
 * retain all ownership and intellectual property rights in the
 * accompanying software and in all derivatives hereto.
 *
 * This software and any accompanying information is for suggestion only.
 * It does not modify Microchip's standard warranty for its products. You
 * agree that you are solely responsible for testing the software and
 * determining its suitability. Microchip has no obligation to modify,
 * test, certify, or support the software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE, ITS INTERACTION WITH
 * MICROCHIP'S PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY
 * APPLICATION.
 *
 * IN NO EVENT, WILL MICROCHIP BE LIABLE, WHETHER IN CONTRACT, WARRANTY,
 * TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), STRICT
 * LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT,
 * SPECIAL, PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE,
 * FOR COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE,
 * HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY
 * OR THE DAMAGES ARE FORESEEABLE. TO THE FULLEST EXTENT ALLOWABLE BY LAW,
 * MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS
 * SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF
 * THESE TERMS.
 *************************************************************************/
/** @file   main.c
*   @brief  Example implementation of the mTouch Framework's API calls
*/
/// @cond

#include "mTouch.h"                            // Required Include

// CONFIGURATION SETTINGS
#include "generic_processorConfigBits.h"        // Provided for ease-of-development. 
                                                // Should not be used in an actual 
                                                // application.
#include "main/lcd_io.h"
#include "main/pksa.h"

// PROTOTYPES
    void            Example_System_Init (void);
    void interrupt  ISR                 (void);
    

// START PROGRAM
void main(void)
{
    //buttonPressedLatch_t hasBeenBackToZero;
    
    //buttonPressedLatch_t buttonsStateStruct;
    
    unsigned char autoManStateCount = 0;
    unsigned char channelStateCount = 0;
    unsigned char backlightStateCount = 0;
#define SensorStateChangeCountThreshold 15
#define SensorStateRollOverValue 128
    
    unsigned char autoManStayedAtZero = 0;
    unsigned char channelStayedAtZero = 0;
    unsigned char backlightStayedAtZero = 0;

    INTCONbits.GIE = 0;
    
    Example_System_Init();          // Your System Initialization Function
    //hasBeenBackToZero.byte = 0;
    
    //================================================================================================
    //  ____                  _              _           _____                _          _    ____ ___ 
    // |  _ \ ___  __ _ _   _(_)_ __ ___  __| |  _ __ __|_   _|__  _   _  ___| |__      / \  |  _ \_ _|
    // | |_) / _ \/ _` | | | | | '__/ _ \/ _` | | '_ ` _ \| |/ _ \| | | |/ __| '_ \    / _ \ | |_) | | 
    // |  _ <  __/ (_| | |_| | | | |  __/ (_| | | | | | | | | (_) | |_| | (__| | | |  / ___ \|  __/| | 
    // |_| \_\___|\__, |\__,_|_|_|  \___|\__,_| |_| |_| |_|_|\___/ \__,_|\___|_| |_| /_/   \_\_|  |___|
    //               |_|                    
    //================================================================================================                                                           
    
    mTouch_Init();                  // mTouch Initialization (Required)
      
    INTCONbits.GIE = 1;             // Initialization complete. Begin servicing interrupts.
    
    while(1)
    {    
    
        if (mTouch_isDataReady())   // Is new information ready?
        {
            mTouch_Service();       // Decode the newly captured data and transmit new data updates.

    //------------------------------------------------------------------------------------------------
    //                              REQUIRED MTOUCH API ENDS HERE
    //
    // NOTE: The below API examples show some possible ways to integrate your application with
    //       the mTouch framework. These are not required, but may be helpful.
    //------------------------------------------------------------------------------------------------
    
    
            //  ____                              ____  _        _            _    ____ ___ 
            // / ___|  ___ _ __  ___  ___  _ __  / ___|| |_ __ _| |_ ___     / \  |  _ \_ _|
            // \___ \ / _ \ '_ \/ __|/ _ \| '__| \___ \| __/ _` | __/ _ \   / _ \ | |_) | | 
            //  ___) |  __/ | | \__ \ (_) | |     ___) | || (_| | ||  __/  / ___ \|  __/| | 
            // |____/ \___|_| |_|___/\___/|_|    |____/ \__\__,_|\__\___| /_/   \_\_|  |___| 
            
            // The mTouch framework does not have an automated, board-level output system. 
            // All system responses to sensor state changes must be programmed by you, as shown below.

            
            //Check Auto/Man sensor state
            if(mTouch_GetButtonState(0) < MTOUCH_PRESSED)
            {
                autoManStateCount = 0;
                ++autoManStayedAtZero;
            }
            else if(autoManStayedAtZero > SensorStateChangeCountThreshold)
            {
                ++autoManStateCount;
            }
            
            //Check Channel sensor state
            if(mTouch_GetButtonState(1) < MTOUCH_PRESSED)
            {
                channelStateCount = 0;
                ++channelStayedAtZero;
            }
            else if(channelStayedAtZero > SensorStateChangeCountThreshold)
            {
                ++channelStateCount;
            }
            
            //Check Backlight sensor state
            if(mTouch_GetButtonState(2) < MTOUCH_PRESSED)
            {
                backlightStateCount = 0;
                ++backlightStayedAtZero;
            }
            else if(backlightStayedAtZero > SensorStateChangeCountThreshold)
            {
                ++backlightStateCount;
            }
            
            if(autoManStateCount == SensorStateChangeCountThreshold)
            {
                autoManStayedAtZero = 0;
                buttonsStateI2CBuf.buttonsStruct.AutoManPressed = 1;
            }
            
            if(channelStateCount == SensorStateChangeCountThreshold)
            {
                channelStayedAtZero = 0;
                buttonsStateI2CBuf.buttonsStruct.ChannelPressed = 1;
            }
            
            if(backlightStateCount == SensorStateChangeCountThreshold)
            {
                backlightStayedAtZero = 0;
                buttonsStateI2CBuf.buttonsStruct.BacklightPressed = 1;
            }
            
            
            //  __  __           _           _    ____ ___ 
            // |  \/  | ___   __| | ___     / \  |  _ \_ _|
            // | |\/| |/ _ \ / _` |/ _ \   / _ \ | |_) | | 
            // | |  | | (_) | (_| |  __/  / ___ \|  __/| | 
            // |_|  |_|\___/ \__,_|\___| /_/   \_\_|  |___|
            // 
            // Modes allow for different sensors to be read in different application states.
            //
            // IMPORTANT NOTE:
            //  The logic, below, assumes debouncing is set to 0. If debouncing is higher
            //  than this, the state of the sensors will be checked before their state has
            //  had an opportunity to update itself. 
            //
            // This is just one way in which the mode switching may be implemented.
            
            
            #if MTOUCH_NUM_MODES > 1
        
            mTouch_DisableScanning();   // Temporarily disable scanning while this logic completes
            
            // Are all of the current sensors initialized? If not, stay in the current mode and keep scanning until 
            // they are. mTouch_state.areInitialized is a single-bit reserved for making temporary, local checks 
            // such as this.
            mTouch_state.areInitialized = 1;
            
            if (mTouch_GetButtonState(0) == MTOUCH_INITIALIZING) {   mTouch_state.areInitialized = 0;    }
            #if (MTOUCH_NUMBER_SENSORS > 1)        
            if (mTouch_GetButtonState(1) == MTOUCH_INITIALIZING) {   mTouch_state.areInitialized = 0;    }
            #endif
            #if (MTOUCH_NUMBER_SENSORS > 2)        
            if (mTouch_GetButtonState(2) == MTOUCH_INITIALIZING) {   mTouch_state.areInitialized = 0;    }
            #endif
            
            if (mTouch_state.areInitialized && !mTouch_state.skippedDecode)
            {
                if (mTouch_modeIndex == 0)
                {
                    if (mTouch_GetButtonState(1) == MTOUCH_PRESSED)
                    {
                        mTouch_ChangeMode(1);
                    }
                }
                else if (mTouch_modeIndex == 1)
                {
                    #if defined(MCOMM_ENABLED) && defined(MCOMM_UART_HARDWARE_USED) 
                                                        // If we are outputting data using a hardware UART...
                        while(MCOMM_UART_TXIF == 0);    // Finish all communications before entering sleep
                    #endif
                    
                    //PIC_SWDTEN_ON();                  // If using a software-enabled WDT, enable it now.
                    //SLEEP();                          // Sleep, if you want to.
                    //NOP();                            // One more instruction is executed before sleeping.
                    
                    if (mTouch_GetButtonState(0) == MTOUCH_PRESSED)
                    {
                        mTouch_ChangeMode(0);
                    }
                    
                    //PIC_SWDTEN_OFF();
                }
            }
            mTouch_EnableScanning();    // Re-enable scanning
            #endif
            
        } // end - mTouch_isDataReady() check       
        
        //  _____                       ____       _            _   _                  _    ____ ___ 
        // | ____|_ __ _ __ ___  _ __  |  _ \  ___| |_ ___  ___| |_(_) ___  _ __      / \  |  _ \_ _|
        // |  _| | '__| '__/ _ \| '__| | | | |/ _ \ __/ _ \/ __| __| |/ _ \| '_ \    / _ \ | |_) | | 
        // | |___| |  | | | (_) | |    | |_| |  __/ ||  __/ (__| |_| | (_) | | | |  / ___ \|  __/| | 
        // |_____|_|  |_|  \___/|_|    |____/ \___|\__\___|\___|\__|_|\___/|_| |_| /_/   \_\_|  |___|
        
        // Checks whether any sensors are shorted to VDD or VSS
        #if defined(MTOUCH_ERROR_DETECTION_ENABLED)
        mTouch_ErrorDetect();
        if (mTouch_state.error)
        {
            // Perform action to notify the user of error.
        }
        #endif
        
    } // end - while(1) main loop
} // end - main() function


//================================================================================================
//  _____                           _        ____            _                   ___       _ _   
// | ____|_  ____ _ _ __ ___  _ __ | | ___  / ___| _   _ ___| |_ ___ _ __ ___   |_ _|_ __ (_) |_ 
// |  _| \ \/ / _` | '_ ` _ \| '_ \| |/ _ \ \___ \| | | / __| __/ _ \ '_ ` _ \   | || '_ \| | __|
// | |___ >  < (_| | | | | | | |_) | |  __/  ___) | |_| \__ \ ||  __/ | | | | |  | || | | | | |_ 
// |_____/_/\_\__,_|_| |_| |_| .__/|_|\___| |____/ \__, |___/\__\___|_| |_| |_| |___|_| |_|_|\__|
//                           |_|                   |___/                          
//================================================================================================               

void Example_System_Init() 
{
    // The mTouch framework controls these modules:
    // *  TMR0  - YOU MUST INIT THE OPTION REGISTER / TMR0 PRESCALER
    //            Do not choose 1:1 prescaling.
    //
    // *  ADC   - automatic initialization
    // *  UART  - automatic initialization (if enabled and available)
    //
    // mTouch performs better as Fosc increases.    
    
    // NOTE: Update the configuration file if Fosc is changed!
    #if defined(_PIC14E)
        #if   _XTAL_FREQ == 16000000
        OSCCON  = 0b01111000;       // 16 MHz (PLLEN config bit doesn't matter)
        #endif
    #endif
    

    TRISA = 0xFF;
    TRISC = 0xFF;
    LATA = 0;
    LATC = 0;
    
    //Capacitive sensors initialized as digital, output low.
    TRISAbits.TRISA2 = 0;
    TRISCbits.TRISC2 = 0;
    TRISCbits.TRISC3 = 0;
    
#if 0
    LATAbits.LATA2 = 0;
    LATCbits.LATC2 = 0;
    LATCbits.LATC3 = 0;
#endif
    
    initLCDPins();
    I2C_Slave_Init();
    
    // EXAMPLE TIMER INITIALIZATIONS   
    //
    // Only an 8-bit timer may be used as the mTouch framework timer. 
    // TMR1/3/5 are not currently able to be used for this purpose.
    #if defined(MTOUCH_ISR_TIMER)
    #if (MTOUCH_ISR_TIMER == 0)
    //bit7: 0 - weak pullups enabled (inverted)
    //bit6: 0 - INT on falling edge (don't care)
    //bit5: 0 - TMR0 on internal clock
    //bit4: 0 - don't care
    //bit3: 0 - prescaler used for TMR0
    //bit2-0: 0b000 - 1:2 prescaler value
        OPTION_REG  = 0b00000000;   // TMR0 Prescaler  = 1:2
    #endif
    #endif
   
    //Init structure
    buttonsStateI2CBuf.byte = 0;
}


//================================================================================================
//  _____                           _        ___ ____  ____  
// | ____|_  ____ _ _ __ ___  _ __ | | ___  |_ _/ ___||  _ \ 
// |  _| \ \/ / _` | '_ ` _ \| '_ \| |/ _ \  | |\___ \| |_) |
// | |___ >  < (_| | | | | | | |_) | |  __/  | | ___) |  _ < 
// |_____/_/\_\__,_|_| |_| |_| .__/|_|\___| |___|____/|_| \_\
//                           |_|                                                                                     
//================================================================================================

#if (MTOUCH_INTEGRATION_TYPE != MTOUCH_CONTROLS_ISR)
void interrupt ISR(void)
{
    // EXAMPLE INTERRUPT SERVICE ROUTINE
    //
    // If MTOUCH_INTEGRATION_TYPE is defined as MTOUCH_CONTROLS_ISR, the framework will implement 
    // a dedicated ISR for the mTouch scans' use. If it is not defined, the application may implement 
    // its own ISR. 
    //
    // A few rules must be followed by custom ISR functions:
    //
    // 1. If MTOUCH_INTEGRATION_TYPE is defined as MTOUCH_CALLED_FROM_MAINLOOP, you must set 
    //    mTouch_state.isrServiced each time you enter the ISR. This tells the framework that the scan 
    //    was interrupted and needs to be repeated.
    //
    // 2. If MTOUCH_INTEGRATION_TYPE is defined as MTOUCH_CALLED_FROM_ISR, the example API usage below 
    //    is required to service mTouch scanning.
    
    
    #if (MTOUCH_INTEGRATION_TYPE == MTOUCH_CALLED_FROM_ISR)
        if (mTouch_checkInterrupt())    // Checks if the TMRxIE and TMRxIF flags are both equal to 1.
        {
            mTouch_Scan();              // Required if running as ISR slave. The mTouch timer interrupt 
                                        // flag is cleared inside the mTouch_Scan() function.
        }
    #elif (MTOUCH_INTEGRATION_TYPE == MTOUCH_CALLED_FROM_MAINLOOP)
        mTouch_state.isrServiced = 1;   // Alerts the mTouch scanning routine that an interrupt may 
                                        // have disrupted a scan. This is cleared at the start of a
                                        // new scan and is checked at the end of the scan.
                                        // Bad data can affect the readings if this flag is not set.
    #endif

}
#endif

/// @endcond


