
#include "lcd_io.h"
#include <xc.h>

#define LCD_E LATAbits.LATA1
#define LCD_RS LATAbits.LATA0
#define LCD_D4 LATAbits.LATA5
#define LCD_D5 LATAbits.LATA4
#define LCD_D6 LATCbits.LATC5
#define LCD_D7 LATCbits.LATC4

void initLCDPins(void)
{
    TRISAbits.TRISA0 = 0; //LCD_RS
    TRISAbits.TRISA1 = 0; //LCD_E
    TRISAbits.TRISA4 = 0; //LCD_D7
    TRISAbits.TRISA5 = 0; //LCD_D7
    TRISCbits.TRISC4 = 0; //LCD_D7
    TRISCbits.TRISC5 = 0; //LCD_D6
    
#if 0
    LCD_RS = 0;
    LCD_E = 0;
    LCD_D4 = 0;
    LCD_D5 = 0;
    LCD_D6 = 0;
    LCD_D7 = 0;
#endif
}

void setLCDPins(LCDByteMapping* ioState)
{
    LCD_RS = ioState->_LCD_RS;
    LCD_D4 = ioState->_LCD_D4;
    LCD_D5 = ioState->_LCD_D5;
    LCD_D6 = ioState->_LCD_D6;
    LCD_D7 = ioState->_LCD_D7;
    
    LCD_E = 1;
    asm("nop");
    LCD_E = 0;
}