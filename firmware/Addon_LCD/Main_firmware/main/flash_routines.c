/*********************************************************************
* FileName:        flash_routines.c
* Dependencies:    See INCLUDES section below
* Processor:       
* Compiler:        
* Company:         Microchip Technology, Inc.
*
* Software License Agreement:
*
* The software supplied herewith by Microchip Technology Incorporated
* (the "Company") for its PICmicro� Microcontroller is intended and
* supplied to you, the Company's customer, for use solely and
* exclusively on Microchip PICmicro Microcontroller products. The
* software is owned by the Company and/or its supplier, and is
* protected under applicable copyright laws. All rights are reserved.
* Any use in violation of the foregoing restrictions may subject the
* user to criminal sanctions under applicable laws, as well as to
* civil liability for the breach of the terms and conditions of this
* license.
*
* THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
* WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
* TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
* IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
* CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
*********************************************************************
* File Description:
*
* Change History:
* Author               Cristian Toma
********************************************************************/

#include <xc.h>

unsigned char getSpecialAccessMemoryByte(unsigned char addr)
{
    volatile unsigned char ADDRL = addr;
    
#asm
    #include <xc.inc>
    ;* This code block will read 1 word of program memory at the memory address:
    ;* PROG_ADDR_LO (must be 00h-08h) data will be returned in the variables;
    ;* PROG_DATA_HI, PROG_DATA_LO
    BANKSEL PMADRL ; Select correct Bank
    MOVLW getSpecialAccessMemoryByte@ADDRL
    MOVWF PMADRL ; Store LSB of address
    CLRF PMADRH ; Clear MSB of address
    BSF CFGS ; Select Configuration Space
    BCF GIE ; Disable interrupts
    BSF RD ; Initiate read
    NOP ; Executed (See Figure 10-2)
    NOP ; Ignored (See Figure 10-2)
    BSF GIE ; Restore interrupts
#endasm
        return ( (PMDATH)<<8 | (PMDATL) ) ;
}

