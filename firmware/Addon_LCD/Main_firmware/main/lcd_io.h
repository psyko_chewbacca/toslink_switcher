/* 
 * File:   lcd_io.h
 * Author: benjaminfd
 *
 * Created on 2 mai 2016, 11:28
 */

#ifndef LCD_IO_H
#define	LCD_IO_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    typedef struct LCDByteMapping
    {
        unsigned char _LCD_E : 1;
        unsigned char _LCD_RS : 1;
        unsigned char _unused : 2;
        unsigned char _LCD_D4 : 1;
        unsigned char _LCD_D5 : 1;
        unsigned char _LCD_D6 : 1;
        unsigned char _LCD_D7 : 1;
    }LCDByteMapping;

    void initLCDPins(void);
    void setLCDPins(LCDByteMapping* ioState);


#ifdef	__cplusplus
}
#endif

#endif	/* LCD_IO_H */

