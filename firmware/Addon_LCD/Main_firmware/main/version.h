/* 
 * File:   version.h
 * Author: benjaminfd
 *
 * Created on 2 mai 2016, 15:09
 */

#ifndef VERSION_H
#define	VERSION_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#define CurrentFirmwareVersion 0x01 //Use only 8 LSBs of variable

const unsigned int FirmwareVersion@0x7FF = CurrentFirmwareVersion;    

#ifdef	__cplusplus
}
#endif

#endif	/* VERSION_H */

