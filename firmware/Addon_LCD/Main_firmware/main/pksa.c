/*********************************************************************
* FileName:        pksa.c
* Dependencies:    See INCLUDES section below
* Processor:       
* Compiler:        
* Company:         Microchip Technology, Inc.
*
* Software License Agreement:
*
* The software supplied herewith by Microchip Technology Incorporated
* (the "Company") for its PICmicro� Microcontroller is intended and
* supplied to you, the Company's customer, for use solely and
* exclusively on Microchip PICmicro Microcontroller products. The
* software is owned by the Company and/or its supplier, and is
* protected under applicable copyright laws. All rights are reserved.
* Any use in violation of the foregoing restrictions may subject the
* user to criminal sanctions under applicable laws, as well as to
* civil liability for the breach of the terms and conditions of this
* license.
*
* THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
* WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
* TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
* IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
* CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
*********************************************************************
* File Description:
*
* Change History:
* Author               Cristian Toma
********************************************************************/
#include <xc.h>
#include "pksa.h"
#include "main.h"
#include "lcd_io.h"
#include "version.h"

unsigned char pksa_wd_address;
unsigned char pksa_status;
buttonPressedLatch_t buttonsStateI2CBuf;

void I2C_Slave_Init()
{
	TRISCbits.TRISC1 = 1;			// SDA input
	TRISCbits.TRISC0 = 1;			// SCL input

	SSPBUF = 0x0;
	SSPSTAT = 0b00000000;				// 400Khz
	SSPADD = SLAVE_ADDR;	
	SSPCON = 0x36;				// Slave mode, 7bit addr	
	SSPCON3 |= 0b01100000;		// enable start and stop conditions
}

void _WriteData(unsigned char data)
{
	do
	{
		WCOL=0;
		SSPBUF = data;
	}
	while(WCOL);
	CKP = 1;
}

int do_i2c_tasks()
{
    unsigned int dat =0 ;
    unsigned char i2cDataByte = 0;	
	

    if (SSP1IF)
    {
        i2cDataByte  = SSP1STAT;	//obtain current state

        if(SSP1STATbits.S)
        {

            switch (i2cDataByte)
            {
                case MWA :								//MASTER WRITES ADDRESS STATE
                    i2cDataByte=SSPBUF;
                    pksa_status=I2C_SLAVE_ADDRESS_RECEIVED;
                break;

                case MWD : 								//MASTER WRITES DATA STATE
                    i2cDataByte=SSPBUF;
                    
                    if(	pksa_status == I2C_SLAVE_ADDRESS_RECEIVED )
                    {   // first time we get the slave address, after that set to word address
                        pksa_wd_address = i2cDataByte;
                        pksa_status = I2C_WORD_ADDRESS_RECEIVED;
                    }			
                    else if ( pksa_status == I2C_WORD_ADDRESS_RECEIVED )
                    {
                        if ( pksa_wd_address == WriteLCDIOs )
                        {
                            setLCDPins((LCDByteMapping*)&i2cDataByte);
                        }   
                    }
                        
                break;

                case MRA :								//MASTER READS ADDRESS STATE
                    if (pksa_wd_address == JumpToApplicationCode)
                    {
                        // jump to firmware code (should use MCLR...)
                        _WriteData(0x00);
                        for ( i2cDataByte =0; i2cDataByte < 255; i2cDataByte++ );
                        #asm
                            GOTO 0x00
                        #endasm
                    }	
                    if (pksa_wd_address == GetFirmwareVersionByte)
                    {
                        // get firmware version byte
                        dat = FirmwareVersion;
                        _WriteData(dat & 0xFF);
                    }	
                    if(pksa_wd_address == RequestRunStatus)
                    {
                        //Master Request run status. Bootloader returns 0, main firmware returns 1
                        _WriteData(1);
                    }
                    if (pksa_wd_address == ReadTouchSensorsValue)
                    {	
                        _WriteData(buttonsStateI2CBuf.byte);
                        //Reset once sent
                        buttonsStateI2CBuf.byte = 0;
                    }
                break;


                case MRD :		//MASTER READS DATA STATE
                    //Call when reading more than a single byte in one transaction.
                    //
#if 0
                    //Not implemented in main application code
                    if (pksa_wd_address == SetGetAddressPointer)
                    {		
                    }
                    if (pksa_wd_address == ReadFlashCommand)
                    {
                    }
#endif
                break;	
            }
        }
        else if(SSP1STATbits.P)
        {	//STOP state	
            asm("nop");
        }	


        SSP1IF = 0;
        SSPEN = 1;														
        CKP = 1;	//release clock
    }
}


