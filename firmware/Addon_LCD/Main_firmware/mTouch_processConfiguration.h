/*************************************************************************
 *  � 2012 Microchip Technology Inc.                                       
 *  
 *  Project Name:    mTouch Framework v2.3
 *  FileName:        mTouch_processConfiguration.h
 *
 *  Processor:       See documentation for supported PIC� microcontrollers 
 *  Compiler:        HI-TECH Ver. 9.81 or later
 *  IDE:             MPLAB� IDE v8.50 (or later) or MPLAB� X                        
 *  Hardware:         
 *  Company:         
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Description:     mTouch Framework Include File
 *                   - No application code should be implemented in this
 *                     or any other mTouch framework file. This will allow
 *                     for better customer support and easier upgrades to
 *                     later firmware versions. Use the main.c and user-
 *                     generated files to implement your application.
 *                   - See the documentation located in the docs/ folder
 *                     for a more information about how the framework is
 *                     implemented.
 *************************************************************************/
/**************************************************************************
 * MICROCHIP SOFTWARE NOTICE AND DISCLAIMER: You may use this software, and 
 * any derivatives created by any person or entity by or on your behalf, 
 * exclusively with Microchip's products in accordance with applicable
 * software license terms and conditions, a copy of which is provided for
 * your referencein accompanying documentation. Microchip and its licensors 
 * retain all ownership and intellectual property rights in the 
 * accompanying software and in all derivatives hereto. 
 * 
 * This software and any accompanying information is for suggestion only. 
 * It does not modify Microchip's standard warranty for its products. You 
 * agree that you are solely responsible for testing the software and 
 * determining its suitability. Microchip has no obligation to modify, 
 * test, certify, or support the software. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE, ITS INTERACTION WITH 
 * MICROCHIP'S PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY 
 * APPLICATION. 
 * 
 * IN NO EVENT, WILL MICROCHIP BE LIABLE, WHETHER IN CONTRACT, WARRANTY, 
 * TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), STRICT 
 * LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, 
 * SPECIAL, PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, 
 * FOR COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, 
 * HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY 
 * OR THE DAMAGES ARE FORESEEABLE. TO THE FULLEST EXTENT ALLOWABLE BY LAW, 
 * MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS 
 * SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID 
 * DIRECTLY TO MICROCHIP FOR THIS SOFTWARE. 
 * 
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF 
 * THESE TERMS. 
 *************************************************************************/
/** @file   mTouch_processConfiguration.h
*   @brief  Internal framework header file performing many pre-compiler 
*           checks and generating predefined values for macro use.
*
* Also, performs setup and error checking operations at compile time.
*/
#ifndef __MTOUCH_PROCESSCONFIGURATION_H
#define __MTOUCH_PROCESSCONFIGURATION_H

/// @cond

// COPIED FROM stdint.h DUE TO THESE BEING MISSING (BUT STILL SUPPORTED) FROM THE PIC18 COMPILER

#ifndef int8_t
typedef signed char int8_t;
#define int8_t int8_t
#define INT8_MIN (-128)
#define INT8_MAX (127)
#endif
#ifndef int16_t
typedef signed int int16_t;
#define int16_t int16_t
#define INT16_MIN (-32768)
#define INT16_MAX (32767)
#endif
#ifndef int24_t
typedef signed short long int int24_t;
#define int24_t int24_t
#define INT24_MIN (-8388608L)
#define INT24_MAX (8388607L)
#endif
#ifndef int32_t
typedef signed long int int32_t;
#define int32_t int32_t
#define INT32_MIN (-2147483648L)
#define INT32_MAX (2147483647L)
#endif
#ifndef uint8_t
typedef unsigned char uint8_t;
#define uint8_t uint8_t
#define UINT8_MAX (255)
#endif
#ifndef uint16_t
typedef unsigned int uint16_t;
#define uint16_t uint16_t
#define UINT16_MAX (65535U)
#endif
#ifndef uint24_t
typedef unsigned short long int uint24_t;
#define uint24_t uint24_t
#define UINT24_MAX (16777216UL)
#endif
#ifndef uint32_t
typedef unsigned long int uint32_t;
#define uint32_t uint32_t
#define UINT32_MAX (4294967295UL)
#endif


#if defined(HI_TECH_C)
#pragma warning disable 1395    // Disables the following message from appearing at compile time:
                                //
                                //  Advisory[1395] notable code sequence candidate suitable 
                                //                 for compiler validation suite detected
                                //
                                // TIP: The compiler will display this message if it encounters a 
                                //      specific code sequence that results in compiler templates 
                                //      being used in a unique way -- this is difficult to explain 
                                //      if you are not familiar with the compiler internals. It 
                                //      does not mean there is a bug in the generated code, but 
                                //      that the code sequence is less tested. If you run across
                                //      this advisory, sending in the code to the compiler team
                                //      enables them to create new test suite cases that will more 
                                //      rigorously test that particular situation.
                                //      The code that is currently generating this advisory in the
                                //      mTouch Framework has already been submitted to Hi-Tech.
#endif

//****************************************************************************************************
//  _   _      _                   __  __                          
// | | | | ___| |_ __   ___ _ __  |  \/  | __ _  ___ _ __ ___  ___ 
// | |_| |/ _ \ | '_ \ / _ \ '__| | |\/| |/ _` |/ __| '__/ _ \/ __|
// |  _  |  __/ | |_) |  __/ |    | |  | | (_| | (__| | | (_) \__ \
// |_| |_|\___|_| .__/ \___|_|    |_|  |_|\__,_|\___|_|  \___/|___/
//              |_|  
//****************************************************************************************************

#define     __paste(a,b)                    __pasteX(a,b)               // Use this macro to concatonate 2 strings in the pre-compiler
#define     __pasteX(a,b)                   a##b                        // Do not use directly. Use __paste(a,b) instead.
#define     __paste2(a,b)                   __paste2X(a,b)              
#define     __paste2X(a,b)                  a##b                        
#define     __paste3(a,b)                   __paste3X(a,b)              
#define     __paste3X(a,b)                  a##b                        
#define     __paste4(a,b)                   __paste4X(a,b)              
#define     __paste4X(a,b)                  a##b                        
#define     __paste5(a,b)                   __paste5X(a,b)              
#define     __paste5X(a,b)                  a##b                        

#define     __3paste(a,b,c)                 __3pasteX(a,b,c)            // Use this macro to concatonate 3 strings in the pre-compiler
#define     __4paste(a,b,c,d)               __4pasteX(a,b,c,d)          // Use this macro to concatonate 4 strings in the pre-compiler
#define     __5paste(a,b,c,d,e)             __5pasteX(a,b,c,d,e)        // Use this macro to concatonate 5 strings in the pre-compiler
#define     __3pasteX(a,b,c)                a##b##c                     // Do not use directly. Use __paste3(a,b,c) instead.
#define     __4pasteX(a,b,c,d)              a##b##c##d                  // Do not use directly. Use __paste4(a,b,c,d) instead.
#define     __5pasteX(a,b,c,d,e)            a##b##c##d##e               // Do not use directly. Use __paste5(a,b,c,d,e) instead.


//****************************************************************************************************
//  _   _               _                          ____             __ _ _           
// | | | | __ _ _ __ __| |_      ____ _ _ __ ___  |  _ \ _ __ ___  / _(_) | ___  ___ 
// | |_| |/ _` | '__/ _` \ \ /\ / / _` | '__/ _ \ | |_) | '__/ _ \| |_| | |/ _ \/ __|
// |  _  | (_| | | | (_| |\ V  V / (_| | | |  __/ |  __/| | | (_) |  _| | |  __/\__ \
// |_| |_|\__,_|_|  \__,_| \_/\_/ \__,_|_|  \___| |_|   |_|  \___/|_| |_|_|\___||___/ 
//
//****************************************************************************************************

// Include the hardware profile header file that corresponds to the currently selected PIC microcontroller.
#if   defined(_12F1501) || defined(_12LF1501) || defined(_16F1503) || defined(_16LF1503) || defined(_16F1507) || defined(_16LF1507) || defined(_16F1509) || defined(_16LF1509)
    #include "Alternative Configurations/mTouch_hardwareProfile_16F150x.h" 
    #define _16F150x
#else
    #error The currently chosen processor is not yet supported by the mTouch Framework.
#endif 


#if !defined(PIC_ADC_ADFM_RIGHT)
#define PIC_ADC_ADFM_RIGHT          1
#endif
#if !defined(PIC_ADC_ADFM_LEFT)
#define PIC_ADC_ADFM_LEFT           0
#endif

    #define MTOUCH_LETTER_SENSOR0           __paste5(MTOUCH_LETTER_,MTOUCH_SENSOR0)
    #define MTOUCH_LETTER_SENSOR1           __paste5(MTOUCH_LETTER_,MTOUCH_SENSOR1)
    #define MTOUCH_LETTER_SENSOR2           __paste5(MTOUCH_LETTER_,MTOUCH_SENSOR2)

    #define MTOUCH_PORT_C_SENSOR0            __paste4(R,MTOUCH_LETTER_SENSOR0)
    #define MTOUCH_PORT_C_SENSOR1            __paste4(R,MTOUCH_LETTER_SENSOR1)
    #define MTOUCH_PORT_C_SENSOR2            __paste4(R,MTOUCH_LETTER_SENSOR2)
    #define MTOUCH_PORT_C_SENSOR(index)      __paste3(MTOUCH_PORT_C_SENSOR, index)

    #define MTOUCH_PORT_ASM_SENSOR0          __paste4(R,MTOUCH_LETTER_SENSOR0)
    #define MTOUCH_PORT_ASM_SENSOR1          __paste4(R,MTOUCH_LETTER_SENSOR1)
    #define MTOUCH_PORT_ASM_SENSOR2          __paste4(R,MTOUCH_LETTER_SENSOR2)
    #define MTOUCH_PORT_ASM_SENSOR(index)    __paste3(MTOUCH_PORT_ASM_SENSOR, index)
    
        
    #define MTOUCH_LAT_C_SENSOR0            __paste4(LAT,MTOUCH_LETTER_SENSOR0)
    #define MTOUCH_LAT_C_SENSOR1            __paste4(LAT,MTOUCH_LETTER_SENSOR1)
    #define MTOUCH_LAT_C_SENSOR2            __paste4(LAT,MTOUCH_LETTER_SENSOR2)
    #define MTOUCH_LAT_C_SENSOR(index)      __paste3(MTOUCH_LAT_C_SENSOR, index)

    #define MTOUCH_LAT_ASM_SENSOR0          __paste4(LAT,MTOUCH_LETTER_SENSOR0)
    #define MTOUCH_LAT_ASM_SENSOR1          __paste4(LAT,MTOUCH_LETTER_SENSOR1)
    #define MTOUCH_LAT_ASM_SENSOR2          __paste4(LAT,MTOUCH_LETTER_SENSOR2)
    #define MTOUCH_LAT_ASM_SENSOR(index)    __paste3(MTOUCH_LAT_ASM_SENSOR, index)

    #define MTOUCH_TRIS_C_SENSOR0           __paste4(TRIS,MTOUCH_LETTER_SENSOR0)
    #define MTOUCH_TRIS_C_SENSOR1           __paste4(TRIS,MTOUCH_LETTER_SENSOR1)
    #define MTOUCH_TRIS_C_SENSOR2           __paste4(TRIS,MTOUCH_LETTER_SENSOR2)
    #define MTOUCH_TRIS_C_SENSOR(index)     __paste3(MTOUCH_TRIS_C_SENSOR, index)

    #define MTOUCH_TRIS_ASM_SENSOR0         __paste4(TRIS,MTOUCH_LETTER_SENSOR0)
    #define MTOUCH_TRIS_ASM_SENSOR1         __paste4(TRIS,MTOUCH_LETTER_SENSOR1)
    #define MTOUCH_TRIS_ASM_SENSOR2         __paste4(TRIS,MTOUCH_LETTER_SENSOR2)
    #define MTOUCH_TRIS_ASM_SENSOR(index)   __paste3(MTOUCH_TRIS_ASM_SENSOR, index)

    #define MTOUCH_PIN_SENSOR0              __paste4(MTOUCH_PIN_,MTOUCH_SENSOR0)
    #define MTOUCH_PIN_SENSOR1              __paste4(MTOUCH_PIN_,MTOUCH_SENSOR1)
    #define MTOUCH_PIN_SENSOR2              __paste4(MTOUCH_PIN_,MTOUCH_SENSOR2)
    #define MTOUCH_PIN_REFERENCE            __paste4(MTOUCH_PIN_,CVD_REFERENCE)
    #define MTOUCH_PIN_SENSOR(index)        __paste3(MTOUCH_PIN_SENSOR, index)
        
    #define MTOUCH_SELECT_SENSOR0	        __paste4(MTOUCH_AD_, MTOUCH_SENSOR0)
    #define MTOUCH_SELECT_SENSOR1	        __paste4(MTOUCH_AD_, MTOUCH_SENSOR1)
    #define MTOUCH_SELECT_SENSOR2	        __paste4(MTOUCH_AD_, MTOUCH_SENSOR2)
    #define MTOUCH_SELECT_FVR_AND_GO        __paste4(MTOUCH_AD_, FVR_AND_GO)
    #define MTOUCH_SELECT_DAC_AND_GO        __paste4(MTOUCH_AD_, DAC_AND_GO)
    #define MTOUCH_SELECT_DAC_NOGO          __paste4(MTOUCH_AD_, DAC_NOGO)
    #define MTOUCH_SELECT_ISO_AND_GO        __paste4(MTOUCH_AD_, ISO_AND_GO) 
    #define MTOUCH_SELECT_ISO_NOGO          __paste4(MTOUCH_AD_, ISO_NOGO) 
    #define MTOUCH_SELECT_REFERENCE         __paste4(MTOUCH_AD_, CVD_REFERENCE)
    #define MTOUCH_SELECT_SENSOR(index)     __paste3(MTOUCH_SELECT_SENSOR, index)
    
    
//****************************************************************************************************
// __     __    _ _     _       _          ____             __ _                       _   _             
// \ \   / /_ _| (_) __| | __ _| |_ ___   / ___|___  _ __  / _(_) __ _ _   _ _ __ __ _| |_(_) ___  _ __  
//  \ \ / / _` | | |/ _` |/ _` | __/ _ \ | |   / _ \| '_ \| |_| |/ _` | | | | '__/ _` | __| |/ _ \| '_ \ 
//   \ V / (_| | | | (_| | (_| | ||  __/ | |__| (_) | | | |  _| | (_| | |_| | | | (_| | |_| | (_) | | | |
//    \_/ \__,_|_|_|\__,_|\__,_|\__\___|  \____\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__|_|\___/|_| |_|
//                                                               |___/    
//****************************************************************************************************
#if defined(MTOUCH_BOARD_REVISION) && defined(MTOUCH_SOFTWARE_REVISION)
    #define MTOUCH_FIRMWARE_VERSION    ((MTOUCH_BOARD_REVISION << 8) | MTOUCH_SOFTWARE_REVISION)
#endif

#if defined(MTOUCH_NUMBER_SENSORS) && (MTOUCH_NUMBER_SENSORS > 0)
    #if !defined(MTOUCH_INTEGRATION_TYPE)
        #define MTOUCH_INTEGRATION_TYPE     MTOUCH_CONTROLS_ISR
        #warning MTOUCH_INTEGRATION_TYPE not defined. The framework will attempt to control 100% of the ISR, by default.
    #else
        #if (MTOUCH_INTEGRATION_TYPE != MTOUCH_CONTROLS_ISR) && (MTOUCH_INTEGRATION_TYPE != MTOUCH_CALLED_FROM_ISR) && (MTOUCH_INTEGRATION_TYPE != MTOUCH_CALLED_FROM_MAINLOOP)
        #undef MTOUCH_INTEGRATION_TYPE
        #define MTOUCH_INTEGRATION_TYPE     MTOUCH_CONTROLS_ISR
        #warning MTOUCH_INTEGRATION_TYPE defined to an invalid value. Defaulting to MTOUCH_CONTROLS_ISR. The framework will attempt to control 100% of the ISR.
        #endif
    #endif
#else
    #error You must have at least one mTouch sensor enabled. Please use mTouch_config.h to reconfigure the mTouch framework's sensors.
#endif


#if !defined(MTOUCH_RELEASE_THRESH_FACTOR)
    #warning MTOUCH_RELEASE_THRESH_FACTOR must be defined in the configuration file. Setting to '0.5'.
    #define MTOUCH_RELEASE_THRESH_FACTOR 0.5
#endif
#if defined(MTOUCH_BASELINE_WEIGHT)
    #if ((MTOUCH_BASELINE_WEIGHT != 1) && (MTOUCH_BASELINE_WEIGHT != 2) && (MTOUCH_BASELINE_WEIGHT != 3) && (MTOUCH_BASELINE_WEIGHT != 4))
        #warning Invalid value for MTOUCH_BASELINE_WEIGHT. Setting to default value of 3.
        #undef  MTOUCH_BASELINE_WEIGHT
        #define MTOUCH_BASELINE_WEIGHT 3
    #endif
#endif

#if !defined(MTOUCH_POWER_UP_SAMPLES)
    #warning MTOUCH_POWER_UP_SAMPLES must be defined in the configuration file. Setting to '50'.
    #define MTOUCH_POWER_UP_SAMPLES 50
#else
    #if MTOUCH_POWER_UP_SAMPLES > 65535
        #warning MTOUCH_POWER_UP_SAMPLES exceeds the maximum limit of 65535. Setting to '65535'.
        #undef MTOUCH_POWER_UP_SAMPLES
        #define MTOUCH_POWER_UP_SAMPLES 65535
    #endif
    #if MTOUCH_POWER_UP_SAMPLES < 1
        #warning MTOUCH_POWER_UP_SAMPLES must be at least '1'. Setting to '1'.
        #undef MTOUCH_POWER_UP_SAMPLES
        #define MTOUCH_POWER_UP_SAMPLES 1
    #endif
#endif
#if defined (MTOUCH_BUTTON_TIMEOUT)
    #if MTOUCH_BUTTON_TIMEOUT > 65535
        #warning MTOUCH_BUTTON_TIMEOUT exceeds the maximum limit of 65535. Setting to '65535'.
        #undef MTOUCH_BUTTON_TIMEOUT
        #define MTOUCH_BUTTON_TIMEOUT 65535
    #endif
#endif
#if defined(MTOUCH_BUTTON_TIMEOUT)
    #if MTOUCH_BUTTON_TIMEOUT > MTOUCH_POWER_UP_SAMPLES
    #define CVD_TIMER_MAX  MTOUCH_BUTTON_TIMEOUT
    #else
    #define CVD_TIMER_MAX  MTOUCH_POWER_UP_SAMPLES
    #endif
#else
#define CVD_TIMER_MAX  MTOUCH_POWER_UP_SAMPLES
#endif
#if CVD_TIMER_MAX > 0
    #if CVD_TIMER_MAX > 1
        #if CVD_TIMER_MAX > 3
            #if CVD_TIMER_MAX > 7
                #if CVD_TIMER_MAX > 15
                    #if CVD_TIMER_MAX > 31
                        #if CVD_TIMER_MAX > 63
                            #if CVD_TIMER_MAX > 127
                                #if CVD_TIMER_MAX > 255
                                    #if CVD_TIMER_MAX > 511
                                        #if CVD_TIMER_MAX > 1023
                                            #if CVD_TIMER_MAX > 2047
                                                #if CVD_TIMER_MAX > 4096
                                                    #if CVD_TIMER_MAX > 8192
                                                        #if CVD_TIMER_MAX > 16384
                                                            #if CVD_TIMER_MAX > 32768
                                                                #define CVD_STATE_TIMER_BITS 16
                                                            #else
                                                            #define CVD_STATE_TIMER_BITS 15
                                                            #endif
                                                        #else
                                                        #define CVD_STATE_TIMER_BITS 14
                                                        #endif
                                                    #else
                                                    #define CVD_STATE_TIMER_BITS 13
                                                    #endif
                                                #else
                                                #define CVD_STATE_TIMER_BITS 12
                                                #endif
                                            #else
                                            #define CVD_STATE_TIMER_BITS 11
                                            #endif
                                        #else
                                        #define CVD_STATE_TIMER_BITS 10
                                        #endif
                                    #else
                                    #define CVD_STATE_TIMER_BITS 9
                                    #endif
                                #else
                                #define CVD_STATE_TIMER_BITS 8
                                #endif
                            #else
                            #define CVD_STATE_TIMER_BITS 7
                            #endif
                        #else
                        #define CVD_STATE_TIMER_BITS 6
                        #endif
                    #else
                    #define CVD_STATE_TIMER_BITS 5
                    #endif
                #else
                #define CVD_STATE_TIMER_BITS 4
                #endif
            #else
            #define CVD_STATE_TIMER_BITS 3
            #endif
        #else
        #define CVD_STATE_TIMER_BITS 2
        #endif
    #else
    #define CVD_STATE_TIMER_BITS 1
    #endif
#else
#define CVD_STATE_TIMER_BITS 0
#endif
#if ( (CVD_STATE_TIMER_BITS > 8) || defined(MTOUCH_EEPROM_ENABLED) )
    #define MTOUCH_STATE_TIMER()               uint16_t timer
#else
    #define MTOUCH_STATE_TIMER()               __paste(unsigned timer  :,CVD_STATE_TIMER_BITS)
#endif

#if !defined(MTOUCH_DEBOUNCE_RELEASE)
    #define MTOUCH_DEBOUNCE_RELEASE 0
#else
    #if MTOUCH_DEBOUNCE_RELEASE > 65535
        #warning MTOUCH_DEBOUNCE_RELEASE exceeds the maximum limit of 65535. Setting to '65535'.
        #undef MTOUCH_DEBOUNCE_RELEASE
        #define MTOUCH_DEBOUNCE_RELEASE 65535
    #endif
    #if MTOUCH_DEBOUNCE_RELEASE < 0
        #warning MTOUCH_DEBOUNCE_RELEASE must be at least '0'. Setting to '0'.
        #undef MTOUCH_DEBOUNCE_RELEASE
        #define MTOUCH_DEBOUNCE_RELEASE 0
    #endif
#endif
#if !defined(MTOUCH_DEBOUNCE_PRESS)
    #define MTOUCH_DEBOUNCE_PRESS 0
#else
    #if MTOUCH_DEBOUNCE_PRESS > 65535
        #warning MTOUCH_DEBOUNCE_PRESS exceeds the maximum limit of 65535. Setting to '65535'.
        #undef MTOUCH_DEBOUNCE_PRESS
        #define MTOUCH_DEBOUNCE_PRESS 65535
    #endif
    #if MTOUCH_DEBOUNCE_PRESS < 0
        #warning MTOUCH_DEBOUNCE_PRESS must be at least '0'. Setting to '0'.
        #undef MTOUCH_DEBOUNCE_PRESS
        #define MTOUCH_DEBOUNCE_PRESS 0
    #endif
#endif
#if MTOUCH_DEBOUNCE_PRESS > MTOUCH_DEBOUNCE_RELEASE
    #define CVD_DEBOUNCE_MAX    MTOUCH_DEBOUNCE_PRESS
#else
    #define CVD_DEBOUNCE_MAX    MTOUCH_DEBOUNCE_RELEASE
#endif
#if CVD_DEBOUNCE_MAX > 0
    #if CVD_DEBOUNCE_MAX > 1
        #if CVD_DEBOUNCE_MAX > 3
            #if CVD_DEBOUNCE_MAX > 7
                #if CVD_DEBOUNCE_MAX > 15
                    #if CVD_DEBOUNCE_MAX > 31
                        #if CVD_DEBOUNCE_MAX > 63
                            #if CVD_DEBOUNCE_MAX > 127
                                #if CVD_DEBOUNCE_MAX > 255
                                    #if CVD_DEBOUNCE_MAX > 511
                                        #if CVD_DEBOUNCE_MAX > 1023
                                            #if CVD_DEBOUNCE_MAX > 2047
                                                #if CVD_DEBOUNCE_MAX > 4096
                                                    #if CVD_DEBOUNCE_MAX > 8192
                                                        #if CVD_DEBOUNCE_MAX > 16384
                                                            #if CVD_DEBOUNCE_MAX > 32768
                                                                #define CVD_STATE_DEBOUNCE_BITS 16
                                                            #else
                                                            #define CVD_STATE_DEBOUNCE_BITS 15
                                                            #endif
                                                        #else
                                                        #define CVD_STATE_DEBOUNCE_BITS 14
                                                        #endif
                                                    #else
                                                    #define CVD_STATE_DEBOUNCE_BITS 13
                                                    #endif
                                                #else
                                                #define CVD_STATE_DEBOUNCE_BITS 12
                                                #endif
                                            #else
                                            #define CVD_STATE_DEBOUNCE_BITS 11
                                            #endif
                                        #else
                                        #define CVD_STATE_DEBOUNCE_BITS 10
                                        #endif
                                    #else
                                    #define CVD_STATE_DEBOUNCE_BITS 9
                                    #endif
                                #else
                                #define CVD_STATE_DEBOUNCE_BITS 8
                                #endif
                            #else
                            #define CVD_STATE_DEBOUNCE_BITS 7
                            #endif
                        #else
                        #define CVD_STATE_DEBOUNCE_BITS 6
                        #endif
                    #else
                    #define CVD_STATE_DEBOUNCE_BITS 5
                    #endif
                #else
                #define CVD_STATE_DEBOUNCE_BITS 4
                #endif
            #else
            #define CVD_STATE_DEBOUNCE_BITS 3
            #endif
        #else
        #define CVD_STATE_DEBOUNCE_BITS 2
        #endif
    #else
    #define CVD_STATE_DEBOUNCE_BITS 1
    #endif
#else
#define CVD_STATE_DEBOUNCE_BITS 0
#endif
#if (CVD_STATE_DEBOUNCE_BITS > 8) || defined(MTOUCH_EEPROM_ENABLED)
    #define MTOUCH_STATE_DEBOUNCE()               uint16_t debounce
#elif CVD_STATE_DEBOUNCE_BITS != 0
    #define MTOUCH_STATE_DEBOUNCE()               __paste(unsigned debounce  :,CVD_STATE_DEBOUNCE_BITS)
#else
    #define MTOUCH_DEBOUNCE_IS_ZERO
#endif

#if !defined(MTOUCH_SAMPLES_PER_SCAN)
    #warning MTOUCH_SAMPLES_PER_SCAN must be defined in the configuration file. Defaulting to 20ms timing.
    #define MTOUCH_SAMPLES_PER_SCAN CVD_20ms_Timing
#endif
#if !defined(MTOUCH_SAMPLES_PER_SCAN)
    #warning MTOUCH_SAMPLES_PER_SCAN must be defined in the configuration file. Defaulting to 30 samples.
    #define MTOUCH_SAMPLES_PER_SCAN 30
#endif
#if MTOUCH_SAMPLES_PER_SCAN > 0
    #if MTOUCH_SAMPLES_PER_SCAN > 1
        #if MTOUCH_SAMPLES_PER_SCAN > 3
            #if MTOUCH_SAMPLES_PER_SCAN > 7
                #if MTOUCH_SAMPLES_PER_SCAN > 15
                    #if MTOUCH_SAMPLES_PER_SCAN > 31
                        #if MTOUCH_SAMPLES_PER_SCAN > 63
                            #if MTOUCH_SAMPLES_PER_SCAN > 127
                                #if MTOUCH_SAMPLES_PER_SCAN > 255
                                    #if MTOUCH_SAMPLES_PER_SCAN > 511
                                        #if MTOUCH_SAMPLES_PER_SCAN > 1023
                                            #if MTOUCH_SAMPLES_PER_SCAN > 2047
                                                #if MTOUCH_SAMPLES_PER_SCAN > 4096
                                                    #if MTOUCH_SAMPLES_PER_SCAN > 8192
                                                        #if MTOUCH_SAMPLES_PER_SCAN > 16384
                                                            #if MTOUCH_SAMPLES_PER_SCAN > 32768
                                                                #define CVD_STATE_SPS_BITS 16
                                                            #else
                                                            #define CVD_STATE_SPS_BITS 15
                                                            #endif
                                                        #else
                                                        #define CVD_STATE_SPS_BITS 14
                                                        #endif
                                                    #else
                                                    #define CVD_STATE_SPS_BITS 13
                                                    #endif
                                                #else
                                                #define CVD_STATE_SPS_BITS 12
                                                #endif
                                            #else
                                            #define CVD_STATE_SPS_BITS 11
                                            #endif
                                        #else
                                        #define CVD_STATE_SPS_BITS 10
                                        #endif
                                    #else
                                    #define CVD_STATE_SPS_BITS 9
                                    #endif
                                #else
                                #define CVD_STATE_SPS_BITS 8
                                #endif
                            #else
                            #define CVD_STATE_SPS_BITS 7
                            #endif
                        #else
                        #define CVD_STATE_SPS_BITS 6
                        #endif
                    #else
                    #define CVD_STATE_SPS_BITS 5
                    #endif
                #else
                #define CVD_STATE_SPS_BITS 4
                #endif
            #else
            #define CVD_STATE_SPS_BITS 3
            #endif
        #else
        #define CVD_STATE_SPS_BITS 2
        #endif
    #else
    #define CVD_STATE_SPS_BITS 1
    #endif
#else
#define CVD_STATE_SPS_BITS 0
#endif
#if (CVD_STATE_SPS_BITS > 8) || defined(MTOUCH_EEPROM_ENABLED)
    #define MTOUCH_STATE_SAMPLE_COUNTER()               uint16_t sampleCounter
#else
    #define MTOUCH_STATE_SAMPLE_COUNTER()               __paste(unsigned sampleCounter  :,CVD_STATE_SPS_BITS)
#endif

#if (defined(MTOUCH_JITTER_BITS) && (MTOUCH_JITTER_BITS != 0)) || defined(MTOUCH_EEPROM_ENABLED)
    #define MTOUCH_JITTER_ENABLE
#else
    #if defined(MTOUCH_JITTER_ENABLE)
    #undef MTOUCH_JITTER_ENABLE
    #endif
#endif

#if defined(MTOUCH_JITTER_ENABLE)
    #define MTOUCH_JITTER_MASK0         0x00
    #define MTOUCH_JITTER_MASK1         0x01
    #define MTOUCH_JITTER_MASK2         0x03
    #define MTOUCH_JITTER_MASK3         0x07
    #define MTOUCH_JITTER_MASK4         0x0F
    #define MTOUCH_JITTER_MASK5         0x1F
    #define MTOUCH_JITTER_MASK6         0x3F
    #define MTOUCH_JITTER_MASK7         0x7F
    #define MTOUCH_JITTER_MASK8         0xFF
    
    #define MTOUCH_JITTER_MASK          __paste4(MTOUCH_JITTER_MASK,MTOUCH_JITTER_BITS)
    #define MTOUCH_JITTER_MASK_ARRAY    { MTOUCH_JITTER_MASK0, MTOUCH_JITTER_MASK1, MTOUCH_JITTER_MASK2, MTOUCH_JITTER_MASK3, MTOUCH_JITTER_MASK4, MTOUCH_JITTER_MASK5, MTOUCH_JITTER_MASK6, MTOUCH_JITTER_MASK7, MTOUCH_JITTER_MASK8 } 
#endif

#if !defined(_XTAL_FREQ)
    #error _XTAL_FREQ is not defined in the configuration file. Framework must be told how fast Fosc is running.
#endif
#if !defined(MTOUCH_SENSOR0)
    #error There is no MTOUCH_SENSOR0 definition in the configuration file. Framework unable to associate the sensor with an analog pin.
#endif
#if !defined(MTOUCH_NEGATIVE_CAPACITANCE)
    #define MTOUCH_NEGATIVE_CAPACITANCE 0
#endif
#if (MTOUCH_NEGATIVE_CAPACITANCE != 0) && (MTOUCH_NEGATIVE_CAPACITANCE != 1) && (MTOUCH_NEGATIVE_CAPACITANCE != 2)
    #warning MTOUCH_NEGATIVE_CAPACITANCE is set to an invalid value. Defaulting to 0.
    #undef MTOUCH_NEGATIVE_CAPACITANCE
    #define MTOUCH_NEGATIVE_CAPACITANCE 0
#endif
#if MTOUCH_BASELINE_RATE > 0
    #if MTOUCH_BASELINE_RATE > 1
        #if MTOUCH_BASELINE_RATE > 3
            #if MTOUCH_BASELINE_RATE > 7
                #if MTOUCH_BASELINE_RATE > 15
                    #if MTOUCH_BASELINE_RATE > 31
                        #if MTOUCH_BASELINE_RATE > 63
                            #if MTOUCH_BASELINE_RATE > 127
                                #if MTOUCH_BASELINE_RATE > 255
                                    #if MTOUCH_BASELINE_RATE > 511
                                        #if MTOUCH_BASELINE_RATE > 1023
                                            #if MTOUCH_BASELINE_RATE > 2047
                                                #if MTOUCH_BASELINE_RATE > 4096
                                                    #if MTOUCH_BASELINE_RATE > 8192
                                                        #if MTOUCH_BASELINE_RATE > 16384
                                                            #if MTOUCH_BASELINE_RATE > 32768
                                                                #define CVD_STATE_BLCOUNT_BITS 16
                                                            #else
                                                            #define CVD_STATE_BLCOUNT_BITS 15
                                                            #endif
                                                        #else
                                                        #define CVD_STATE_BLCOUNT_BITS 14
                                                        #endif
                                                    #else
                                                    #define CVD_STATE_BLCOUNT_BITS 13
                                                    #endif
                                                #else
                                                #define CVD_STATE_BLCOUNT_BITS 12
                                                #endif
                                            #else
                                            #define CVD_STATE_BLCOUNT_BITS 11
                                            #endif
                                        #else
                                        #define CVD_STATE_BLCOUNT_BITS 10
                                        #endif
                                    #else
                                    #define CVD_STATE_BLCOUNT_BITS 9
                                    #endif
                                #else
                                #define CVD_STATE_BLCOUNT_BITS 8
                                #endif
                            #else
                            #define CVD_STATE_BLCOUNT_BITS 7
                            #endif
                        #else
                        #define CVD_STATE_BLCOUNT_BITS 6
                        #endif
                    #else
                    #define CVD_STATE_BLCOUNT_BITS 5
                    #endif
                #else
                #define CVD_STATE_BLCOUNT_BITS 4
                #endif
            #else
            #define CVD_STATE_BLCOUNT_BITS 3
            #endif
        #else
        #define CVD_STATE_BLCOUNT_BITS 2
        #endif
    #else
    #define CVD_STATE_BLCOUNT_BITS 1
    #endif
#else
#define CVD_STATE_BLCOUNT_BITS 0
#endif
#if (CVD_STATE_BLCOUNT_BITS > 8) || defined(MTOUCH_EEPROM_ENABLED)
    #define MTOUCH_STATE_BASELINE_COUNT()         uint16_t baselineCount
    #define MTOUCH_BLCOUNT_16BITS_REQUIRED
#else
    #define MTOUCH_STATE_BASELINE_COUNT()         __paste(unsigned baselineCount  :,CVD_STATE_BLCOUNT_BITS)
#endif


#define MTOUCH_EXIT_SENSOR0()   EXIT_LAST_SENSOR(0)
#if MTOUCH_NUMBER_SENSORS > 1
    #if !defined(MTOUCH_SENSOR1)
        #error There is no MTOUCH_SENSOR1 definition in the configuration file. Framework unable to associate the sensor with an analog pin.
    #endif
    #undef  MTOUCH_EXIT_SENSOR0()
    #define MTOUCH_EXIT_SENSOR0()   EXIT_SENSOR(mTouch_currentSensor)
    #define MTOUCH_EXIT_SENSOR1()   EXIT_LAST_SENSOR(mTouch_currentSensor)
#endif
#if MTOUCH_NUMBER_SENSORS > 2
    #if !defined(MTOUCH_SENSOR2)
        #error There is no MTOUCH_SENSOR2 definition in the configuration file. Framework unable to associate the sensor with an analog pin.
    #endif
    #undef  MTOUCH_EXIT_SENSOR1()
    #define MTOUCH_EXIT_SENSOR1()   EXIT_SENSOR(mTouch_currentSensor)
    #define MTOUCH_EXIT_SENSOR2()   EXIT_LAST_SENSOR(mTouch_currentSensor)
#endif


#if !defined(THRESHOLD_PRESS_SENSOR0)
    #if MTOUCH_NUMBER_SENSORS > 0
        #error Required definition, THRESHOLD_PRESS_SENSOR0, is not defined in the configuration file.
    #else
        #define THRESHOLD_PRESS_SENSOR0 0
    #endif
#endif
#if !defined(THRESHOLD_PRESS_SENSOR1)
    #if MTOUCH_NUMBER_SENSORS > 1
        #error Required definition, THRESHOLD_PRESS_SENSOR1, is not defined in the configuration file.
    #else
        #define THRESHOLD_PRESS_SENSOR1 0
    #endif
#endif
#if !defined(THRESHOLD_PRESS_SENSOR2)
    #if MTOUCH_NUMBER_SENSORS > 2
        #error Required definition, THRESHOLD_PRESS_SENSOR2, is not defined in the configuration file.
    #else
        #define THRESHOLD_PRESS_SENSOR2 0
    #endif
#endif

#define THRESHOLD_RELEASE_SENSOR0       (uint16_t)((float) THRESHOLD_PRESS_SENSOR0  * MTOUCH_RELEASE_THRESH_FACTOR)                                             
#define THRESHOLD_RELEASE_SENSOR1       (uint16_t)((float) THRESHOLD_PRESS_SENSOR1  * MTOUCH_RELEASE_THRESH_FACTOR) 
#define THRESHOLD_RELEASE_SENSOR2       (uint16_t)((float) THRESHOLD_PRESS_SENSOR2  * MTOUCH_RELEASE_THRESH_FACTOR) 

#if MTOUCH_NUMBER_SENSORS == 1
    #define MTOUCH_INDEXMINUSONE_0 0
    #define MTOUCH_ASM_MACRO_GLOBAL_SCAN_LABELS() asm("global sensor_0")    
    #define PRESS_THRESHOLD_INIT {THRESHOLD_PRESS_SENSOR0}
    #define RELEASE_THRESHOLD_INIT {THRESHOLD_RELEASE_SENSOR0}
    #define MTOUCH_SENSOR_CHANNEL_INIT  {MTOUCH_SELECT_SENSOR0}
    #if defined(MTOUCH_UNIQUE_OVERSAMPLE_ENABLE)
        #undef MTOUCH_UNIQUE_OVERSAMPLE_ENABLE
        #warning Disabling MTOUCH_UNIQUE_OVERSAMPLE_ENABLE because only one sensor is enabled.
    #endif    
#endif
#if MTOUCH_NUMBER_SENSORS == 2
    #define MTOUCH_INDEXMINUSONE_0 1
    #define MTOUCH_ASM_MACRO_GLOBAL_SCAN_LABELS() asm("global sensor_0, sensor_1")
    #define PRESS_THRESHOLD_INIT {THRESHOLD_PRESS_SENSOR0,THRESHOLD_PRESS_SENSOR1}
    #define RELEASE_THRESHOLD_INIT {THRESHOLD_RELEASE_SENSOR0,THRESHOLD_RELEASE_SENSOR1}
    #if defined(MTOUCH_UNIQUE_OVERSAMPLE_ENABLE)
    #define MTOUCH_UNIQUE_OVERSAMPLE_INIT   { MTOUCH_OVERSAMPLE0, MTOUCH_OVERSAMPLE1 }
    #endif
    #define MTOUCH_SENSOR_CHANNEL_INIT  {MTOUCH_SELECT_SENSOR0, MTOUCH_SELECT_SENSOR0}
#endif
#if MTOUCH_NUMBER_SENSORS == 3
    #define MTOUCH_INDEXMINUSONE_0 2
    #define MTOUCH_ASM_MACRO_GLOBAL_SCAN_LABELS() asm("global sensor_0, sensor_1, sensor_2")
    #define PRESS_THRESHOLD_INIT {THRESHOLD_PRESS_SENSOR0,THRESHOLD_PRESS_SENSOR1,THRESHOLD_PRESS_SENSOR2}
    #define RELEASE_THRESHOLD_INIT {THRESHOLD_RELEASE_SENSOR0,THRESHOLD_RELEASE_SENSOR1,THRESHOLD_RELEASE_SENSOR2}
    #if defined(MTOUCH_UNIQUE_OVERSAMPLE_ENABLE)
    #define MTOUCH_UNIQUE_OVERSAMPLE_INIT   { MTOUCH_OVERSAMPLE0, MTOUCH_OVERSAMPLE1, MTOUCH_OVERSAMPLE2 }
    #endif
    #define MTOUCH_SENSOR_CHANNEL_INIT  {MTOUCH_SELECT_SENSOR0, MTOUCH_SELECT_SENSOR1, MTOUCH_SELECT_SENSOR2}
#endif

#define NOP0    
#define NOP1     asm("nop");
#define NOP2     NOP1;  NOP1;
#define NOP3     NOP2;  NOP1;
#define NOP4     NOP3;  NOP1;
#define NOP5     NOP4;  NOP1;
#define NOP6     NOP5;  NOP1;
#define NOP7     NOP6;  NOP1;
#define NOP8     NOP7;  NOP1;
#define NOP9     NOP8;  NOP1;
#define NOP10    NOP9;  NOP1;
#define NOP11   NOP10;  NOP1;
#define NOP12   NOP11;  NOP1;
#define NOP13   NOP12;  NOP1;
#define NOP14   NOP13;  NOP1;
#define NOP15   NOP14;  NOP1;
#define NOP16   NOP15;  NOP1;
#define NOP17   NOP16;  NOP1;
#define NOP18   NOP17;  NOP1;
#define NOP19   NOP18;  NOP1;
#define NOP20   NOP19;  NOP1;
#define NOP21   NOP20;  NOP1;
#define NOP22   NOP21;  NOP1;
#define NOP23   NOP22;  NOP1;
#define NOP24   NOP23;  NOP1;
#define NOP25   NOP24;  NOP1;
#define NOP26   NOP25;  NOP1;
#define NOP27   NOP26;  NOP1;
#define NOP28   NOP27;  NOP1;
#define NOP29   NOP28;  NOP1;
#define NOP30   NOP29;  NOP1;
#define NOP31   NOP30;  NOP1;
#define NOP32   NOP31;  NOP1;
#define NOP33   NOP32;  NOP1;
#define NOP34   NOP33;  NOP1;
#define NOP35   NOP34;  NOP1;
#define NOP36   NOP35;  NOP1;
#define NOP37   NOP36;  NOP1;
#define NOP38   NOP37;  NOP1;
#define NOP39   NOP38;  NOP1;
#define NOP40   NOP39;  NOP1;
#define NOP41   NOP40;  NOP1;
#define NOP42   NOP41;  NOP1;
#define NOP43   NOP42;  NOP1;
#define NOP44   NOP43;  NOP1;
#define NOP45   NOP44;  NOP1;
#define NOP46   NOP45;  NOP1;
#define NOP47   NOP46;  NOP1;
#define NOP48   NOP47;  NOP1;
#define NOP49   NOP48;  NOP1;
#define NOP50   NOP49;  NOP1;
#define NOP51   NOP50;  NOP1;
#define NOP52   NOP50;  NOP2;
#define NOP53   NOP50;  NOP3;
#define NOP54   NOP50;  NOP4;
#define NOP55   NOP50;  NOP5;
#define NOP56   NOP50;  NOP6;
#define NOP57   NOP50;  NOP7;
#define NOP58   NOP50;  NOP8;
#define NOP59   NOP50;  NOP9;
#define NOP60   NOP50; NOP10;
#define NOP61   NOP50; NOP10; NOP1;
#define NOP62   NOP50; NOP11; NOP1;
#define NOP63   NOP50; NOP12; NOP1;
#define NOP64   NOP50; NOP13; NOP1;
#define NOP65   NOP50; NOP14; NOP1;
#define NOP66   NOP50; NOP15; NOP1;
#define NOP67   NOP50; NOP16; NOP1;
#define NOP68   NOP50; NOP17; NOP1;
#define NOP69   NOP50; NOP18; NOP1;
#define NOP70   NOP50; NOP19; NOP1;
#define NOP71   NOP50; NOP20; NOP1;
#define NOP72   NOP50; NOP21; NOP1;
#define NOP73   NOP50; NOP22; NOP1;
#define NOP74   NOP50; NOP23; NOP1;
#define NOP75   NOP50; NOP24; NOP1;
#define NOP76   NOP50; NOP25; NOP1;
#define NOP77   NOP50; NOP26; NOP1;
#define NOP78   NOP50; NOP27; NOP1;
#define NOP79   NOP50; NOP28; NOP1;
#define NOP80   NOP50; NOP29; NOP1;
#define NOP81   NOP50; NOP30; NOP1;
#define NOP82   NOP50; NOP31; NOP1;
#define NOP83   NOP50; NOP32; NOP1;
#define NOP84   NOP50; NOP33; NOP1;
#define NOP85   NOP50; NOP34; NOP1;
#define NOP86   NOP50; NOP35; NOP1;
#define NOP87   NOP50; NOP36; NOP1;
#define NOP88   NOP50; NOP37; NOP1;
#define NOP89   NOP50; NOP38; NOP1;
#define NOP90   NOP50; NOP39; NOP1;
#define NOP91   NOP50; NOP40; NOP1;
#define NOP92   NOP50; NOP41; NOP1;
#define NOP93   NOP50; NOP42; NOP1;
#define NOP94   NOP50; NOP43; NOP1;
#define NOP95   NOP50; NOP44; NOP1;
#define NOP96   NOP50; NOP45; NOP1;
#define NOP97   NOP50; NOP46; NOP1;
#define NOP98   NOP50; NOP47; NOP1;
#define NOP99   NOP50; NOP48; NOP1;
#define NOP100  NOP50; NOP50;

#define _NOP_DELAY(value)       __paste(NOP,value);

#if !defined(CVD_CHOLD_CHARGE_DELAY)
    #define CVD_CHOLD_CHARGE_DELAY 0
#endif
#if !defined(CVD_SETTLING_DELAY)
    #define CVD_SETTLING_DELAY 0
#endif
#if !defined(CVD_PRE_GODONE_DELAY)
    #define CVD_PRE_GODONE_DELAY 0
#endif

#if (CVD_CHOLD_CHARGE_DELAY - 2 - 3 * ((CVD_CHOLD_CHARGE_DELAY-2)/3)) == 1 
    #define CVD_DELAY_CHOLD_REMAINDER()   _NOP_DELAY(1);
#elif (CVD_CHOLD_CHARGE_DELAY - 2 - 3 * ((CVD_CHOLD_CHARGE_DELAY-2)/3)) == 2
    #define CVD_DELAY_CHOLD_REMAINDER()   _NOP_DELAY(2);
#else
    #define CVD_DELAY_CHOLD_REMAINDER()
#endif

#if defined(_12F615) || defined(_12HV615) || defined(_PIC18)
    #define CVD_DELAY_CHOLD()  _NOP_DELAY(CVD_CHOLD_CHARGE_DELAY);
#else
    #if CVD_CHOLD_CHARGE_DELAY > 4
        #define CVD_DELAY_VARIABLE_REQUIRED
        #define CVD_DELAY_CHOLD()                                               \
            do {                                                                \
                asm("BANKSEL    _mTouch_delayCount"                         );  \
                asm("movlw  "   ___mkstr(CVD_CHOLD_CHARGE_DELAY-2)      "/3");  \
                asm("movwf  "   ___mkstr(NOBANK(_mTouch_delayCount))        );  \
                asm("decfsz "   ___mkstr(NOBANK(_mTouch_delayCount))   ", F");  \
                asm("goto $-1");                                                \
                CVD_DELAY_CHOLD_REMAINDER();                                    \
            } while(0)
    #else
        #define CVD_DELAY_CHOLD()   _NOP_DELAY(CVD_CHOLD_CHARGE_DELAY);
    #endif
#endif 

#if (CVD_SETTLING_DELAY - 2 - 3 * ((CVD_SETTLING_DELAY-2)/3)) == 1
    #define CVD_DELAY_SETTLE_REMAINDER()   _NOP_DELAY(1);
#elif (CVD_SETTLING_DELAY - 2 - 3 * ((CVD_SETTLING_DELAY-2)/3)) == 2
    #define CVD_DELAY_SETTLE_REMAINDER()   _NOP_DELAY(2);
#else
    #define CVD_DELAY_SETTLE_REMAINDER()
#endif

#if defined(_12F615) || defined(_12HV615) || defined(_PIC18)
    #define CVD_DELAY_SETTLE()  _NOP_DELAY(CVD_SETTLING_DELAY);
#else
    #if CVD_SETTLING_DELAY > 4
        #if !defined(CVD_DELAY_VARIABLE_REQUIRED)
            #define CVD_DELAY_VARIABLE_REQUIRED
        #endif
        
        #define CVD_DELAY_SETTLE()                                              \
            do {                                                                \
                asm("BANKSEL    _mTouch_delayCount"                         );  \
                asm("movlw  "   ___mkstr(CVD_SETTLING_DELAY-2)         "/3" );  \
                asm("movwf  "   ___mkstr(NOBANK(_mTouch_delayCount))        );  \
                asm("decfsz "   ___mkstr(NOBANK(_mTouch_delayCount))   ", F");  \
                asm("goto       $-1"                                        );  \
                CVD_DELAY_SETTLE_REMAINDER();                                   \
            } while(0)
    #else
        #define CVD_DELAY_SETTLE()  _NOP_DELAY(CVD_SETTLING_DELAY);
    #endif       
#endif

#if (CVD_UNIMPLEMENTED_AVAILABLE == 1)
    #define CVD_GO_DONE_DELAY()     asm("BANKSEL "  ___mkstr(_ADCON0));                         \
                                    asm("movlw   "  ___mkstr(MTOUCH_SELECT_ISO_NOGO | 0x02));   \
                                    asm("movwf   "  ___mkstr(NOBANK(_ADCON0)));  
#else
    #if !defined(PIC_ADC_TAD)
        #warning The PIC_ADC_TAD value in your devices hardware profile has not been defined. Defaulting to '1' us.
        #define PIC_ADC_TAD 1
    #endif
    #if PIC_ADC_TAD == 1
        #if _XTAL_FREQ == 32000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(3);
        #elif _XTAL_FREQ == 16000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(1);
        #elif _XTAL_FREQ == 8000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(0);
        #elif _XTAL_FREQ == 4000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(0);
        #elif _XTAL_FREQ == 2000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(0);
        #elif _XTAL_FREQ == 1000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(0);
        #endif
    #elif PIC_ADC_TAD == 2
        #if _XTAL_FREQ == 32000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(7);
        #elif _XTAL_FREQ == 16000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(3);
        #elif _XTAL_FREQ == 8000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(1);
        #elif _XTAL_FREQ == 4000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(0);
        #elif _XTAL_FREQ == 2000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(0);
        #elif _XTAL_FREQ == 1000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(0);
        #endif
    #elif PIC_ADC_TAD == 4
        #if _XTAL_FREQ == 32000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(15);
        #elif _XTAL_FREQ == 16000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(7);
        #elif _XTAL_FREQ == 8000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(3);
        #elif _XTAL_FREQ == 4000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(1);
        #elif _XTAL_FREQ == 2000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(0);
        #elif _XTAL_FREQ == 1000000
            #define CVD_GO_DONE_DELAY()     _NOP_DELAY(0);
        #endif
    #endif
#endif

#define mTouch_GetAccumulator(dataPointer)          ((uint24_t)(dataPointer->accumulator.v) & 0xFFFFF)
#define mTouch_GetResult(dataPointer)               (uint16_t)(dataPointer->result.v >> 4)
#define mTouch_SetAccumulator(dataPointer, value)                                   \
    do                                                                              \
    {                                                                               \
        mTouch_ClearAccumulator(dataPointer)                                        \
        dataPointer->accumulator.v |= (uint16_t)(value & 0xFFF);                    \
    } while(0);
#define mTouch_SetResult(dataPointer, value)                                        \
    do                                                                              \
    {                                                                               \
        dataPointer->result.v &= 0x000F;                                            \
        dataPointer->result.v |= (uint16_t)(value << 4);                            \
    } while(0);
#define mTouch_IncResult(dataPointer)                                               \
    do                                                                              \
    {                                                                               \
        dataPointer->result.v += 0x10;                                              \
        result++;                                                                   \
    } while(0);
#define mTouch_DecResult(dataPointer)                                               \
    do                                                                              \
    {                                                                               \
        dataPointer->result.v -= 0x10;                                              \
        result--;                                                                   \
    } while(0);
#define mTouch_UpdateAccumulator(dataPointer)                                       \
    do                                                                              \
    {                                                                               \
        dataPointer->accumulator.v += mTouch_GetResult(dataPointer);                \
    } while(0);
#define mTouch_ClearAccumulator(dataPointer)                                        \
    do                                                                              \
    {                                                                               \
        dataPointer->accumulator.v &= 0xF00000;                                     \
    } while(0);

#define MTOUCH_INDEXMINUSONE_1      0
#define MTOUCH_INDEXMINUSONE_2      1
#define MTOUCH_INDEXMINUSONE_3      2


#if defined(MTOUCH_UNIQUE_REFERENCE_OPTIONS) && defined(CVD_REFERENCE_SENSOR0)  
#define CVD_REFSENSOR_0  CVD_REFERENCE_SENSOR0  
#else    
#define CVD_REFSENSOR_0  MTOUCH_INDEXMINUSONE_0    
#endif
#if defined(MTOUCH_UNIQUE_REFERENCE_OPTIONS) && defined(CVD_REFERENCE_SENSOR1)   
#define CVD_REFSENSOR_1  CVD_REFERENCE_SENSOR1   
#else   
#define CVD_REFSENSOR_1  MTOUCH_INDEXMINUSONE_1    
#endif
#if defined(MTOUCH_UNIQUE_REFERENCE_OPTIONS) && defined(CVD_REFERENCE_SENSOR2)  
#define CVD_REFSENSOR_2  CVD_REFERENCE_SENSOR2   
#else    
#define CVD_REFSENSOR_2  MTOUCH_INDEXMINUSONE_2    
#endif

#if defined(MTOUCH_UNIQUE_SCAN_OPTIONS)
#if defined(CVD_GUARD_IO_0_ENABLED)
    #define 
#endif
#endif


#if defined(CVD_FORCE_REF_DAC) && !defined(PIC_DACOUT_AVAILABLE)
    #error Your currently chosen processor does not have a DACOUT pin available. Please undefine CVD_FORCE_REF_DAC in the configuration file.
#endif
#if defined(CVD_GUARD_DACOUT_ENABLED) && !defined(PIC_DACOUT_AVAILABLE)
    #error Your currently chosen processor does not have a DACOUT pin available. Please undefine CVD_GUARD_DACOUT_ENABLED in the configuration file.
#endif
#if defined(CVD_MUTUAL_ENABLED) && defined(CVD_MUTUAL_EXCLUSIVE_ENABLED)
    #error Both CVD_MUTUAL_ENABLED and CVD_MUTUAL_EXCLUSIVE_ENABLED are defined. Please comment out one of them in the configuration file.
#endif
#if defined(CVD_GUARD_DACOUT_ENABLED) && defined(CVD_GUARD_IO_ENABLED)
    #error Both CVD_GUARD_IO_ENABLED and CVD_GUARD_DACOUT_ENABLED are defined. Please undefine one of them in the configuration file.
#endif

#if (MTOUCH_INTEGRATION_TYPE == MTOUCH_CONTROLS_ISR) && defined(MCOMM_ENABLED) && defined(MCOMM_TWO_WAY_ENABLED)
    #error mComm 2-way communications are not compatible with MTOUCH_INTEGRATION_TYPE == MTOUCH_CONTROLS_ISR, please redefine MTOUCH_INTEGRATION_TYPE to MTOUCH_CALLED_FROM_ISR or MTOUCH_CALLED_FROM_MAINLOOP. An example ISR function to handle the mComm and mTouch interrupts is available in the example main.c file.
#endif

#if (MTOUCH_INTEGRATION_TYPE == MTOUCH_CONTROLS_ISR) || (MTOUCH_INTEGRATION_TYPE == MTOUCH_CALLED_FROM_ISR)
    #if !defined(MTOUCH_ISR_TIMER)
        #define MTOUCH_ISR_TIMER 0
        #warning MTOUCH_ISR_TIMER is not defined in the configuration file. Defaulting to TMR0.
    #endif  
    
    #if MTOUCH_ISR_TIMER == 0
        #if !defined(PIC_TIMER0_AVAILABLE)
            #error Timer0 does not exist for the currently selected microcontroller. Please redefine MTOUCH_ISR_TIMER to an available timer.
        #else
            #define MTOUCH_ISR_TMRxIF       TMR0IF
            #define MTOUCH_ISR_TMRxIE       TMR0IE
            #define MTOUCH_ISR_TMR_ASM      TMR0
            #define MTOUCH_ISR_TMR_C        TMR0
        #endif
    #elif MTOUCH_ISR_TIMER == 1
        #error The mTouch framework is not compatible with TMR1. Please redefine MTOUCH_ISR_TIMER to an 8-bit timer.
    #elif MTOUCH_ISR_TIMER == 3
        #error The mTouch framework is not compatible with TMR3. Please redefine MTOUCH_ISR_TIMER to an 8-bit timer.
    #elif MTOUCH_ISR_TIMER == 5
        #error The mTouch framework is not compatible with TMR5. Please redefine MTOUCH_ISR_TIMER to an 8-bit timer.
    #elif MTOUCH_ISR_TIMER == 7
        #error The mTouch framework is not compatible with TMR7. Please redefine MTOUCH_ISR_TIMER to an 8-bit timer.
    #elif MTOUCH_ISR_TIMER == 9
        #error The mTouch framework is not compatible with TMR9. Please redefine MTOUCH_ISR_TIMER to an 8-bit timer.
    #elif MTOUCH_ISR_TIMER == 2
        #if !defined(PIC_TIMER2_AVAILABLE)
            #error Timer2 does not exist for the currently selected microcontroller. Please redefine MTOUCH_ISR_TIMER to an available timer.
        #else
            #define MTOUCH_ISR_TMRxIF       TMR2IF
            #define MTOUCH_ISR_TMRxIE       TMR2IE
            #define MTOUCH_ISR_TMR_ASM      TMR2
            #define MTOUCH_ISR_TMR_C        TMR2
            #define MTOUCH_ISR_TMR_PEIE_REQUIRED
        #endif
    #elif MTOUCH_ISR_TIMER == 4
        #if !defined(PIC_TIMER4_AVAILABLE)
            #error Timer4 does not exist for the currently selected microcontroller. Please redefine MTOUCH_ISR_TIMER to an available timer.
        #else
            #define MTOUCH_ISR_TMRxIF       TMR4IF
            #define MTOUCH_ISR_TMRxIE       TMR4IE
            #define MTOUCH_ISR_TMR_ASM      TMR4
            #define MTOUCH_ISR_TMR_C        TMR4
            #define MTOUCH_ISR_TMR_PEIE_REQUIRED
        #endif
    #elif MTOUCH_ISR_TIMER == 6
        #if !defined(PIC_TIMER6_AVAILABLE)
            #error Timer6 does not exist for the currently selected microcontroller. Please redefine MTOUCH_ISR_TIMER to an available timer.
        #else
            #define MTOUCH_ISR_TMRxIF       TMR6IF
            #define MTOUCH_ISR_TMRxIE       TMR6IE
            #define MTOUCH_ISR_TMR_ASM      TMR6
            #define MTOUCH_ISR_TMR_C        TMR6
            #define MTOUCH_ISR_TMR_PEIE_REQUIRED
        #endif
    #elif MTOUCH_ISR_TIMER == 8
        #if !defined(PIC_TIMER8_AVAILABLE)
            #error Timer8 does not exist for the currently selected microcontroller. Please redefine MTOUCH_ISR_TIMER to an available timer.
        #else
            #define MTOUCH_ISR_TMRxIF       TMR8IF
            #define MTOUCH_ISR_TMRxIE       TMR8IE
            #define MTOUCH_ISR_TMR_ASM      TMR8
            #define MTOUCH_ISR_TMR_C        TMR8
            #define MTOUCH_ISR_TMR_PEIE_REQUIRED
        #endif
    #elif MTOUCH_ISR_TIMER == 10
        #if !defined(PIC_TIMER10_AVAILABLE)
            #error Timer10 does not exist for the currently selected microcontroller. Please redefine MTOUCH_ISR_TIMER to an available timer.
        #else
            #define MTOUCH_ISR_TMRxIF       TMR10IF
            #define MTOUCH_ISR_TMRxIE       TMR10IE
            #define MTOUCH_ISR_TMR_ASM      TMR10
            #define MTOUCH_ISR_TMR_C        TMR10
            #define MTOUCH_ISR_TMR_PEIE_REQUIRED
        #endif
    #endif
#endif

#if defined(MTOUCH_MOST_PRESSED_ONLY) && (MTOUCH_NUMBER_SENSORS < 2)
    #warning The mTouch Framework's most-pressed algorithm has been enabled but there are not 2 active sensors. Undefining MTOUCH_MOST_PRESSED_ONLY.
    #undef MTOUCH_MOST_PRESSED_ONLY
#endif



    
// Include the macro library header file that corresponds to the currently selected PIC microcontroller's core.
#if defined(_PIC14E)
    // Enhanced Mid-range
    #include "mTouchCVD_macroLibrary_PIC16F1.h"
#else
    #error The currently chosen processor core does not have a matching macroLibrary file associated with it.
#endif    

    #define mTouch_GetPressThreshold(i)         mTouch_pressThreshold[i]  
    #define mTouch_GetReleaseThreshold(i)       mTouch_releaseThreshold[i]  

    #define MTOUCH_pDEBOUNCE_VALUE              MTOUCH_DEBOUNCE_PRESS
    #define MTOUCH_rDEBOUNCE_VALUE              MTOUCH_DEBOUNCE_RELEASE
    #define MTOUCH_pTIMEOUT_VALUE               MTOUCH_BUTTON_TIMEOUT
    #define MTOUCH_SpS_VALUE                    MTOUCH_SAMPLES_PER_SCAN
    #define MTOUCH_SCALING_VALUE                MTOUCH_SCALING
    #define MTOUCH_BASELINE_WEIGHT_VALUE        MTOUCH_BASELINE_WEIGHT
    #define MTOUCH_BASELINE_RATE_VALUE          MTOUCH_BASELINE_RATE
    #define MTOUCH_NEGCAP_VALUE                 MTOUCH_NEGATIVE_CAPACITANCE
    #define MTOUCH_DECIMATION_MAX_VALUE         MTOUCH_DECIMATION_MAX_STEP


    // If no matrix is being implemented...
    #define MTOUCH_SENSOR_START         0
    #define MTOUCH_SENSOR_END           MTOUCH_NUMBER_SENSORS-1
        
    
    #if   defined(PIC_ADC_HCVD_AVAILABLE)
        #define MTOUCH_ADC_INIT()           mTouch_HCVD_Init();
    #else
        #if (PIC_ADC_BITS == 10)
            #define MTOUCH_ADC_INIT()       PIC_ADC_SETCLK();   PIC_ADC_ADFM = PIC_ADC_ADFM_RIGHT;
        #endif
    #endif

    #if (MTOUCH_NUMBER_SENSORS <= 8)
    #define MTOUCH_STATEMASK_BYTES  1
    #endif

    #define MTOUCH_STATEMACHINE_0       &mTouch_ButtonStateMachine
    #define MTOUCH_STATEMACHINE_1       , &mTouch_ButtonStateMachine
    #define MTOUCH_STATEMACHINE_2       , &mTouch_ButtonStateMachine

    #define MTOUCH_CONFIG_EEPROM    0x00

    #define MTOUCH_CVD_OR_CTMU      0x00

    #define MTOUCH_CONFIG_EERAMSTORAGE  0x00
    
    #define MTOUCH_CONFIGURATION_BYTE   (MTOUCH_CVD_OR_CTMU | MTOUCH_CONFIG_EEPROM | MTOUCH_CONFIG_EERAMSTORAGE)
    
    
#endif
/// @endcond
