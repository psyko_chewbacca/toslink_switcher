/*********************************************************************
* FileName:        flash_routines.c
* Dependencies:    See INCLUDES section below
* Processor:       
* Compiler:        
* Company:         Microchip Technology, Inc.
*
* Software License Agreement:
*
* The software supplied herewith by Microchip Technology Incorporated
* (the "Company") for its PICmicro� Microcontroller is intended and
* supplied to you, the Company's customer, for use solely and
* exclusively on Microchip PICmicro Microcontroller products. The
* software is owned by the Company and/or its supplier, and is
* protected under applicable copyright laws. All rights are reserved.
* Any use in violation of the foregoing restrictions may subject the
* user to criminal sanctions under applicable laws, as well as to
* civil liability for the breach of the terms and conditions of this
* license.
*
* THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
* WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
* TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
* IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
* CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
*********************************************************************
* File Description:
*
* Change History:
* Author               Cristian Toma
********************************************************************/

#include <xc.h>


//****************************************************************
//  FLASH MEMORY READ
//  needs 16 bit address pointer in address
//  returns 14 bit value from selected address
//
//****************************************************************
unsigned int flash_memory_read (unsigned int address)
{
    volatile unsigned char lowerAddr =((address)&0xff);
	volatile unsigned char HigherAddr =((address)>>8);	
    
#asm
#include <xc.inc>
    BANKSEL PMADRL
    MOVLW flash_memory_read@lowerAddr
    MOVWF PMADRL
    MOVLW flash_memory_read@HigherAddr
    MOVWF PMADRH

    BCF CFGS
    BSF RD
    NOP
    NOP
#endasm

	return ( (PMDATH)<<8 | (PMDATL) ) ;
}	
//****************************************************************
//  FLASH MEMORY WRITE
//  needs 16 bit address pointer in address, 16 bit data pointer
//
//****************************************************************

void flash_memory_write (unsigned int address, unsigned char *data )
{
    volatile unsigned char lowerAddr =((address)&0xff);
	volatile unsigned char HigherAddr =((address)>>8);	
    volatile unsigned int dataBufAddr = (unsigned int)data;
#asm
    BCF GIE ; //Disable ints so required sequences will execute properly
    BANKSEL PMADRH ; //Bank 3
    MOVF flash_memory_write@HigherAddr,W ; //Load initial address
    MOVWF PMADRH
    MOVF flash_memory_write@lowerAddr,W
    MOVWF PMADRL
    MOVLW LOW flash_memory_write@dataBufAddr ; //Load initial data address
    MOVWF FSR0L
    MOVLW HIGH flash_memory_write@dataBufAddr ; //Load initial data address
    MOVWF FSR0H
    BCF CFGS ; //Not configuration space
    BSF WREN ; //Enable writes
    BSF LWLO ; //Only Load Write Latches
LOOP
    MOVIW FSR0++ ; //Load first data byte into lower
    MOVWF PMDATL
    MOVIW FSR0++ ; //Load second data byte into upper
    MOVWF PMDATH
    MOVF PMADRL,W ; //Check if lower bits of address are '00000'
    XORLW 0x0F ; //Check if we're on the last of 16 addresses
    ANDLW 0x0F
    BTFSC ZERO ; //Exit if last of 16 words,
    GOTO START_WRITE
    MOVLW 55h ; //Start of required write sequence:
    MOVWF PMCON2 ; //Write 55h
    MOVLW 0AAh
    MOVWF PMCON2 ; //Write AAh
    BSF WR ; //Set WR bit to begin write
    NOP; //NOP instructions are forced as processor
    ; //loads program memory write latches
    NOP
    INCF PMADRL,F ; //Still loading latches Increment address
    GOTO LOOP ; //Write next latches
START_WRITE
    BCF LWLO ; //No more loading latches - Actually start Flash program
    ; //memory write
    MOVLW 55h ; //Start of required write sequence:
    MOVWF PMCON2 ; //Write 55h
    MOVLW 0AAh ;
    MOVWF PMCON2 ; //Write AAh
    BSF WR ; //Set WR bit to begin write
    NOP ; //NOP instructions are forced as processor writes
    //all the program memory write latches simultaneously
    NOP ; //to program memory.
    ; //After NOPs, the processor
    ; //stalls until the self-write process in complete
    ; //after write processor continues with 3rd instruction
    BCF WREN ; //Disable writes
    BSF GIE ; //Enable interrupts
#endasm
}
//****************************************************************
//  FLASH MEMORY ERASE
//  Program memory can only be erased by rows. 
//  A row consists of 32 words where the EEADRL<4:0> = 0000.
//
//****************************************************************	
void flash_memory_erase (unsigned int address)
{
    volatile unsigned char ADDRL =((address)&0xff);
	volatile unsigned char ADDRH =((address)>>8);	
#asm
    BCF GIE ; //Disable ints so required sequences will execute properly
    BANKSEL PMADRL
    MOVF flash_memory_erase@ADDRL,W ; //Load lower 8 bits of erase address boundary
    MOVWF PMADRL
    MOVF flash_memory_erase@ADDRH,W ; //Load upper 6 bits of erase address boundary
    MOVWF PMADRH
    BCF CFGS ; //Not configuration space
    BSF FREE ; //Specify an erase operation
    BSF WREN ; //Enable writes
    MOVLW 55h ; //Start of required sequence to initiate erase
    MOVWF PMCON2 ; //Write 55h
    MOVLW 0AAh
    MOVWF PMCON2 ; //Write AAh
    BSF WR ; //Set WR bit to begin erase
    NOP ; //NOP instructions are forced as processor starts
    NOP ; //row erase of program memory.

    ; //The processor stalls until the erase process is complete
    ; //after erase processor continues with 3rd instruction
    BCF WREN ; Disable writes
    BSF GIE ; Enable interrupts

#endasm
}	

