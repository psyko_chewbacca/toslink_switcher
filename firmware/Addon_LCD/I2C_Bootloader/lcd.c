#include "lcd.h"
#include <xc.h>

#define DISP_CLEAR          0x01 // cmd: clear display command

#define DISP_HOME           0x02 // cmd: return cursor to home

#define DISP_ENTRY_MODE_SET 0x04 // cmd: enable cursor moving direction and enable the shift of entire display
#define DISP_S_FLAG         0x01 // Increments(1) or decrements(0) the DDRAM address by 1 when a character code is written
#define DISP_ID_FLAG        0x02 //Shifts the entire display either to the right(0) or to the left(1) when S is 1. The display doe not shift if S is 0.

#define DISP_CONTROL        0x08  //cmd: display ON/OFF control
#define DISP_D_FLAG         0x04  // display ON(1) or OFF(0)
#define DISP_C_FLAG         0x02  // cursor ON(1) or OFF(0)
#define DISP_B_FLAG         0x01  // blinking ON(1) or OFF(0)

#define DISP_SHIFT          0x10  //cmd: set cursor moving and display shift control bit, and the direction without changing of ddram data
#define DISP_SC_FLAG        0x08
#define DISP_RL_FLAG        0x04

#define DISP_FUNCTION_SET   0x20  // cmd: set interface data length
#define DISP_DL_FLAG        0x10  // set interface data length: 8bit(1)/4bit(0)
#define DISP_N_FLAG         0x08  // number of display lines:2-line(1) / 1-line(0)
#define DISP_F_FLAG         0x04  // display font type 5x11 dots(1) or 5x8 dots(0)

#define DISP_CGRAM_SET      0x40  // cmd: set CGRAM address in address counter

#define DISP_DDRAM_SET      0x80  // cmd: set DDRAM address in address counter

enum
{
    lcdInitWaitState = 0U, //15ms duration
    lcdInitState1,  //5ms duration
    lcdInitState2,  //160us duration
    lcdInitState3,  //160us duration
    lcd4BitModeState    , //160us duration
    lcdWriteFunctionSetState1,
    lcdWriteFunctionSetState2            ,
    lcdDisplayOFFState1,
    lcdDisplayOFFState2
#if 0
            ,
    lcdClearDisplayState1,
    lcdClearDisplayState2,
    lcdCursorDirectionState1,
    lcdCursorDirectionState2,
    lcdCursorHomeState1,
    lcdCursorHomeState2,
    lcdWriteBootloaderTextState1,
    lcdWriteBootloaderTextState2
#endif
};

static unsigned char initState = lcdInitWaitState;
static unsigned char timerWaitCounter = 0;

#define BootloaderTextLength 10
#define FirmUpdateTextLength 15
//static const char BootloaderText[BootloaderTextLength + 1] = "Bootloader\0";
//static const char FirmUpdateText[FirmUpdateTextLength + 1] = "Firmware Update\0";

//static void writeTextNibble(unsigned char data);
//static void writeControlNibble(unsigned char data);
static void writeNibble(unsigned char dataNibble);

unsigned char lcdFSM(void)
{

    switch(initState)
    {
        case lcdInitWaitState:
            timerWaitCounter++;
            if(timerWaitCounter == 65)
            {
                //15 ms over
                initState = lcdInitState1;
                timerWaitCounter = 0;
            }
            break;
        case lcdInitState1:
            if(timerWaitCounter == 0) //Send only once
            {
                writeNibble(0x03);
            }
            timerWaitCounter++;
            if(timerWaitCounter == 21)
            {
                //5 ms over
                initState = lcdInitState2;
            }
            break;
        case lcdInitState2:
            writeNibble(0x03);
            initState = lcdInitState3;
            break;
        case lcdInitState3:
            writeNibble(0x03);
            initState = lcd4BitModeState;
            break;
        case lcd4BitModeState:
            writeNibble(0x02);
            initState = lcdWriteFunctionSetState1;
            break;
            //return 1;
            //LCD now in 4-bit mode.

            //High nibble is always sent first
        case lcdWriteFunctionSetState1:
            writeNibble(DISP_FUNCTION_SET >> 4);    //Set 1 line, 5x7 resolution characters
            initState = lcdWriteFunctionSetState2;
            break;
        case lcdWriteFunctionSetState2:
            writeNibble(DISP_FUNCTION_SET);
            //return 1;            
            initState = lcdDisplayOFFState1;
            break;
        case lcdDisplayOFFState1:
            writeNibble(DISP_CONTROL >> 4);   //Set display OFF, cursor off, blink off.
            initState = lcdDisplayOFFState2;
            break;
        case lcdDisplayOFFState2:
            writeNibble(DISP_CONTROL);
            return 1;
#if 0
            initState = lcdClearDisplayState1;
            break;
        case lcdClearDisplayState1:
            writeControlNibble(DISP_CLEAR >> 4);
            initState = lcdClearDisplayState2;
            timerWaitCounter = 0;
            break;
        case lcdClearDisplayState2:
            if(timerWaitCounter == 0) //Send only once
            {
                writeControlNibble(DISP_CLEAR);
            }
            timerWaitCounter++;
            if(timerWaitCounter == 13)
            {
                //3 ms over
                initState = lcdCursorDirectionState1;
            }
            break;
        case lcdCursorDirectionState1:
            writeControlNibble((DISP_ENTRY_MODE_SET | DISP_ID_FLAG) >> 4); //Increment the Cursor after Each Byte Written to Display
            initState = lcdCursorDirectionState2;
            break;
        case lcdCursorDirectionState2:
            writeControlNibble(DISP_ENTRY_MODE_SET | DISP_ID_FLAG);
            initState = lcdCursorHomeState1;
            break;
        case lcdCursorHomeState1:
            writeControlNibble((DISP_CONTROL | DISP_D_FLAG) >> 4); //Set Display ON
            initState = lcdCursorHomeState2;
            break;
        case lcdCursorHomeState2:
            writeControlNibble(DISP_CONTROL | DISP_D_FLAG);
            //initState = lcdWriteBootloaderTextState1;
            //timerWaitCounter = 0;
            //return 1; //Allowing jump to main firmware
            lcdIsInInit = 0;//Init sequence is over. Showing Bootloader on LCD.
            break;
        case lcdWriteBootloaderTextState1:
            writeTextNibble(BootloaderText[timerWaitCounter] >> 4);
            initState = lcdWriteBootloaderTextState2;
            break;
        case lcdWriteBootloaderTextState2:
            writeTextNibble(BootloaderText[timerWaitCounter]);
            if(timerWaitCounter >= BootloaderTextLength)    
            {
                initState = lcdWriteBootloaderTextState1;
                lcdIsInInit = 0;//Init sequence is over. Showing Bootloader on LCD.
            }
            timerWaitCounter++;
            break;
#endif
    }
    return 0;
}
#if 0
static void writeTextNibble(unsigned char data)
{
    LCD_RS = 1;
    writeNibble(data);
}

static void writeControlNibble(unsigned char data)
{
    //LCD_RS = 0;
    writeNibble(data);
}
#endif
static void writeNibble(unsigned char dataNibble)
{
    //Write on datalines
    LCD_D4 = (dataNibble & 0x1) > 0;
    LCD_D5 = (dataNibble & 0x2) > 0;
    LCD_D6 = (dataNibble & 0x4) > 0;
    LCD_D7 = (dataNibble & 0x8) > 0;
    
    //raise 'E' line
    LCD_E = 1;

    //At Fsys of 4Mhz, single instruction period is 250ns
    //HD44780 powered at 5Vdc requires a minimal time of 230ns for LCD_E pulse length
    //A single NOP is all it takes
    //Wait 250ns
    asm("NOP");
    //drop 'E' line
    LCD_E = 0;
}
