/* 
 * File:   lcd.h
 * Author: benjaminfd
 *
 * Created on 29 avril 2016, 11:12
 */

#ifndef LCD_H
#define	LCD_H

#ifdef	__cplusplus
extern "C" {
#endif
    
//LCD IOs define
#define LCD_RS LATAbits.LATA0
#define LCD_E LATAbits.LATA1
#define LCD_D5 LATAbits.LATA4
#define LCD_D4 LATAbits.LATA5
    
#define LCD_D7 LATCbits.LATC4
#define LCD_D6 LATCbits.LATC5

unsigned char lcdFSM(void);    


#ifdef	__cplusplus
}
#endif

#endif	/* LCD_H */

