/*********************************************************************
* FileName:        pksa.c
* Dependencies:    See INCLUDES section below
* Processor:       
* Compiler:        
* Company:         Microchip Technology, Inc.
*
* Software License Agreement:
*
* The software supplied herewith by Microchip Technology Incorporated
* (the "Company") for its PICmicro� Microcontroller is intended and
* supplied to you, the Company's customer, for use solely and
* exclusively on Microchip PICmicro Microcontroller products. The
* software is owned by the Company and/or its supplier, and is
* protected under applicable copyright laws. All rights are reserved.
* Any use in violation of the foregoing restrictions may subject the
* user to criminal sanctions under applicable laws, as well as to
* civil liability for the breach of the terms and conditions of this
* license.
*
* THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
* WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
* TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
* IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
* CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
*********************************************************************
* File Description:
*
* Change History:
* Author               Cristian Toma
********************************************************************/
#include <xc.h>
#include "pksa.h"
#include "flash_routines.h"

const	unsigned char mask = _SSP1STAT_D_nA_MASK | _SSP1STAT_R_nW_MASK | _SSP1STAT_BF_MASK;		//I2C states mask, 0x25

unsigned int dat;

void I2C_Slave_Init()
{
	SSPBUF = 0x0;
	SSPSTAT = 0b00000000;				// 400Khz
	SSPADD = SLAVE_ADDR;	
	SSPCON = 0x36;				// Slave mode, 7bit addr	
	SSPCON3 |= 0b01100000;		// enable start and stop conditions
}

void _WriteData(unsigned char data)
{
	do
	{
		WCOL = 0;
		SSPBUF = data;
	}
	while(WCOL);
	CKP = 1;
}

void do_i2c_tasks(void)
{
    unsigned char genPurposeByte;
	
    if(SSP1IF)
    {
        genPurposeByte = SSP1STAT & mask;	//obtain current state

        if(SSP1STATbits.S)
        {
            switch(genPurposeByte)
            {
                case MWA :								//MASTER WRITES ADDRESS STATE
                    genPurposeByte = SSPBUF;
                    pksa_status = I2C_SLAVE_ADDRESS_RECEIVED;
                break;
                
                case MWD : 								//MASTER WRITES DATA STATE
                    genPurposeByte = SSPBUF;

                    if(pksa_status == I2C_SLAVE_ADDRESS_RECEIVED)
                    {   // first time we get the slave address, after that set to word address
                        pksa_wd_address = genPurposeByte;
                        pksa_index = 0;
                        pksa_status = I2C_WORD_ADDRESS_RECEIVED;
                    }
                    else if (pksa_status == I2C_WORD_ADDRESS_RECEIVED)
                    {	// second time we get the word address, so look into word address 
                        if (pksa_wd_address == SetGetAddressPointer)	// 0x01 is buffer word address
                        {
                            if (pksa_index == 0)
                            {
                                flash_addr_pointer.bytes.byte_H = genPurposeByte;
                                pksa_index++;
                            }
                            else if (pksa_index == 1)
                            {
                                 flash_addr_pointer.bytes.byte_L = genPurposeByte;	
                                 //Received 16(14) bits address
                                 //Command sequence over
                            }
                        }
                        if (pksa_wd_address == DownloadDataToInternalBuf)	// 0x02 write data word address
                        {
                            flash_buffer[pksa_index] = genPurposeByte;
                            if (pksa_index < FlashBufferMaxSizeInBytes)
                            {
                                pksa_index++;  //Auto-increment while still in buffer
                            }
                        }	
                    }					

                break;

                case MRA :								//MASTER READS ADDRESS STATE
                    if (pksa_wd_address == SetGetAddressPointer)			// buffer word address
                    {	
                        // Send first byte here, next byte will be send at MRD case, see below		
                        _WriteData(flash_addr_pointer.bytes.byte_H);
                    }
                    if (pksa_wd_address == ReadFlashCommand)	// read data from flash memory
                    {
                        if (pksa_index == 0)
                        {
                            // read data into flash_buffer(32 bytes)
                            for (genPurposeByte = 0; genPurposeByte < FlashBufferMaxSizeInBytes; genPurposeByte+=2) //Read 1 word at a time
                            {	
                                dat = flash_memory_read(flash_addr_pointer.word.address);
                                flash_buffer[genPurposeByte] = dat >> 8;
                                flash_buffer[genPurposeByte + 1] = dat & 0xFF;
                                flash_addr_pointer.word.address++;
                            }		
                            // send first byte, the rest will be sent at MRD, see below							
                            _WriteData(flash_buffer[pksa_index]);
                            if (pksa_index < FlashBufferMaxSizeInBytes)
                            {
                                pksa_index++;  //Auto-increment while still in buffer
                            }
                        }		
                    }
                    if (pksa_wd_address == EraseFlashCommand)
                    {
                        // erase command, erases a row of 32 words
                        flash_memory_erase(flash_addr_pointer.word.address);
                        flash_addr_pointer.word.address += FlashBufferMaxSizeInBytes;
                        _WriteData(0x00);

                    }
                                        
                    if (pksa_wd_address == WriteFlashCommand)
                    {
                        // write command. What's stored into flash_buffer is written 
                        // to FLASH memory at the address pointed to by the address pointer.
                        // The address pointer automatically increments by 16 units.
                        flash_memory_write(flash_addr_pointer.word.address, (unsigned int)flash_buffer);
                        flash_addr_pointer.word.address += (FlashBufferMaxSizeInBytes / 2); //divided by 2 since address unit is on 16bits
                        _WriteData(0x00);

                    }	
                    if (pksa_wd_address == JumpToApplicationCode)
                    {
                        // jump to appplication code
                        _WriteData(readyToGoToMainFirmware); //0 == not booting, 1 == booting
                        if(readyToGoToMainFirmware == 254)
                        {
                            for (genPurposeByte = 0; genPurposeByte < 255; genPurposeByte++);
                            #asm
                                RESET;
                            #endasm
                        }
                    }	
                    if (pksa_wd_address == GetFirmwareVersionByte)
                    {
                        // get firmware version byte
                        _WriteData(version);
                    }	
                    if(pksa_wd_address == RequestRunStatus)
                    {
                        //Master Request run status. Bootloader returns 0, main firmware returns 1
                        _WriteData(0);
                    }
                break;

                case MRD :		//MASTER READS DATA STATE
                    //Call when reading more than a single byte in one transaction.
                    //
                    if (pksa_wd_address == SetGetAddressPointer)	// buffer word address
                    {		
                        _WriteData(flash_addr_pointer.bytes.byte_L); //Get lower byte of address, higher byte was sent in MRA sequence
                    }
                    if (pksa_wd_address == ReadFlashCommand)    //Send the rest of the bytes that were requested in MRA sequence
                    {
                        _WriteData(flash_buffer[pksa_index]);
                        if (pksa_index < FlashBufferMaxSizeInBytes)
                        {
                            pksa_index++;  //Auto-increment while still in buffer
                        }
                    }								
                break;		
            }
        }
        else if(SSP1STATbits.P) //Stop condition detected
        {	//STOP state	
            asm("nop");
            pksa_status = I2C_NO_TRANSACTION; 
        }	

        SSP1IF = 0;
        SSPEN = 1;														
        CKP = 1;	//release clock
    }
}


