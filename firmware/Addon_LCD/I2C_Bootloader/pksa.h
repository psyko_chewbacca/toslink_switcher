/*********************************************************************
* FileName:        pksa.h
* Dependencies:    See INCLUDES section below
* Processor:       
* Compiler:        
* Company:         Microchip Technology, Inc.
*
* Software License Agreement:
*
* The software supplied herewith by Microchip Technology Incorporated
* (the "Company") for its PICmicro� Microcontroller is intended and
* supplied to you, the Company's customer, for use solely and
* exclusively on Microchip PICmicro Microcontroller products. The
* software is owned by the Company and/or its supplier, and is
* protected under applicable copyright laws. All rights are reserved.
* Any use in violation of the foregoing restrictions may subject the
* user to criminal sanctions under applicable laws, as well as to
* civil liability for the breach of the terms and conditions of this
* license.
*
* THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
* WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
* TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
* IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
* CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
*********************************************************************
* File Description:
*
* Change History:
* Author               Cristian Toma
********************************************************************/

#include "typedef.h"

// pickit serial communication states
#define I2C_NO_TRANSACTION					0
#define I2C_SLAVE_ADDRESS_RECEIVED			1
#define I2C_WORD_ADDRESS_RECEIVED			2
#define I2C_READ_ADDRESS					3
#define I2C_READ_DATA						4
#define I2C_MASTER_NACK						5

// slave address definition
#define SLAVE_ADDR 0xa0 			

//What interests us here are bits :
//bit5: "SSP1STATbits.D_nA" - Data payload(1) or address(0)
//bit2; "SSP1STATbits.R_nW" - Master requests read(1) or write(0)
//bit1; "SSP1STATbits.BF" - Received whole byte(1) or not yet(0)
// define states
#define MWA 0x1			//Master Writes Address 
#define MWD 0x21		//Master Writes Data
#define MRA 0x5			//Master Reads Address
#define MRD 0x24		//Master Reads Data


//Bootloader commands
#define SetGetAddressPointer      0x01 //read and write
#define DownloadDataToInternalBuf 0x02 //write only
#define ReadFlashCommand          0x03 //read only
#define EraseFlashCommand         0x04 //read only
#define WriteFlashCommand         0x05 //read only
#define JumpToApplicationCode     0x06 //read only
#define GetFirmwareVersionByte    0x07 //read only
#define RequestRunStatus          0x08 //read only

// address typdef
extern ADDRESS	flash_addr_pointer;


// function prototypes
void I2C_Slave_Init(void);
void  do_i2c_tasks(void);

// externs
extern unsigned char nrbytes; 
extern unsigned char flash_buffer[];
extern unsigned char pksa_index;
extern unsigned char pksa_wd_address;
extern unsigned char pksa_status;
extern unsigned char pksa_cmd;
extern unsigned char readyToGoToMainFirmware;
extern unsigned int version;

//defines
#define FlashBufferMaxSizeInBytes 32



