/*********************************************************************
* FileName:        flash_routines.c
* Dependencies:    See INCLUDES section below
* Processor:       
* Compiler:        
* Company:         Microchip Technology, Inc.
*
* Software License Agreement:
*
* The software supplied herewith by Microchip Technology Incorporated
* (the "Company") for its PICmicro� Microcontroller is intended and
* supplied to you, the Company's customer, for use solely and
* exclusively on Microchip PICmicro Microcontroller products. The
* software is owned by the Company and/or its supplier, and is
* protected under applicable copyright laws. All rights are reserved.
* Any use in violation of the foregoing restrictions may subject the
* user to criminal sanctions under applicable laws, as well as to
* civil liability for the breach of the terms and conditions of this
* license.
*
* THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
* WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
* TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
* IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
* CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
*********************************************************************
* File Description:
*
* Change History:
* Author               Cristian Toma
********************************************************************/

#include <xc.h>


//****************************************************************
//  FLASH MEMORY READ
//  needs 16 bit address pointer in address
//  returns 14 bit value from selected address
//
//****************************************************************
unsigned int flash_memory_read (unsigned int address)
{

	PMADRL=((address)&0xff);
	PMADRH=((address)>>8);	
	CFGS = 0;					// access FLASH program, not config

	RD = 1;
	#asm
		NOP
		NOP
	#endasm
	return ( (PMDATH)<<8 | (PMDATL) ) ;
}	
//****************************************************************
//  FLASH MEMORY WRITE
//  needs 16 bit address pointer in address, 16 bit data pointer
//
//****************************************************************

void flash_memory_write (unsigned int address, unsigned char *data )
{
		unsigned char wdi;
	
		PMADRL=((address)&0xff);	// load address
		PMADRH=((address)>>8);		// load address
	
		for (wdi=0;wdi<14;wdi+=2)
		{
			PMDATH = data[wdi];
			PMDATL = data[wdi+1];
		
			CFGS = 0;					// access FLASH program, not config
			WREN = 1;					// allow program/erase
			LWLO = 1;					// only load latches
			PMCON2 = 0x55;
			PMCON2 = 0xAA;
			
			
			WR = 1;						// set WR to begin write
			#asm
				NOP
				NOP
			#endasm
			
			PMADRL++;
		}	

		PMDATH = data[14];
		PMDATL = data[15];
		CFGS = 0;					// access FLASH program, not config
		WREN = 1;					// allow program/erase
		
		LWLO = 0;					// this time start write
		PMCON2 = 0x55;				
		PMCON2 = 0xAA;				
		WR = 1;						// set WR to begin write
		#asm
			NOP
			NOP
		#endasm

		
		WREN = 0;					// disallow program/erase
		
}
//****************************************************************
//  FLASH MEMORY ERASE
//  Program memory can only be erased by rows. 
//  A row consists of 32 words where the EEADRL<4:0> = 0000.
//
//****************************************************************	
void flash_memory_erase (unsigned int address)
{
        GIE = 0;
		PMADRL=((address)&0xff);	// load address
		PMADRH=((address)>>8);		// load address
		CFGS = 0;					// access FLASH program, not config
        FREE = 1;
		WREN = 1;					// allow program/erase		
		PMCON2 = 0x55;				// required sequence
		PMCON2 = 0xAA;				// required sequence
		WR = 1;						// set WR to begin erase cycle
		WREN = 0;					// disallow program/erase	
        GIE = 1;
}	
