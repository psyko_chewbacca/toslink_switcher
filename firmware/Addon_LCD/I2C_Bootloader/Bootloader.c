/*********************************************************************
* FileName:        Bootloader.c
* Dependencies:    See INCLUDES section below
* Processor:       
* Compiler:        
* Company:         Microchip Technology, Inc.
*
* Software License Agreement:
*
* The software supplied herewith by Microchip Technology Incorporated
* (the "Company") for its PICmicro� Microcontroller is intended and
* supplied to you, the Company's customer, for use solely and
* exclusively on Microchip PICmicro Microcontroller products. The
* software is owned by the Company and/or its supplier, and is
* protected under applicable copyright laws. All rights are reserved.
* Any use in violation of the foregoing restrictions may subject the
* user to criminal sanctions under applicable laws, as well as to
* civil liability for the breach of the terms and conditions of this
* license.
*
* THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
* WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
* TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
* IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
* CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
*********************************************************************
* File Description:
*
* Change History:
* Author               Cristian Toma
********************************************************************/


#include <xc.h>
#include "pksa.h"
#include "flash_routines.h"
#include "fuses.h"
#include "lcd.h"

unsigned char flash_buffer[FlashBufferMaxSizeInBytes];
unsigned char pksa_wd_address;
unsigned char pksa_index;
unsigned char pksa_status;
unsigned int version;
unsigned char readyToGoToMainFirmware = 0;

ADDRESS	flash_addr_pointer;

void baseInit(void);


//*****************************************************************************
// 	This project must be compiled with :
//	Optimization settings : SPEED must be set  
// 	ROM Ranges = 0 - 1FF 
//	Additional command line :  -L-pstrings=CODE
//  *All values here are in hex.
//*****************************************************************************



// The bootloader code does not use any interrupts.
// However, the downloaded code may use interrupts.
// The interrupt vector on a PIC16F1703 is located at 
// address 0x0004. The following function will be located 
// at the interrupt vector and will contain a jump to
// 0x0404
void interrupt service_isr()
{
	#asm
		GOTO	0x204;
	#endasm
}	
	

void main()
{
   
	baseInit();
	I2C_Slave_Init();
    
    version = flash_memory_read(0x7FF);
	
    // main program loop
    while (1)
    {
        do_i2c_tasks();	
        if(readyToGoToMainFirmware == 0 && INTCONbits.T0IF)
        {
            if(lcdFSM())
            {
                readyToGoToMainFirmware = 1;
            }
            
            INTCONbits.T0IF = 0;
        }
    }	
}					

void baseInit()
{
	OSCCON = 0xFA;  //16MHz, internal osc
    //HFINTOSC must be ready and stable before continuing
#ifndef __DEBUG
    while(OSCSTATbits.HFIOFR == 0 || OSCSTATbits.HFIOFS == 0);  
#endif
    
    INTCON = 0; //No interrupts enabled, reset Timer0 overflow flag
    
    //Further init here
    //bit7: 0 - weak pullups enabled (inverted)
    //bit6: 0 - INT on falling edge (don't care)
    //bit5: 0 - TMR0 on internal clock
    //bit4: 0 - don't care
    //bit3: 0 - prescaler used for TMR0
    //bit2-0: 0b001 - 1:4 prescaler value
    OPTION_REG = 0b00000001;
    // Fcount period: 1/(16MHz / 4) = 250ns
    // 8-bit timer: 250ns * 256 = 64us
    // 1:4 prescaler: 64us * 4 = 256us
    //XXX: Does T0 sync circuit multiply timer length by 2??? (See note in section 18.1.1)
    //TMR0 = 0;
    
    LATA = 0;   //Could be skipped to gain program space
    LATC = 0;
    
    //TRISC1 = 1;			// SDA input
	//TRISC0 = 1;			// SCL input
    TRISA = 0b11000100;     // RA3 is reset
    TRISC = 0b11000011;     // Rest are Either LCD signals or touch sensors.
}
