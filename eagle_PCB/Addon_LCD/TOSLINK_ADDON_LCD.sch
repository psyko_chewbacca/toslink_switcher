<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="no" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="ATT_MISO" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="prix" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="TOSLINK_switcher">
<packages>
<package name="HEADER_2X5_INVERT">
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="-3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="6.35" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="-6.35" y2="-2.54" width="0.1524" layer="21"/>
<pad name="2" x="-5.08" y="-1.27" drill="1.02" diameter="1.632"/>
<pad name="1" x="-5.08" y="1.27" drill="1.02" diameter="1.632" shape="square"/>
<pad name="4" x="-2.54" y="-1.27" drill="1.02" diameter="1.632"/>
<pad name="3" x="-2.54" y="1.27" drill="1.02" diameter="1.632"/>
<pad name="6" x="0" y="-1.27" drill="1.02" diameter="1.632"/>
<pad name="5" x="0" y="1.27" drill="1.02" diameter="1.632"/>
<pad name="8" x="2.54" y="-1.27" drill="1.02" diameter="1.632"/>
<pad name="7" x="2.54" y="1.27" drill="1.02" diameter="1.632"/>
<pad name="10" x="5.08" y="-1.27" drill="1.02" diameter="1.632"/>
<pad name="9" x="5.08" y="1.27" drill="1.02" diameter="1.632" first="yes"/>
<text x="-6.35" y="2.794" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<wire x1="-6.35" y1="0" x2="-3.81" y2="0" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
</package>
<package name="TOUCH_PASSIVE_10MM">
<pad name="BUTTON" x="0" y="4.064" drill="0.3556" stop="no" thermals="no"/>
<polygon width="0.6096" layer="1">
<vertex x="-4.75" y="0" curve="-90"/>
<vertex x="0" y="4.75" curve="-90"/>
<vertex x="4.75" y="0" curve="-90"/>
<vertex x="0" y="-4.75" curve="-90"/>
</polygon>
<polygon width="0.254" layer="41" spacing="0.254">
<vertex x="-5.334" y="0.254" curve="-90"/>
<vertex x="-0.254" y="5.334"/>
<vertex x="0.254" y="5.334" curve="-90"/>
<vertex x="5.334" y="0.254"/>
<vertex x="5.334" y="-0.254"/>
<vertex x="5.334" y="-0.508" curve="-90"/>
<vertex x="0.508" y="-5.334"/>
<vertex x="0" y="-5.334"/>
<vertex x="-0.254" y="-5.334" curve="-90"/>
<vertex x="-5.334" y="-0.254"/>
<vertex x="-6.096" y="-0.254" curve="90"/>
<vertex x="-0.254" y="-6.096"/>
<vertex x="0.254" y="-6.096"/>
<vertex x="0.508" y="-6.096" curve="90"/>
<vertex x="6.096" y="-0.508"/>
<vertex x="6.096" y="0"/>
<vertex x="6.096" y="0.254" curve="90"/>
<vertex x="0.254" y="6.096"/>
<vertex x="-0.254" y="6.096" curve="90"/>
<vertex x="-6.096" y="0.254"/>
</polygon>
<circle x="0" y="0" radius="5.842" width="0.762" layer="21"/>
<text x="0" y="-7.112" size="0.6096" layer="25" font="vector" ratio="15">&gt;NAME</text>
<polygon width="0.254" layer="43" spacing="0.254">
<vertex x="0" y="6.35" curve="90"/>
<vertex x="-6.35" y="0" curve="90"/>
<vertex x="0" y="-6.35" curve="90"/>
<vertex x="6.35" y="0" curve="90"/>
</polygon>
</package>
<package name="PIC_ICSP_RIGHT_ANGLE">
<wire x1="-6.05" y1="3.685" x2="-4.11" y2="3.685" width="0.1524" layer="21"/>
<wire x1="-4.11" y1="3.685" x2="-3.81" y2="3.385" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.695" x2="-4.11" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-4.11" y1="1.395" x2="-6.05" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-6.05" y1="1.395" x2="-6.35" y2="1.695" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.695" x2="-6.35" y2="3.385" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="3.385" x2="-6.05" y2="3.685" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="3.385" x2="-3.81" y2="3.385" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.695" x2="-3.81" y2="1.695" width="0.1524" layer="21"/>
<wire x1="-6.05" y1="3.685" x2="-6.05" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-4.11" y1="3.685" x2="-4.11" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-5.41" y1="-0.055" x2="-4.75" y2="-0.055" width="0.1524" layer="51"/>
<wire x1="-4.75" y1="-0.055" x2="-4.75" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="-4.75" y1="-0.715" x2="-5.41" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="-5.41" y1="-0.715" x2="-5.41" y2="-0.055" width="0.1524" layer="51"/>
<wire x1="-3.51" y1="3.685" x2="-1.57" y2="3.685" width="0.1524" layer="21"/>
<wire x1="-1.57" y1="3.685" x2="-1.27" y2="3.385" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.695" x2="-1.57" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-1.57" y1="1.395" x2="-3.51" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-3.51" y1="1.395" x2="-3.81" y2="1.695" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.695" x2="-3.81" y2="3.385" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="3.385" x2="-3.51" y2="3.685" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="3.385" x2="-1.27" y2="3.385" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.695" x2="-1.27" y2="1.695" width="0.1524" layer="21"/>
<wire x1="-3.51" y1="3.685" x2="-3.51" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-1.57" y1="3.685" x2="-1.57" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-2.87" y1="-0.055" x2="-2.21" y2="-0.055" width="0.1524" layer="51"/>
<wire x1="-2.21" y1="-0.055" x2="-2.21" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="-2.21" y1="-0.715" x2="-2.87" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="-2.87" y1="-0.715" x2="-2.87" y2="-0.055" width="0.1524" layer="51"/>
<wire x1="-0.97" y1="3.685" x2="0.97" y2="3.685" width="0.1524" layer="21"/>
<wire x1="0.97" y1="3.685" x2="1.27" y2="3.385" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.695" x2="0.97" y2="1.395" width="0.1524" layer="21"/>
<wire x1="0.97" y1="1.395" x2="-0.97" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-0.97" y1="1.395" x2="-1.27" y2="1.695" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.695" x2="-1.27" y2="3.385" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.385" x2="-0.97" y2="3.685" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.385" x2="1.27" y2="3.385" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.695" x2="1.27" y2="1.695" width="0.1524" layer="21"/>
<wire x1="-0.97" y1="3.685" x2="-0.97" y2="1.395" width="0.1524" layer="21"/>
<wire x1="0.97" y1="3.685" x2="0.97" y2="1.395" width="0.1524" layer="21"/>
<wire x1="-0.33" y1="-0.055" x2="0.33" y2="-0.055" width="0.1524" layer="51"/>
<wire x1="0.33" y1="-0.055" x2="0.33" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="0.33" y1="-0.715" x2="-0.33" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="-0.33" y1="-0.715" x2="-0.33" y2="-0.055" width="0.1524" layer="51"/>
<wire x1="1.57" y1="3.685" x2="3.51" y2="3.685" width="0.1524" layer="21"/>
<wire x1="3.51" y1="3.685" x2="3.81" y2="3.385" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.695" x2="3.51" y2="1.395" width="0.1524" layer="21"/>
<wire x1="3.51" y1="1.395" x2="1.57" y2="1.395" width="0.1524" layer="21"/>
<wire x1="1.57" y1="1.395" x2="1.27" y2="1.695" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.695" x2="1.27" y2="3.385" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.385" x2="1.57" y2="3.685" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.385" x2="3.81" y2="3.385" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.695" x2="3.81" y2="1.695" width="0.1524" layer="21"/>
<wire x1="1.57" y1="3.685" x2="1.57" y2="1.395" width="0.1524" layer="21"/>
<wire x1="3.51" y1="3.685" x2="3.51" y2="1.395" width="0.1524" layer="21"/>
<wire x1="2.21" y1="-0.055" x2="2.87" y2="-0.055" width="0.1524" layer="51"/>
<wire x1="2.87" y1="-0.055" x2="2.87" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="2.87" y1="-0.715" x2="2.21" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="2.21" y1="-0.715" x2="2.21" y2="-0.055" width="0.1524" layer="51"/>
<wire x1="4.11" y1="3.685" x2="6.05" y2="3.685" width="0.1524" layer="21"/>
<wire x1="6.05" y1="3.685" x2="6.35" y2="3.385" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.385" x2="6.35" y2="1.695" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.695" x2="6.05" y2="1.395" width="0.1524" layer="21"/>
<wire x1="6.05" y1="1.395" x2="4.11" y2="1.395" width="0.1524" layer="21"/>
<wire x1="4.11" y1="1.395" x2="3.81" y2="1.695" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.695" x2="3.81" y2="3.385" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.385" x2="4.11" y2="3.685" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.385" x2="6.35" y2="3.385" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.695" x2="6.35" y2="1.695" width="0.1524" layer="21"/>
<wire x1="4.11" y1="3.685" x2="4.11" y2="1.395" width="0.1524" layer="21"/>
<wire x1="6.05" y1="3.685" x2="6.05" y2="1.395" width="0.1524" layer="21"/>
<wire x1="4.75" y1="-0.055" x2="5.41" y2="-0.055" width="0.1524" layer="51"/>
<wire x1="5.41" y1="-0.055" x2="5.41" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="5.41" y1="-0.715" x2="4.75" y2="-0.715" width="0.1524" layer="51"/>
<wire x1="4.75" y1="-0.715" x2="4.75" y2="-0.055" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="-0.385" drill="1.02" diameter="1.632" shape="square" first="yes"/>
<pad name="2" x="-2.54" y="-0.385" drill="1.02" diameter="1.632"/>
<pad name="3" x="0" y="-0.385" drill="1.02" diameter="1.632"/>
<pad name="4" x="2.54" y="-0.385" drill="1.02" diameter="1.632"/>
<pad name="5" x="5.08" y="-0.385" drill="1.02" diameter="1.632"/>
<text x="-5.73" y="10.54" size="1.27" layer="21" ratio="10">&gt;NAME</text>
<rectangle x1="-5.41" y1="0.532" x2="-4.75" y2="1.395" layer="21"/>
<rectangle x1="-5.41" y1="-0.055" x2="-4.75" y2="0.532" layer="51"/>
<rectangle x1="-2.87" y1="0.532" x2="-2.21" y2="1.395" layer="21"/>
<rectangle x1="-2.87" y1="-0.055" x2="-2.21" y2="0.532" layer="51"/>
<rectangle x1="-0.33" y1="0.532" x2="0.33" y2="1.395" layer="21"/>
<rectangle x1="-0.33" y1="-0.055" x2="0.33" y2="0.532" layer="51"/>
<rectangle x1="2.21" y1="0.532" x2="2.87" y2="1.395" layer="21"/>
<rectangle x1="2.21" y1="-0.055" x2="2.87" y2="0.532" layer="51"/>
<rectangle x1="4.75" y1="0.532" x2="5.41" y2="1.395" layer="21"/>
<rectangle x1="4.75" y1="-0.055" x2="5.41" y2="0.532" layer="51"/>
<polygon width="0.1524" layer="21">
<vertex x="-5.41" y="3.685"/>
<vertex x="-5.41" y="9.125"/>
<vertex x="-5.19" y="9.885"/>
<vertex x="-4.97" y="9.885"/>
<vertex x="-4.75" y="9.125"/>
<vertex x="-4.75" y="3.685"/>
</polygon>
<polygon width="0.1524" layer="21">
<vertex x="-2.87" y="3.685"/>
<vertex x="-2.87" y="9.125"/>
<vertex x="-2.65" y="9.885"/>
<vertex x="-2.43" y="9.885"/>
<vertex x="-2.21" y="9.125"/>
<vertex x="-2.21" y="3.685"/>
</polygon>
<polygon width="0.1524" layer="21">
<vertex x="-0.33" y="3.685"/>
<vertex x="-0.33" y="9.125"/>
<vertex x="-0.11" y="9.885"/>
<vertex x="0.11" y="9.885"/>
<vertex x="0.33" y="9.125"/>
<vertex x="0.33" y="3.685"/>
</polygon>
<polygon width="0.1524" layer="21">
<vertex x="2.21" y="3.685"/>
<vertex x="2.21" y="9.125"/>
<vertex x="2.43" y="9.885"/>
<vertex x="2.65" y="9.885"/>
<vertex x="2.87" y="9.125"/>
<vertex x="2.87" y="3.685"/>
</polygon>
<polygon width="0.1524" layer="21">
<vertex x="4.75" y="3.685"/>
<vertex x="4.75" y="9.125"/>
<vertex x="4.97" y="9.885"/>
<vertex x="5.19" y="9.885"/>
<vertex x="5.41" y="9.125"/>
<vertex x="5.41" y="3.685"/>
</polygon>
</package>
<package name="SOIC14">
<wire x1="4.305" y1="-1.9" x2="-4.305" y2="-1.9" width="0.2032" layer="25"/>
<wire x1="-4.305" y1="-1.9" x2="-4.305" y2="-1.4" width="0.2032" layer="25"/>
<wire x1="-4.305" y1="-1.4" x2="-4.305" y2="1.9" width="0.2032" layer="25"/>
<wire x1="4.305" y1="-1.4" x2="-4.305" y2="-1.4" width="0.2032" layer="25"/>
<wire x1="4.305" y1="1.9" x2="4.305" y2="-1.4" width="0.2032" layer="25"/>
<wire x1="4.305" y1="-1.4" x2="4.305" y2="-1.9" width="0.2032" layer="25"/>
<wire x1="-4.305" y1="1.9" x2="4.305" y2="1.9" width="0.2032" layer="25"/>
<smd name="2" x="-2.54" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="-2.54" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-3.81" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-1.27" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="0" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="-3.81" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="-1.27" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="0" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="2.54" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="2.54" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.27" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="3.81" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="1.27" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="3.81" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-4.572" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.842" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.0551" y1="-3.1001" x2="-3.5649" y2="-2" layer="51"/>
<rectangle x1="-2.7851" y1="-3.1001" x2="-2.2949" y2="-2" layer="51"/>
<rectangle x1="-1.5151" y1="-3.1001" x2="-1.0249" y2="-2" layer="51"/>
<rectangle x1="-0.2451" y1="-3.1001" x2="0.2451" y2="-2" layer="51"/>
<rectangle x1="-0.2451" y1="2" x2="0.2451" y2="3.1001" layer="51"/>
<rectangle x1="-1.5151" y1="2" x2="-1.0249" y2="3.1001" layer="51"/>
<rectangle x1="-2.7851" y1="2" x2="-2.2949" y2="3.1001" layer="51"/>
<rectangle x1="-4.0551" y1="2" x2="-3.5649" y2="3.1001" layer="51"/>
<rectangle x1="1.0249" y1="-3.1001" x2="1.5151" y2="-2" layer="51"/>
<rectangle x1="2.2949" y1="-3.1001" x2="2.7851" y2="-2" layer="51"/>
<rectangle x1="3.5649" y1="-3.1001" x2="4.0551" y2="-2" layer="51"/>
<rectangle x1="3.5649" y1="2" x2="4.0551" y2="3.1001" layer="51"/>
<rectangle x1="2.2949" y1="2" x2="2.7851" y2="3.1001" layer="51"/>
<rectangle x1="1.0249" y1="2" x2="1.5151" y2="3.1001" layer="51"/>
<circle x="-3.7" y="-0.75" radius="0.35" width="0.127" layer="25"/>
</package>
<package name="SOT-23">
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1524" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.8636" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="2.2225" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.4925" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="TOP_TRIM_POT">
<wire x1="-2.25" y1="4.65" x2="2.25" y2="4.65" width="0.254" layer="21"/>
<wire x1="2.25" y1="4.65" x2="2.25" y2="-4.65" width="0.254" layer="21"/>
<wire x1="2.25" y1="-4.65" x2="-2.25" y2="-4.65" width="0.254" layer="21"/>
<wire x1="-2.25" y1="-4.65" x2="-2.25" y2="4.65" width="0.254" layer="21"/>
<circle x="0" y="-2.55" radius="1.1011" width="0.1524" layer="21"/>
<pad name="1" x="0" y="-2.54" drill="0.6096" diameter="1.4224"/>
<pad name="2" x="0" y="0" drill="0.6096" diameter="1.4224" rot="R180"/>
<pad name="3" x="0" y="2.54" drill="0.6096" diameter="1.4224"/>
<text x="-2.65" y="-4.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.95" y="-4.5" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-3.6" x2="0.15" y2="-1.5" layer="51"/>
</package>
<package name="SMD_PAD">
<smd name="1" x="0" y="0" dx="1.3" dy="2.8" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="GND">
<pin name="GND" x="0" y="0" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="HEADER_2X5">
<wire x1="-3.81" y1="7.62" x2="6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="6.35" y1="7.62" x2="6.35" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-7.62" x2="-3.81" y2="7.62" width="0.4064" layer="94"/>
<text x="-3.81" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="BUT_PASSIVE">
<circle x="0" y="0" radius="10.16" width="0.6096" layer="94"/>
<pin name="BUTTON" x="15.24" y="0" visible="pin" length="middle" direction="pas" rot="R180"/>
<text x="12.7" y="-5.08" size="1.27" layer="94" font="vector" ratio="15">Capacitive
Button</text>
<text x="12.7" y="-7.62" size="1.27" layer="95" font="vector" ratio="15">&gt;NAME</text>
<wire x1="15.24" y1="0" x2="10.16" y2="0" width="0.6096" layer="94"/>
<text x="12.7" y="-10.16" size="1.27" layer="96" font="vector" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="PIC_ICSP">
<wire x1="-6.604" y1="-7.62" x2="-6.604" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.604" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="7.62" x2="3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="-6.604" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.6675" y="8.255" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-2.032" y="4.318" size="1.27" layer="95" font="vector" ratio="12">!MCLR</text>
<text x="-2.032" y="2.032" size="1.27" layer="95" font="vector" ratio="12">+V</text>
<text x="-2.032" y="-0.508" size="1.27" layer="95" font="vector" ratio="12">GND</text>
<text x="-2.032" y="-3.048" size="1.27" layer="95" font="vector" ratio="12">PGD</text>
<text x="-2.032" y="-5.588" size="1.27" layer="95" font="vector" ratio="12">PGC</text>
<pin name="+V" x="-5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="/MCLR" x="-5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="GND" x="-5.08" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="PGC" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="PGD" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PIC16F1503">
<wire x1="-25.4" y1="10.16" x2="22.86" y2="10.16" width="0.254" layer="94"/>
<wire x1="22.86" y1="10.16" x2="22.86" y2="-12.7" width="0.254" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="-25.4" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-12.7" x2="-25.4" y2="10.16" width="0.254" layer="94"/>
<text x="-12.192" y="7.62" size="1.524" layer="95">VDD</text>
<text x="-12.446" y="-11.938" size="1.524" layer="95">VSS</text>
<text x="2.54" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="-10.16" y="12.7" visible="pad" length="short" direction="pwr" rot="R270"/>
<pin name="OSC1/CLKIN/T1CKI/RA5" x="-27.94" y="2.54" length="short"/>
<pin name="T1G/OSC2/CLKOUT/AN3/RA4" x="-27.94" y="0" length="short"/>
<pin name="VPP/MCLR/RA3" x="-27.94" y="5.08" length="short" direction="in"/>
<pin name="PWM1/RC5" x="25.4" y="5.08" length="short" rot="R180"/>
<pin name="RC4" x="25.4" y="2.54" length="short" rot="R180"/>
<pin name="PWM2/RC3/AN7" x="25.4" y="0" length="short" rot="R180"/>
<pin name="RC2/AN6" x="25.4" y="-2.54" length="short" rot="R180"/>
<pin name="SDA/RC1/AN5" x="25.4" y="-5.08" length="short" rot="R180"/>
<pin name="SCL/RC0/AN4" x="25.4" y="-7.62" length="short" rot="R180"/>
<pin name="C1OUT/T0CKI/INT/AN2/RA2" x="-27.94" y="-2.54" length="short"/>
<pin name="C1IN0-/VREF/ICSPCLK/AN1/RA1" x="-27.94" y="-5.08" length="short"/>
<pin name="C1IN+/ICSPDAT/AN0/RA0" x="-27.94" y="-7.62" length="short"/>
<pin name="VSS" x="-10.16" y="-15.24" visible="pad" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="TRIM_POT">
<wire x1="0.762" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="-1.8796" y2="1.7526" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.286" y1="1.27" x2="-1.651" y2="2.413" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-3.048" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-2.032" y2="-1.524" width="0.1524" layer="94"/>
<text x="-5.969" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="E" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="S" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="PAD">
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<pin name="1" x="0" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND">
<gates>
<gate name="G$1" symbol="GND" x="0" y="5.08"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V">
<gates>
<gate name="G$1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EXT-IO_FEMALE">
<gates>
<gate name="G$1" symbol="HEADER_2X5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HEADER_2X5_INVERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_BUT_10MM">
<gates>
<gate name="G$1" symbol="BUT_PASSIVE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TOUCH_PASSIVE_10MM">
<connects>
<connect gate="G$1" pin="BUTTON" pad="BUTTON"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIC_ICSP_RIGHT_ANGLE">
<gates>
<gate name="G$1" symbol="PIC_ICSP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIC_ICSP_RIGHT_ANGLE">
<connects>
<connect gate="G$1" pin="+V" pad="2"/>
<connect gate="G$1" pin="/MCLR" pad="1"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="PGC" pad="5"/>
<connect gate="G$1" pin="PGD" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIC16F1503-SO">
<gates>
<gate name="G$1" symbol="PIC16F1503" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC14">
<connects>
<connect gate="G$1" pin="C1IN+/ICSPDAT/AN0/RA0" pad="13"/>
<connect gate="G$1" pin="C1IN0-/VREF/ICSPCLK/AN1/RA1" pad="12"/>
<connect gate="G$1" pin="C1OUT/T0CKI/INT/AN2/RA2" pad="11"/>
<connect gate="G$1" pin="OSC1/CLKIN/T1CKI/RA5" pad="2"/>
<connect gate="G$1" pin="PWM1/RC5" pad="5"/>
<connect gate="G$1" pin="PWM2/RC3/AN7" pad="7"/>
<connect gate="G$1" pin="RC2/AN6" pad="8"/>
<connect gate="G$1" pin="RC4" pad="6"/>
<connect gate="G$1" pin="SCL/RC0/AN4" pad="10"/>
<connect gate="G$1" pin="SDA/RC1/AN5" pad="9"/>
<connect gate="G$1" pin="T1G/OSC2/CLKOUT/AN3/RA4" pad="3"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VPP/MCLR/RA3" pad="4"/>
<connect gate="G$1" pin="VSS" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSISTOR_BJT_NPN">
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRIM_POT_TOP">
<gates>
<gate name="G$1" symbol="TRIM_POT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TOP_TRIM_POT">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="E" pad="3"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1X1_SMD_PAD">
<gates>
<gate name="G$1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMD_PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dp_devices">
<description>Dangerous Prototypes Standard PCB sizes
http://dangerousprototypes.com</description>
<packages>
<package name="HEADER_PRG_1X05_ICSP_B">
<description>PIC ICSP Header with triangular marker above pin 1</description>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-3.81" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="square"/>
<pad name="2" x="-2.54" y="0" drill="1.016"/>
<pad name="3" x="0" y="0" drill="1.016"/>
<pad name="4" x="2.54" y="0" drill="1.016"/>
<pad name="5" x="5.08" y="0" drill="1.016"/>
<text x="-6.35" y="-2.8575" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="48"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="48"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="48"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="48"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="48"/>
<polygon width="0.0634" layer="21">
<vertex x="-5.08" y="1.905"/>
<vertex x="-4.445" y="2.54"/>
<vertex x="-5.715" y="2.54"/>
</polygon>
</package>
<package name="HEADER_PRG_1X05">
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-3.81" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="-3.81" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="square" first="yes"/>
<pad name="2" x="-2.54" y="0" drill="1.016"/>
<pad name="3" x="0" y="0" drill="1.016"/>
<pad name="4" x="2.54" y="0" drill="1.016"/>
<pad name="5" x="5.08" y="0" drill="1.016"/>
<text x="-6.35" y="1.5875" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="48"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="48"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="48"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="48"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="48"/>
<polygon width="0.0634" layer="21">
<vertex x="-5.08" y="-1.905"/>
<vertex x="-5.715" y="-2.54"/>
<vertex x="-4.445" y="-2.54"/>
</polygon>
</package>
<package name="CON_POGO_ICSP_PROG">
<description>&lt;B&gt;Pogo pin ICSP programmer side footprint &lt;/B&gt;
&lt;BR/&gt; pogo pin diameter 0.65mm
&lt;BR/&gt; total pogo diameter 1.70mm
&lt;BR/&gt; pinout from top left: MCLR, GND, VCC
&lt;BR/&gt; pinout from bottom left: PGD, PGC, KEY(NC)</description>
<wire x1="1.5" y1="1.866" x2="0.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="0.5" y1="1.866" x2="-1.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="1.866" x2="-2.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="-3.366" y1="0.366" x2="-3.232" y2="0.134" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-1.866" x2="-0.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="-1.866" x2="1.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-1.866" x2="2.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="3.229" y1="-0.132" x2="2.634" y2="0.211" width="0.1524" layer="21"/>
<wire x1="2.634" y1="0.211" x2="2.366" y2="0.211" width="0.1524" layer="21"/>
<wire x1="2.366" y1="0.211" x2="1.634" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="1.634" y1="-0.211" x2="1.5" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-0.211" x2="1.366" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="1.366" y1="-0.211" x2="0.634" y2="0.211" width="0.1524" layer="21"/>
<wire x1="0.634" y1="0.211" x2="0.5" y2="0.211" width="0.1524" layer="21"/>
<wire x1="0.5" y1="0.211" x2="0.366" y2="0.211" width="0.1524" layer="21"/>
<wire x1="0.366" y1="0.211" x2="-0.366" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="-0.366" y1="-0.211" x2="-0.5" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="-0.211" x2="-0.634" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="-0.634" y1="-0.211" x2="-1.366" y2="0.211" width="0.1524" layer="21"/>
<wire x1="-1.366" y1="0.211" x2="-1.5" y2="0.211" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="0.211" x2="-1.634" y2="0.211" width="0.1524" layer="21"/>
<wire x1="-1.634" y1="0.211" x2="-2.366" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="-2.366" y1="-0.211" x2="-2.634" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="-2.634" y1="-0.211" x2="-3.232" y2="0.134" width="0.1524" layer="21"/>
<wire x1="0.5" y1="0.211" x2="0.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="0.211" x2="-1.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-0.211" x2="1.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="-0.211" x2="-0.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="1.5" y1="1.866" x2="2.395" y2="1.311" width="0.1524" layer="21" curve="-64.003574"/>
<wire x1="2.395" y1="1.311" x2="3.229" y2="-0.132" width="0.1524" layer="21"/>
<wire x1="3.395" y1="-0.421" x2="2.5" y2="-1.866" width="0.1524" layer="21" curve="-115.996362"/>
<wire x1="3.229" y1="-0.132" x2="3.395" y2="-0.421" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-1.866" x2="-2.366" y2="-1.366" width="0.1524" layer="21" curve="-60.005161"/>
<wire x1="-2.366" y1="-1.366" x2="-3.366" y2="0.366" width="0.1524" layer="21"/>
<wire x1="-2.5" y1="1.866" x2="-3.366" y2="0.366" width="0.1524" layer="21" curve="120.001456"/>
<pad name="MCLR" x="-2.5" y="0.866" drill="0.7" diameter="1.7" rot="R180"/>
<pad name="VCC" x="1.5" y="0.866" drill="0.7" diameter="1.7" rot="R180"/>
<pad name="GND" x="-0.5" y="0.866" drill="0.7" diameter="1.7" rot="R180"/>
<pad name="PGC" x="0.5" y="-0.866" drill="0.7" diameter="1.7" rot="R180"/>
<pad name="PGD" x="-1.5" y="-0.866" drill="0.7" diameter="1.7" rot="R180"/>
<pad name="NC" x="2.5" y="-0.866" drill="0.7" diameter="1.7" rot="R180"/>
<text x="-3.81" y="2.2225" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<polygon width="0.1524" layer="41">
<vertex x="2.333" y="1.419" curve="64.003039"/>
<vertex x="1.5" y="1.866"/>
<vertex x="-2.5" y="1.866" curve="120.001456"/>
<vertex x="-3.366" y="0.366"/>
<vertex x="-2.366" y="-1.366" curve="60.005161"/>
<vertex x="-1.5" y="-1.866"/>
<vertex x="2.5" y="-1.866" curve="115.996362"/>
<vertex x="3.395" y="-0.421"/>
</polygon>
<polygon width="0.1524" layer="42">
<vertex x="2.333" y="1.419" curve="64.003039"/>
<vertex x="1.5" y="1.866"/>
<vertex x="-2.5" y="1.866" curve="120.001456"/>
<vertex x="-3.366" y="0.366"/>
<vertex x="-2.366" y="-1.366" curve="60.005161"/>
<vertex x="-1.5" y="-1.866"/>
<vertex x="2.5" y="-1.866" curve="115.996362"/>
<vertex x="3.395" y="-0.421"/>
</polygon>
</package>
<package name="CON_POGO_ICSP_TARGET">
<description>&lt;B&gt;Pogo pin ICSP target side footprint &lt;/B&gt;
&lt;BR/&gt; pogo pin pad diameter 1.00mm
&lt;BR/&gt; pinout from top left: VCC, GND, MCLR
&lt;BR/&gt; pinout from bottom left:  PGC, PGD</description>
<wire x1="-1.5" y1="1.866" x2="-0.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="1.866" x2="1.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="1.5" y1="1.866" x2="2.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="3.366" y1="0.366" x2="3.232" y2="0.134" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-1.866" x2="0.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="0.5" y1="-1.866" x2="-1.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-1.866" x2="-2.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="-3.229" y1="-0.132" x2="-2.634" y2="0.211" width="0.1524" layer="21"/>
<wire x1="-2.634" y1="0.211" x2="-2.366" y2="0.211" width="0.1524" layer="21"/>
<wire x1="-2.366" y1="0.211" x2="-1.634" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="-1.634" y1="-0.211" x2="-1.5" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-0.211" x2="-1.366" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="-1.366" y1="-0.211" x2="-0.634" y2="0.211" width="0.1524" layer="21"/>
<wire x1="-0.634" y1="0.211" x2="-0.5" y2="0.211" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="0.211" x2="-0.366" y2="0.211" width="0.1524" layer="21"/>
<wire x1="-0.366" y1="0.211" x2="0.366" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="0.366" y1="-0.211" x2="0.5" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="0.5" y1="-0.211" x2="0.634" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="0.634" y1="-0.211" x2="1.366" y2="0.211" width="0.1524" layer="21"/>
<wire x1="1.366" y1="0.211" x2="1.5" y2="0.211" width="0.1524" layer="21"/>
<wire x1="1.5" y1="0.211" x2="1.634" y2="0.211" width="0.1524" layer="21"/>
<wire x1="1.634" y1="0.211" x2="2.366" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="2.366" y1="-0.211" x2="2.634" y2="-0.211" width="0.1524" layer="21"/>
<wire x1="2.634" y1="-0.211" x2="3.232" y2="0.134" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="0.211" x2="-0.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="1.5" y1="0.211" x2="1.5" y2="1.866" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-0.211" x2="-1.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="0.5" y1="-0.211" x2="0.5" y2="-1.866" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="1.866" x2="-2.395" y2="1.311" width="0.1524" layer="21" curve="64.003574"/>
<wire x1="-2.395" y1="1.311" x2="-3.229" y2="-0.132" width="0.1524" layer="21"/>
<wire x1="-3.395" y1="-0.421" x2="-2.5" y2="-1.866" width="0.1524" layer="21" curve="115.996362"/>
<wire x1="-3.229" y1="-0.132" x2="-3.395" y2="-0.421" width="0.1524" layer="21"/>
<wire x1="1.5" y1="-1.866" x2="2.366" y2="-1.366" width="0.1524" layer="21" curve="59.998544"/>
<wire x1="2.366" y1="-1.366" x2="3.366" y2="0.366" width="0.1524" layer="21"/>
<wire x1="2.5" y1="1.866" x2="3.366" y2="0.366" width="0.1524" layer="21" curve="-120.001456"/>
<pad name="MCLR" x="2.5" y="0.866" drill="0.6"/>
<smd name="PGC" x="-0.5" y="-0.866" dx="1" dy="1" layer="1" roundness="100"/>
<smd name="PGD" x="1.5" y="-0.866" dx="1" dy="1" layer="1" roundness="100"/>
<smd name="GND" x="0.5" y="0.866" dx="1" dy="1" layer="1" roundness="100"/>
<smd name="VCC" x="-1.5" y="0.866" dx="1" dy="1" layer="1" roundness="100"/>
<text x="-3.81" y="2.2225" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<hole x="-2.5" y="-0.866" drill="0.99"/>
<polygon width="0.1524" layer="41">
<vertex x="-2.333" y="1.419" curve="-64.003039"/>
<vertex x="-1.5" y="1.866"/>
<vertex x="2.5" y="1.866" curve="-120.001456"/>
<vertex x="3.366" y="0.366"/>
<vertex x="2.366" y="-1.366" curve="-59.998544"/>
<vertex x="1.5" y="-1.866"/>
<vertex x="-2.5" y="-1.866" curve="-115.996362"/>
<vertex x="-3.395" y="-0.421"/>
</polygon>
</package>
<package name="M1X2">
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="-2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8" shape="square" first="yes"/>
<pad name="2" x="1.27" y="0" drill="0.8"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="M1X2_MALE_RIGHT_ANGLED">
<description>1x02 Right angled male header PTH
&lt;br&gt; Mfr no: 22-28-8020
&lt;br&gt; &lt;a href="http://www.digikey.com/product-search/en?x=0&amp;y=0&amp;lang=en&amp;site=us&amp;KeyWords=22-28-8020+"&gt; 22-28-8020&lt;/a&gt;</description>
<wire x1="-2.24" y1="1.145" x2="-0.3" y2="1.145" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="1.145" x2="0" y2="0.845" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.845" x2="-0.3" y2="-1.145" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-1.145" x2="-2.24" y2="-1.145" width="0.1524" layer="21"/>
<wire x1="-2.24" y1="-1.145" x2="-2.54" y2="-0.845" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.845" x2="-2.54" y2="0.845" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.845" x2="-2.24" y2="1.145" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.845" x2="0" y2="0.845" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.845" x2="0" y2="-0.845" width="0.1524" layer="21"/>
<wire x1="-2.24" y1="1.145" x2="-2.24" y2="-1.145" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="1.145" x2="-0.3" y2="-1.145" width="0.1524" layer="21"/>
<wire x1="-1.6" y1="-2.595" x2="-0.94" y2="-2.595" width="0.1524" layer="51"/>
<wire x1="-0.94" y1="-2.595" x2="-0.94" y2="-3.255" width="0.1524" layer="51"/>
<wire x1="-0.94" y1="-3.255" x2="-1.6" y2="-3.255" width="0.1524" layer="51"/>
<wire x1="-1.6" y1="-3.255" x2="-1.6" y2="-2.595" width="0.1524" layer="51"/>
<wire x1="0.3" y1="1.145" x2="2.24" y2="1.145" width="0.1524" layer="21"/>
<wire x1="2.24" y1="1.145" x2="2.54" y2="0.845" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.845" x2="2.24" y2="-1.145" width="0.1524" layer="21"/>
<wire x1="2.24" y1="-1.145" x2="0.3" y2="-1.145" width="0.1524" layer="21"/>
<wire x1="0.3" y1="-1.145" x2="0" y2="-0.845" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.845" x2="0" y2="0.845" width="0.1524" layer="21"/>
<wire x1="0" y1="0.845" x2="0.3" y2="1.145" width="0.1524" layer="21"/>
<wire x1="0" y1="0.845" x2="2.54" y2="0.845" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.845" x2="2.54" y2="-0.845" width="0.1524" layer="21"/>
<wire x1="0.3" y1="1.145" x2="0.3" y2="-1.145" width="0.1524" layer="21"/>
<wire x1="2.24" y1="1.145" x2="2.24" y2="-1.145" width="0.1524" layer="21"/>
<wire x1="0.94" y1="-2.595" x2="1.6" y2="-2.595" width="0.1524" layer="51"/>
<wire x1="1.6" y1="-2.595" x2="1.6" y2="-3.255" width="0.1524" layer="51"/>
<wire x1="1.6" y1="-3.255" x2="0.94" y2="-3.255" width="0.1524" layer="51"/>
<wire x1="0.94" y1="-3.255" x2="0.94" y2="-2.595" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-0.845" x2="2.54" y2="0.845" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="-2.925" drill="1.02" diameter="1.632" shape="square" first="yes"/>
<pad name="2" x="1.27" y="-2.925" drill="1.02" diameter="1.632"/>
<text x="-1.92" y="8" size="1.27" layer="21" ratio="10">&gt;NAME</text>
<rectangle x1="-1.6" y1="-2.008" x2="-0.94" y2="-1.145" layer="21"/>
<rectangle x1="-1.6" y1="-2.595" x2="-0.94" y2="-2.008" layer="51"/>
<rectangle x1="0.94" y1="-2.008" x2="1.6" y2="-1.145" layer="21"/>
<rectangle x1="0.94" y1="-2.595" x2="1.6" y2="-2.008" layer="51"/>
<polygon width="0.1524" layer="21">
<vertex x="-1.6" y="1.145"/>
<vertex x="-1.6" y="6.585"/>
<vertex x="-1.38" y="7.345"/>
<vertex x="-1.16" y="7.345"/>
<vertex x="-0.94" y="6.585"/>
<vertex x="-0.94" y="1.145"/>
</polygon>
<polygon width="0.1524" layer="21">
<vertex x="0.94" y="1.145"/>
<vertex x="0.94" y="6.585"/>
<vertex x="1.16" y="7.345"/>
<vertex x="1.38" y="7.345"/>
<vertex x="1.6" y="6.585"/>
<vertex x="1.6" y="1.145"/>
</polygon>
</package>
<package name="R805">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
</package>
<package name="R603">
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
</package>
<package name="R402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="R1206">
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="RTH025W">
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.8575" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="TO-220-2">
<wire x1="4.826" y1="-1.778" x2="5.08" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.397" x2="5.08" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.524" x2="-5.08" y2="1.397" width="0.1524" layer="21"/>
<circle x="-4.6228" y="-1.1684" radius="0.254" width="0" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-3.3782" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="-1.27" y="-1.27" size="1.27" layer="51" font="vector" ratio="10">1</text>
<text x="3.81" y="-1.27" size="1.27" layer="51" font="vector" ratio="10">2</text>
<rectangle x1="-5.334" y1="1.27" x2="-3.429" y2="2.54" layer="21"/>
<rectangle x1="-3.429" y1="1.778" x2="-1.651" y2="2.54" layer="21"/>
<rectangle x1="-1.651" y1="1.27" x2="-0.889" y2="2.54" layer="21"/>
<rectangle x1="-0.889" y1="1.778" x2="0.889" y2="2.54" layer="21"/>
<rectangle x1="0.889" y1="1.27" x2="1.651" y2="2.54" layer="21"/>
<rectangle x1="1.651" y1="1.778" x2="3.429" y2="2.54" layer="21"/>
<rectangle x1="3.429" y1="1.27" x2="5.334" y2="2.54" layer="21"/>
<rectangle x1="-3.429" y1="1.27" x2="-1.651" y2="1.778" layer="51"/>
<rectangle x1="-0.889" y1="1.27" x2="0.889" y2="1.778" layer="21"/>
<rectangle x1="1.651" y1="1.27" x2="3.429" y2="1.778" layer="51"/>
</package>
<package name="C805">
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.127" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.016" layer="25" ratio="12">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.016" layer="27" ratio="12">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
</package>
<package name="C402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="C603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8302" y2="0.4801" layer="51" rot="R180"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
</package>
<package name="C1206">
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="C0.1PTH">
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.8575" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PRG_ICSP_PIC">
<wire x1="-4.064" y1="-7.62" x2="-4.064" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-4.064" y1="7.62" x2="6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="6.35" y1="7.62" x2="6.35" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-7.62" x2="-4.064" y2="-7.62" width="0.4064" layer="94"/>
<text x="-4.1275" y="8.255" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="0.508" y="4.318" size="1.27" layer="95" font="vector" ratio="12">!MCLR</text>
<text x="0.508" y="2.032" size="1.27" layer="95" font="vector" ratio="12">+V</text>
<text x="0.508" y="-0.508" size="1.27" layer="95" font="vector" ratio="12">GND</text>
<text x="0.508" y="-3.048" size="1.27" layer="95" font="vector" ratio="12">PGD</text>
<text x="0.508" y="-5.588" size="1.27" layer="95" font="vector" ratio="12">PGC</text>
<pin name="+V" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="/MCLR" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="GND" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="PGC" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="PGD" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="HEADER1X2">
<wire x1="-1.27" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="2.54" width="0.4064" layer="94"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="0" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="0" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CON_HEADER_PRG_PIC_ICSP" prefix="J">
<description>5-pin PIC In Circuit Serial Programming header</description>
<gates>
<gate name="J" symbol="PRG_ICSP_PIC" x="-2.54" y="0"/>
</gates>
<devices>
<device name="0.1INCH_A" package="HEADER_PRG_1X05">
<connects>
<connect gate="J" pin="+V" pad="2"/>
<connect gate="J" pin="/MCLR" pad="1"/>
<connect gate="J" pin="GND" pad="3"/>
<connect gate="J" pin="PGC" pad="5"/>
<connect gate="J" pin="PGD" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.1INCH_B" package="HEADER_PRG_1X05_ICSP_B">
<connects>
<connect gate="J" pin="+V" pad="2"/>
<connect gate="J" pin="/MCLR" pad="1"/>
<connect gate="J" pin="GND" pad="3"/>
<connect gate="J" pin="PGC" pad="5"/>
<connect gate="J" pin="PGD" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-POGO-PROG" package="CON_POGO_ICSP_PROG">
<connects>
<connect gate="J" pin="+V" pad="VCC"/>
<connect gate="J" pin="/MCLR" pad="MCLR"/>
<connect gate="J" pin="GND" pad="GND"/>
<connect gate="J" pin="PGC" pad="PGC"/>
<connect gate="J" pin="PGD" pad="PGD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-POGO-FOOTPRINT" package="CON_POGO_ICSP_TARGET">
<connects>
<connect gate="J" pin="+V" pad="VCC"/>
<connect gate="J" pin="/MCLR" pad="MCLR"/>
<connect gate="J" pin="GND" pad="GND"/>
<connect gate="J" pin="PGC" pad="PGC"/>
<connect gate="J" pin="PGD" pad="PGD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON_HEADER_1X02" prefix="J">
<gates>
<gate name="J" symbol="HEADER1X2" x="0" y="0"/>
</gates>
<devices>
<device name="-PTH" package="M1X2">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PTH_RIGHT_ANGLED" package="M1X2_MALE_RIGHT_ANGLED">
<connects>
<connect gate="J" pin="1" pad="1"/>
<connect gate="J" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0805" package="R805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="R805"/>
</technologies>
</device>
<device name="-0603" package="R603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="R402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PTH-0.4" package="RTH025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TO-220-2" package="TO-220-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR_NPOL" prefix="C" uservalue="yes">
<description>Non-Polarized capacitor in various packages</description>
<gates>
<gate name="C" symbol="CAP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="-0805" package="C805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402" package="C402">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="C603">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206" package="C1206">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PTH_0.1&quot;" package="C0.1PTH">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HD44780LCD">
<packages>
<package name="LCD-16X2-4HOLES">
<wire x1="-40" y1="18" x2="40" y2="18" width="0.2032" layer="21"/>
<wire x1="40" y1="18" x2="40" y2="-18" width="0.2032" layer="21"/>
<wire x1="40" y1="-18" x2="-40" y2="-18" width="0.2032" layer="21"/>
<wire x1="-40" y1="-18" x2="-40" y2="18" width="0.2032" layer="21"/>
<wire x1="-35.65" y1="13.15" x2="35.65" y2="13.15" width="0.2032" layer="21"/>
<wire x1="35.65" y1="13.15" x2="35.65" y2="-13.15" width="0.2032" layer="21"/>
<wire x1="35.65" y1="-13.15" x2="-35.65" y2="-13.15" width="0.2032" layer="21"/>
<wire x1="-35.65" y1="-13.15" x2="-35.65" y2="13.15" width="0.2032" layer="21"/>
<wire x1="-32.25" y1="8.2" x2="32.25" y2="8.2" width="0.2032" layer="47"/>
<wire x1="32.25" y1="8.2" x2="32.25" y2="-8.2" width="0.2032" layer="47"/>
<wire x1="32.25" y1="-8.2" x2="-32.25" y2="-8.2" width="0.2032" layer="47"/>
<wire x1="-32.25" y1="-8.2" x2="-32.25" y2="8.2" width="0.2032" layer="47"/>
<wire x1="-32" y1="8" x2="32" y2="8" width="0.127" layer="51"/>
<wire x1="32" y1="8" x2="32" y2="-8" width="0.127" layer="51"/>
<wire x1="32" y1="-8" x2="-32" y2="-8" width="0.127" layer="51"/>
<wire x1="-32" y1="-8" x2="-32" y2="8" width="0.127" layer="51"/>
<pad name="1" x="-32" y="15.5" drill="1.016" shape="square"/>
<pad name="2" x="-29.46" y="15.5" drill="1.016"/>
<pad name="3" x="-26.92" y="15.5" drill="1.016"/>
<pad name="4" x="-24.38" y="15.5" drill="1.016"/>
<pad name="5" x="-21.84" y="15.5" drill="1.016"/>
<pad name="6" x="-19.3" y="15.5" drill="1.016"/>
<pad name="7" x="-16.76" y="15.5" drill="1.016"/>
<pad name="8" x="-14.22" y="15.5" drill="1.016"/>
<pad name="9" x="-11.68" y="15.5" drill="1.016"/>
<pad name="10" x="-9.14" y="15.5" drill="1.016"/>
<pad name="11" x="-6.6" y="15.5" drill="1.016"/>
<pad name="12" x="-4.06" y="15.5" drill="1.016"/>
<pad name="13" x="-1.52" y="15.5" drill="1.016"/>
<pad name="14" x="1.02" y="15.5" drill="1.016"/>
<pad name="15" x="3.56" y="15.5" drill="1.016"/>
<pad name="16" x="6.1" y="15.5" drill="1.016"/>
<text x="-34.29" y="15.24" size="1.016" layer="51" ratio="15">1</text>
<rectangle x1="-32" y1="-14" x2="-27" y2="-11" layer="40"/>
<rectangle x1="-32" y1="11" x2="-27" y2="14" layer="40"/>
<rectangle x1="27" y1="11" x2="32" y2="14" layer="40"/>
<rectangle x1="27" y1="-14" x2="32" y2="-11" layer="40"/>
<rectangle x1="-2" y1="-14" x2="3" y2="-11" layer="40"/>
<rectangle x1="-3" y1="11" x2="2" y2="14" layer="40"/>
<hole x="-37.6" y="15.5" drill="3.2"/>
<hole x="-37.6" y="-15.5" drill="3.2"/>
<hole x="37.6" y="-15.5" drill="3.2"/>
<hole x="37.6" y="15.5" drill="3.2"/>
</package>
<package name="LCD-16X2-3HOLES">
<wire x1="-40" y1="18" x2="40" y2="18" width="0.2032" layer="21"/>
<wire x1="40" y1="18" x2="40" y2="-18" width="0.2032" layer="21"/>
<wire x1="40" y1="-18" x2="-40" y2="-18" width="0.2032" layer="21"/>
<wire x1="-40" y1="-18" x2="-40" y2="18" width="0.2032" layer="21"/>
<wire x1="-35.65" y1="13.15" x2="35.65" y2="13.15" width="0.2032" layer="21"/>
<wire x1="35.65" y1="13.15" x2="35.65" y2="-13.15" width="0.2032" layer="21"/>
<wire x1="35.65" y1="-13.15" x2="-35.65" y2="-13.15" width="0.2032" layer="21"/>
<wire x1="-35.65" y1="-13.15" x2="-35.65" y2="13.15" width="0.2032" layer="21"/>
<wire x1="-32.25" y1="8.2" x2="32.25" y2="8.2" width="0.2032" layer="47"/>
<wire x1="32.25" y1="8.2" x2="32.25" y2="-8.2" width="0.2032" layer="47"/>
<wire x1="32.25" y1="-8.2" x2="-32.25" y2="-8.2" width="0.2032" layer="47"/>
<wire x1="-32.25" y1="-8.2" x2="-32.25" y2="8.2" width="0.2032" layer="47"/>
<wire x1="-32" y1="8" x2="32" y2="8" width="0.127" layer="51"/>
<wire x1="32" y1="8" x2="32" y2="-8" width="0.127" layer="51"/>
<wire x1="32" y1="-8" x2="-32" y2="-8" width="0.127" layer="51"/>
<wire x1="-32" y1="-8" x2="-32" y2="8" width="0.127" layer="51"/>
<pad name="1" x="-32" y="15.5" drill="1.016" shape="square"/>
<pad name="2" x="-29.46" y="15.5" drill="1.016"/>
<pad name="3" x="-26.92" y="15.5" drill="1.016"/>
<pad name="4" x="-24.38" y="15.5" drill="1.016"/>
<pad name="5" x="-21.84" y="15.5" drill="1.016"/>
<pad name="6" x="-19.3" y="15.5" drill="1.016"/>
<pad name="7" x="-16.76" y="15.5" drill="1.016"/>
<pad name="8" x="-14.22" y="15.5" drill="1.016"/>
<pad name="9" x="-11.68" y="15.5" drill="1.016"/>
<pad name="10" x="-9.14" y="15.5" drill="1.016"/>
<pad name="11" x="-6.6" y="15.5" drill="1.016"/>
<pad name="12" x="-4.06" y="15.5" drill="1.016"/>
<pad name="13" x="-1.52" y="15.5" drill="1.016"/>
<pad name="14" x="1.02" y="15.5" drill="1.016"/>
<pad name="15" x="3.56" y="15.5" drill="1.016"/>
<pad name="16" x="6.1" y="15.5" drill="1.016"/>
<text x="-34.29" y="15.24" size="1.016" layer="51" ratio="15">1</text>
<rectangle x1="-32" y1="11" x2="-27" y2="14" layer="40"/>
<rectangle x1="27" y1="11" x2="32" y2="14" layer="40"/>
<rectangle x1="-3" y1="11" x2="2" y2="14" layer="40"/>
<rectangle x1="-32" y1="-14" x2="-27" y2="-11" layer="40"/>
<rectangle x1="27" y1="-14" x2="32" y2="-11" layer="40"/>
<rectangle x1="-2" y1="-14" x2="3" y2="-11" layer="40"/>
<hole x="-37.5" y="15.5" drill="3.2"/>
<hole x="-37.5" y="-15.5" drill="3.2"/>
<hole x="37.5" y="15.5" drill="3.2"/>
</package>
<package name="LCD-20X4-4HOLES">
<wire x1="0" y1="0" x2="0" y2="60" width="0.127" layer="21"/>
<wire x1="0" y1="60" x2="98" y2="60" width="0.127" layer="21"/>
<wire x1="98" y1="60" x2="98" y2="0" width="0.127" layer="21"/>
<wire x1="98" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="1.8" y1="11.3" x2="1.8" y2="48.7" width="0.127" layer="21"/>
<wire x1="1.8" y1="48.7" x2="96.2" y2="48.7" width="0.127" layer="21"/>
<wire x1="96.2" y1="48.7" x2="96.2" y2="11.3" width="0.127" layer="21"/>
<wire x1="96.2" y1="11.3" x2="1.8" y2="11.3" width="0.127" layer="21"/>
<pad name="P$1" x="10" y="57.5" drill="1.016" shape="square"/>
<pad name="P$2" x="12.54" y="57.5" drill="1.016"/>
<pad name="P$3" x="15.08" y="57.5" drill="1.016"/>
<pad name="P$4" x="17.62" y="57.5" drill="1.016"/>
<pad name="P$5" x="20.16" y="57.5" drill="1.016"/>
<pad name="P$6" x="22.7" y="57.5" drill="1.016"/>
<pad name="P$7" x="25.24" y="57.5" drill="1.016"/>
<pad name="P$8" x="27.78" y="57.5" drill="1.016"/>
<pad name="P$9" x="30.32" y="57.5" drill="1.016"/>
<pad name="P$10" x="32.86" y="57.5" drill="1.016"/>
<pad name="P$11" x="35.4" y="57.5" drill="1.016"/>
<pad name="P$12" x="37.94" y="57.5" drill="1.016"/>
<pad name="P$13" x="40.48" y="57.5" drill="1.016"/>
<pad name="P$14" x="43.02" y="57.5" drill="1.016"/>
<pad name="P$15" x="45.56" y="57.5" drill="1.016"/>
<pad name="P$16" x="48.1" y="57.5" drill="1.016"/>
<rectangle x1="3" y1="11" x2="6" y2="14" layer="40"/>
<rectangle x1="3" y1="46" x2="6" y2="49" layer="40"/>
<rectangle x1="92" y1="46" x2="95" y2="49" layer="40"/>
<rectangle x1="92" y1="11" x2="95" y2="14" layer="40"/>
<rectangle x1="31" y1="11" x2="34" y2="14" layer="40"/>
<rectangle x1="64" y1="11" x2="67" y2="14" layer="40"/>
<rectangle x1="31" y1="46" x2="34" y2="49" layer="40"/>
<rectangle x1="64" y1="46" x2="67" y2="49" layer="40"/>
<hole x="2.5" y="2.5" drill="3.2"/>
<hole x="2.5" y="57.5" drill="3.2"/>
<hole x="95.5" y="57.5" drill="3.2"/>
<hole x="95.5" y="2.5" drill="3.2"/>
</package>
<package name="LCD-20X4-3HOLES">
<wire x1="0" y1="0" x2="0" y2="60" width="0.127" layer="21"/>
<wire x1="0" y1="60" x2="98" y2="60" width="0.127" layer="21"/>
<wire x1="98" y1="60" x2="98" y2="0" width="0.127" layer="21"/>
<wire x1="98" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="1.8" y1="11.3" x2="1.8" y2="48.7" width="0.127" layer="21"/>
<wire x1="1.8" y1="48.7" x2="96.2" y2="48.7" width="0.127" layer="21"/>
<wire x1="96.2" y1="48.7" x2="96.2" y2="11.3" width="0.127" layer="21"/>
<wire x1="96.2" y1="11.3" x2="1.8" y2="11.3" width="0.127" layer="21"/>
<pad name="P$1" x="10" y="57.5" drill="1.016" shape="square"/>
<pad name="P$2" x="12.54" y="57.5" drill="1.016"/>
<pad name="P$3" x="15.08" y="57.5" drill="1.016"/>
<pad name="P$4" x="17.62" y="57.5" drill="1.016"/>
<pad name="P$5" x="20.16" y="57.5" drill="1.016"/>
<pad name="P$6" x="22.7" y="57.5" drill="1.016"/>
<pad name="P$7" x="25.24" y="57.5" drill="1.016"/>
<pad name="P$8" x="27.78" y="57.5" drill="1.016"/>
<pad name="P$9" x="30.32" y="57.5" drill="1.016"/>
<pad name="P$10" x="32.86" y="57.5" drill="1.016"/>
<pad name="P$11" x="35.4" y="57.5" drill="1.016"/>
<pad name="P$12" x="37.94" y="57.5" drill="1.016"/>
<pad name="P$13" x="40.48" y="57.5" drill="1.016"/>
<pad name="P$14" x="43.02" y="57.5" drill="1.016"/>
<pad name="P$15" x="45.56" y="57.5" drill="1.016"/>
<pad name="P$16" x="48.1" y="57.5" drill="1.016"/>
<rectangle x1="3" y1="11" x2="6" y2="14" layer="40"/>
<rectangle x1="3" y1="46" x2="6" y2="49" layer="40"/>
<rectangle x1="31" y1="46" x2="34" y2="49" layer="40"/>
<rectangle x1="64" y1="46" x2="67" y2="49" layer="40"/>
<rectangle x1="92" y1="46" x2="95" y2="49" layer="40"/>
<rectangle x1="31" y1="11" x2="34" y2="14" layer="40"/>
<rectangle x1="64" y1="11" x2="67" y2="14" layer="40"/>
<rectangle x1="92" y1="11" x2="95" y2="14" layer="40"/>
<hole x="2.5" y="2.5" drill="3.2"/>
<hole x="2.5" y="57.5" drill="3.2"/>
<hole x="95.5" y="57.5" drill="3.2"/>
</package>
<package name="LCD-16X2-4HOLES-BL">
<wire x1="-40" y1="18" x2="40" y2="18" width="0.2032" layer="21"/>
<wire x1="40" y1="18" x2="40" y2="-18" width="0.2032" layer="21"/>
<wire x1="40" y1="-18" x2="-40" y2="-18" width="0.2032" layer="21"/>
<wire x1="-40" y1="-18" x2="-40" y2="18" width="0.2032" layer="21"/>
<wire x1="-35.65" y1="13.15" x2="35.65" y2="13.15" width="0.2032" layer="21"/>
<wire x1="35.65" y1="13.15" x2="35.65" y2="-13.15" width="0.2032" layer="21"/>
<wire x1="35.65" y1="-13.15" x2="-35.65" y2="-13.15" width="0.2032" layer="21"/>
<wire x1="-35.65" y1="-13.15" x2="-35.65" y2="13.15" width="0.2032" layer="21"/>
<wire x1="-32.25" y1="8.2" x2="32.25" y2="8.2" width="0.2032" layer="47"/>
<wire x1="32.25" y1="8.2" x2="32.25" y2="-8.2" width="0.2032" layer="47"/>
<wire x1="32.25" y1="-8.2" x2="-32.25" y2="-8.2" width="0.2032" layer="47"/>
<wire x1="-32.25" y1="-8.2" x2="-32.25" y2="8.2" width="0.2032" layer="47"/>
<pad name="1" x="-32" y="-15.5" drill="1.016" shape="square"/>
<pad name="2" x="-29.46" y="-15.5" drill="1.016"/>
<pad name="3" x="-26.92" y="-15.5" drill="1.016"/>
<pad name="4" x="-24.38" y="-15.5" drill="1.016"/>
<pad name="5" x="-21.84" y="-15.5" drill="1.016"/>
<pad name="6" x="-19.3" y="-15.5" drill="1.016"/>
<pad name="7" x="-16.76" y="-15.5" drill="1.016"/>
<pad name="8" x="-14.22" y="-15.5" drill="1.016"/>
<pad name="9" x="-11.68" y="-15.5" drill="1.016"/>
<pad name="10" x="-9.14" y="-15.5" drill="1.016"/>
<pad name="11" x="-6.6" y="-15.5" drill="1.016"/>
<pad name="12" x="-4.06" y="-15.5" drill="1.016"/>
<pad name="13" x="-1.52" y="-15.5" drill="1.016"/>
<pad name="14" x="1.02" y="-15.5" drill="1.016"/>
<pad name="15" x="3.56" y="-15.5" drill="1.016"/>
<pad name="16" x="6.1" y="-15.5" drill="1.016"/>
<text x="-32.385" y="-17.78" size="1.016" layer="51" ratio="15">1</text>
<rectangle x1="-32" y1="-14" x2="-27" y2="-11" layer="40"/>
<rectangle x1="-32" y1="11" x2="-27" y2="14" layer="40"/>
<rectangle x1="27" y1="11" x2="32" y2="14" layer="40"/>
<rectangle x1="27" y1="-14" x2="32" y2="-11" layer="40"/>
<rectangle x1="-2" y1="-14" x2="3" y2="-11" layer="40"/>
<rectangle x1="-3" y1="11" x2="2" y2="14" layer="40"/>
<hole x="-37.6" y="15.5" drill="3.2"/>
<hole x="-37.6" y="-15.5" drill="3.2"/>
<hole x="37.6" y="-15.5" drill="3.2"/>
<hole x="37.6" y="15.5" drill="3.2"/>
</package>
</packages>
<symbols>
<symbol name="LCD-HD44780">
<wire x1="-7.62" y1="22.86" x2="-7.62" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-20.32" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="5.08" y2="22.86" width="0.254" layer="94"/>
<wire x1="5.08" y1="22.86" x2="-7.62" y2="22.86" width="0.254" layer="94"/>
<text x="-7.62" y="23.368" size="1.778" layer="95">&gt;Name</text>
<text x="-7.62" y="-22.86" size="1.778" layer="96">&gt;Value</text>
<text x="3.048" y="-9.398" size="2.286" layer="94" ratio="20" rot="R90">HD44780 LCD</text>
<pin name="A" x="-10.16" y="-15.24" visible="pin" length="short" direction="pwr"/>
<pin name="DB0" x="-10.16" y="5.08" visible="pin" length="short"/>
<pin name="DB1" x="-10.16" y="2.54" visible="pin" length="short"/>
<pin name="DB2" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="DB3" x="-10.16" y="-2.54" visible="pin" length="short"/>
<pin name="DB4" x="-10.16" y="-5.08" visible="pin" length="short"/>
<pin name="DB5" x="-10.16" y="-7.62" visible="pin" length="short"/>
<pin name="DB6" x="-10.16" y="-10.16" visible="pin" length="short"/>
<pin name="DB7" x="-10.16" y="-12.7" visible="pin" length="short"/>
<pin name="E" x="-10.16" y="7.62" visible="pin" length="short" direction="in"/>
<pin name="K" x="-10.16" y="-17.78" visible="pin" length="short" direction="pwr"/>
<pin name="R/W" x="-10.16" y="10.16" visible="pin" length="short" direction="in"/>
<pin name="RS" x="-10.16" y="12.7" visible="pin" length="short" direction="in"/>
<pin name="VDD" x="-10.16" y="17.78" visible="pin" length="short" direction="pwr"/>
<pin name="VO" x="-10.16" y="15.24" visible="pin" length="short"/>
<pin name="VSS" x="-10.16" y="20.32" visible="pin" length="short" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LCD-HD44780">
<gates>
<gate name="G$1" symbol="LCD-HD44780" x="0" y="0"/>
</gates>
<devices>
<device name="16X2-4-HOLES" package="LCD-16X2-4HOLES">
<connects>
<connect gate="G$1" pin="A" pad="15"/>
<connect gate="G$1" pin="DB0" pad="7"/>
<connect gate="G$1" pin="DB1" pad="8"/>
<connect gate="G$1" pin="DB2" pad="9"/>
<connect gate="G$1" pin="DB3" pad="10"/>
<connect gate="G$1" pin="DB4" pad="11"/>
<connect gate="G$1" pin="DB5" pad="12"/>
<connect gate="G$1" pin="DB6" pad="13"/>
<connect gate="G$1" pin="DB7" pad="14"/>
<connect gate="G$1" pin="E" pad="6"/>
<connect gate="G$1" pin="K" pad="16"/>
<connect gate="G$1" pin="R/W" pad="5"/>
<connect gate="G$1" pin="RS" pad="4"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VO" pad="3"/>
<connect gate="G$1" pin="VSS" pad="1"/>
</connects>
<technologies>
<technology name="4-HOLES"/>
</technologies>
</device>
<device name="16X2-3-HOLES" package="LCD-16X2-3HOLES">
<connects>
<connect gate="G$1" pin="A" pad="15"/>
<connect gate="G$1" pin="DB0" pad="7"/>
<connect gate="G$1" pin="DB1" pad="8"/>
<connect gate="G$1" pin="DB2" pad="9"/>
<connect gate="G$1" pin="DB3" pad="10"/>
<connect gate="G$1" pin="DB4" pad="11"/>
<connect gate="G$1" pin="DB5" pad="12"/>
<connect gate="G$1" pin="DB6" pad="13"/>
<connect gate="G$1" pin="DB7" pad="14"/>
<connect gate="G$1" pin="E" pad="6"/>
<connect gate="G$1" pin="K" pad="16"/>
<connect gate="G$1" pin="R/W" pad="5"/>
<connect gate="G$1" pin="RS" pad="4"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VO" pad="3"/>
<connect gate="G$1" pin="VSS" pad="1"/>
</connects>
<technologies>
<technology name="3-HOLES"/>
</technologies>
</device>
<device name="20X4-4HOLES" package="LCD-20X4-4HOLES">
<connects>
<connect gate="G$1" pin="A" pad="P$15"/>
<connect gate="G$1" pin="DB0" pad="P$7"/>
<connect gate="G$1" pin="DB1" pad="P$8"/>
<connect gate="G$1" pin="DB2" pad="P$9"/>
<connect gate="G$1" pin="DB3" pad="P$10"/>
<connect gate="G$1" pin="DB4" pad="P$11"/>
<connect gate="G$1" pin="DB5" pad="P$12"/>
<connect gate="G$1" pin="DB6" pad="P$13"/>
<connect gate="G$1" pin="DB7" pad="P$14"/>
<connect gate="G$1" pin="E" pad="P$6"/>
<connect gate="G$1" pin="K" pad="P$16"/>
<connect gate="G$1" pin="R/W" pad="P$5"/>
<connect gate="G$1" pin="RS" pad="P$4"/>
<connect gate="G$1" pin="VDD" pad="P$2"/>
<connect gate="G$1" pin="VO" pad="P$3"/>
<connect gate="G$1" pin="VSS" pad="P$1"/>
</connects>
<technologies>
<technology name="4-HOLES"/>
</technologies>
</device>
<device name="20X4-3HOLES" package="LCD-20X4-3HOLES">
<connects>
<connect gate="G$1" pin="A" pad="P$15"/>
<connect gate="G$1" pin="DB0" pad="P$7"/>
<connect gate="G$1" pin="DB1" pad="P$8"/>
<connect gate="G$1" pin="DB2" pad="P$9"/>
<connect gate="G$1" pin="DB3" pad="P$10"/>
<connect gate="G$1" pin="DB4" pad="P$11"/>
<connect gate="G$1" pin="DB5" pad="P$12"/>
<connect gate="G$1" pin="DB6" pad="P$13"/>
<connect gate="G$1" pin="DB7" pad="P$14"/>
<connect gate="G$1" pin="E" pad="P$6"/>
<connect gate="G$1" pin="K" pad="P$16"/>
<connect gate="G$1" pin="R/W" pad="P$5"/>
<connect gate="G$1" pin="RS" pad="P$4"/>
<connect gate="G$1" pin="VDD" pad="P$2"/>
<connect gate="G$1" pin="VO" pad="P$3"/>
<connect gate="G$1" pin="VSS" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="16X2-4-HOLES-BL" package="LCD-16X2-4HOLES-BL">
<connects>
<connect gate="G$1" pin="A" pad="15"/>
<connect gate="G$1" pin="DB0" pad="7"/>
<connect gate="G$1" pin="DB1" pad="8"/>
<connect gate="G$1" pin="DB2" pad="9"/>
<connect gate="G$1" pin="DB3" pad="10"/>
<connect gate="G$1" pin="DB4" pad="11"/>
<connect gate="G$1" pin="DB5" pad="12"/>
<connect gate="G$1" pin="DB6" pad="13"/>
<connect gate="G$1" pin="DB7" pad="14"/>
<connect gate="G$1" pin="E" pad="6"/>
<connect gate="G$1" pin="K" pad="16"/>
<connect gate="G$1" pin="R/W" pad="5"/>
<connect gate="G$1" pin="RS" pad="4"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VO" pad="3"/>
<connect gate="G$1" pin="VSS" pad="1"/>
</connects>
<technologies>
<technology name="16X2-4-HOLES-BL"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="BOT_ICSP" library="dp_devices" deviceset="CON_HEADER_PRG_PIC_ICSP" device="0.1INCH_B" value="CON_HEADER_PRG_PIC_ICSPB"/>
<part name="GND9" library="TOSLINK_switcher" deviceset="GND" device=""/>
<part name="+5V11" library="TOSLINK_switcher" deviceset="+5V" device=""/>
<part name="EXP-IO" library="TOSLINK_switcher" deviceset="EXT-IO_FEMALE" device=""/>
<part name="+5V4" library="TOSLINK_switcher" deviceset="+5V" device=""/>
<part name="GND2" library="TOSLINK_switcher" deviceset="GND" device=""/>
<part name="SUP1" library="dp_devices" deviceset="CON_HEADER_1X02" device="-PTH"/>
<part name="FRAME2" library="frames" deviceset="A4L-LOC" device=""/>
<part name="LCD" library="HD44780LCD" deviceset="LCD-HD44780" device="16X2-4-HOLES" technology="4-HOLES"/>
<part name="AUTO/MAN" library="TOSLINK_switcher" deviceset="CAP_BUT_10MM" device=""/>
<part name="CHANNEL" library="TOSLINK_switcher" deviceset="CAP_BUT_10MM" device=""/>
<part name="BACKLIGHT" library="TOSLINK_switcher" deviceset="CAP_BUT_10MM" device=""/>
<part name="SLAVE" library="TOSLINK_switcher" deviceset="PIC_ICSP_RIGHT_ANGLE" device=""/>
<part name="MASTER" library="TOSLINK_switcher" deviceset="PIC_ICSP_RIGHT_ANGLE" device=""/>
<part name="+5V1" library="TOSLINK_switcher" deviceset="+5V" device=""/>
<part name="GND1" library="TOSLINK_switcher" deviceset="GND" device=""/>
<part name="U1" library="TOSLINK_switcher" deviceset="PIC16F1503-SO" device=""/>
<part name="+5V2" library="TOSLINK_switcher" deviceset="+5V" device=""/>
<part name="GND3" library="TOSLINK_switcher" deviceset="GND" device=""/>
<part name="GND4" library="TOSLINK_switcher" deviceset="GND" device=""/>
<part name="R5" library="dp_devices" deviceset="RESISTOR" device="-1206" value="4R7"/>
<part name="R4" library="TOSLINK_switcher" deviceset="TRIM_POT_TOP" device=""/>
<part name="GND5" library="TOSLINK_switcher" deviceset="GND" device=""/>
<part name="+5V3" library="TOSLINK_switcher" deviceset="+5V" device=""/>
<part name="+5V5" library="TOSLINK_switcher" deviceset="+5V" device=""/>
<part name="U2" library="TOSLINK_switcher" deviceset="TRANSISTOR_BJT_NPN" device=""/>
<part name="R3" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="4K7"/>
<part name="GND6" library="TOSLINK_switcher" deviceset="GND" device=""/>
<part name="C1" library="dp_devices" deviceset="CAPACITOR_NPOL" device="-0805" value="0.1uF"/>
<part name="GND7" library="TOSLINK_switcher" deviceset="GND" device=""/>
<part name="R1" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="4K7"/>
<part name="R2" library="dp_devices" deviceset="RESISTOR" device="-0805" technology="R805" value="4K7"/>
<part name="+5V6" library="TOSLINK_switcher" deviceset="+5V" device=""/>
<part name="J3" library="TOSLINK_switcher" deviceset="1X1_SMD_PAD" device=""/>
<part name="J2" library="TOSLINK_switcher" deviceset="1X1_SMD_PAD" device=""/>
<part name="J1" library="TOSLINK_switcher" deviceset="1X1_SMD_PAD" device=""/>
<part name="GND8" library="TOSLINK_switcher" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<description>MCU</description>
<plain>
<text x="-33.02" y="-22.86" size="1.778" layer="91">For expansion board 
mounting support only</text>
</plain>
<instances>
<instance part="BOT_ICSP" gate="J" x="27.94" y="-12.7"/>
<instance part="GND9" gate="G$1" x="2.54" y="-12.7" smashed="yes"/>
<instance part="+5V11" gate="G$1" x="2.54" y="-7.62" smashed="yes">
<attribute name="VALUE" x="0" y="-5.08" size="1.778" layer="96"/>
</instance>
<instance part="EXP-IO" gate="G$1" x="101.6" y="81.28" rot="MR0"/>
<instance part="+5V4" gate="G$1" x="116.84" y="88.9" smashed="yes" rot="MR0">
<attribute name="VALUE" x="119.38" y="91.44" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND2" gate="G$1" x="83.82" y="86.36" smashed="yes" rot="MR0"/>
<instance part="SUP1" gate="J" x="-40.64" y="-17.78"/>
<instance part="FRAME2" gate="G$1" x="-50.8" y="-33.02"/>
<instance part="LCD" gate="G$1" x="170.18" y="96.52" rot="R90"/>
<instance part="AUTO/MAN" gate="G$1" x="55.88" y="50.8"/>
<instance part="CHANNEL" gate="G$1" x="10.16" y="50.8"/>
<instance part="BACKLIGHT" gate="G$1" x="-30.48" y="50.8"/>
<instance part="SLAVE" gate="G$1" x="-38.1" y="83.82" rot="MR0"/>
<instance part="MASTER" gate="G$1" x="63.5" y="-12.7"/>
<instance part="+5V1" gate="G$1" x="43.18" y="-7.62" smashed="yes">
<attribute name="VALUE" x="40.64" y="-5.08" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="G$1" x="43.18" y="-12.7" smashed="yes"/>
<instance part="U1" gate="G$1" x="15.24" y="83.82"/>
<instance part="+5V2" gate="G$1" x="5.08" y="114.3" smashed="yes">
<attribute name="VALUE" x="7.62" y="114.3" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="G$1" x="7.62" y="68.58" smashed="yes"/>
<instance part="GND4" gate="G$1" x="149.86" y="76.2" smashed="yes" rot="MR0"/>
<instance part="R5" gate="G$1" x="185.42" y="73.66" rot="R90"/>
<instance part="R4" gate="G$1" x="154.94" y="60.96" rot="MR90"/>
<instance part="GND5" gate="G$1" x="160.02" y="55.88" smashed="yes" rot="MR0"/>
<instance part="+5V3" gate="G$1" x="137.16" y="71.12" smashed="yes" rot="MR0">
<attribute name="VALUE" x="139.7" y="73.66" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="+5V5" gate="G$1" x="175.26" y="71.12" smashed="yes" rot="MR0">
<attribute name="VALUE" x="177.8" y="73.66" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="U2" gate="G$1" x="185.42" y="45.72"/>
<instance part="R3" gate="G$1" x="177.8" y="45.72"/>
<instance part="GND6" gate="G$1" x="187.96" y="38.1" smashed="yes" rot="MR0"/>
<instance part="C1" gate="C" x="10.16" y="104.14"/>
<instance part="GND7" gate="G$1" x="10.16" y="101.6" smashed="yes" rot="MR0"/>
<instance part="R1" gate="G$1" x="66.04" y="83.82" rot="R90"/>
<instance part="R2" gate="G$1" x="73.66" y="81.28" rot="R90"/>
<instance part="+5V6" gate="G$1" x="73.66" y="91.44" smashed="yes" rot="MR0">
<attribute name="VALUE" x="76.2" y="93.98" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="J3" gate="G$1" x="88.9" y="-2.54" rot="R180"/>
<instance part="J2" gate="G$1" x="88.9" y="-10.16" rot="R180"/>
<instance part="J1" gate="G$1" x="88.9" y="-17.78" rot="R180"/>
<instance part="GND8" gate="G$1" x="96.52" y="-17.78" smashed="yes" rot="MR0"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="BOT_ICSP" gate="J" pin="GND"/>
<pinref part="GND9" gate="G$1" pin="GND"/>
<wire x1="25.4" y1="-12.7" x2="2.54" y2="-12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="EXP-IO" gate="G$1" pin="2"/>
<pinref part="GND2" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="86.36" x2="83.82" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="GND"/>
<pinref part="GND1" gate="G$1" pin="GND"/>
<wire x1="58.42" y1="-12.7" x2="43.18" y2="-12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VSS"/>
<pinref part="GND3" gate="G$1" pin="GND"/>
<pinref part="SLAVE" gate="G$1" pin="GND"/>
<wire x1="-33.02" y1="83.82" x2="-27.94" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="83.82" x2="-27.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="68.58" x2="5.08" y2="68.58" width="0.1524" layer="91"/>
<junction x="5.08" y="68.58"/>
<wire x1="5.08" y1="68.58" x2="7.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LCD" gate="G$1" pin="VSS"/>
<pinref part="GND4" gate="G$1" pin="GND"/>
<wire x1="149.86" y1="76.2" x2="149.86" y2="78.74" width="0.1524" layer="91"/>
<pinref part="LCD" gate="G$1" pin="R/W"/>
<wire x1="149.86" y1="78.74" x2="149.86" y2="86.36" width="0.1524" layer="91"/>
<wire x1="160.02" y1="86.36" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<wire x1="160.02" y1="78.74" x2="149.86" y2="78.74" width="0.1524" layer="91"/>
<junction x="149.86" y="78.74"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="A"/>
<pinref part="GND5" gate="G$1" pin="GND"/>
<wire x1="160.02" y1="60.96" x2="160.02" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="E"/>
<pinref part="GND6" gate="G$1" pin="GND"/>
<wire x1="187.96" y1="38.1" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="C" pin="2"/>
<pinref part="GND7" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="88.9" y1="-17.78" x2="96.52" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="GND8" gate="G$1" pin="GND"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="BOT_ICSP" gate="J" pin="+V"/>
<pinref part="+5V11" gate="G$1" pin="+5V"/>
<wire x1="25.4" y1="-10.16" x2="2.54" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+5V4" gate="G$1" pin="+5V"/>
<wire x1="116.84" y1="88.9" x2="116.84" y2="86.36" width="0.1524" layer="91"/>
<pinref part="EXP-IO" gate="G$1" pin="1"/>
<wire x1="116.84" y1="86.36" x2="104.14" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="+V"/>
<pinref part="+5V1" gate="G$1" pin="+5V"/>
<wire x1="58.42" y1="-10.16" x2="43.18" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-10.16" x2="43.18" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="+V"/>
<wire x1="-33.02" y1="86.36" x2="-27.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="86.36" x2="-27.94" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="-27.94" y1="96.52" x2="5.08" y2="96.52" width="0.1524" layer="91"/>
<pinref part="+5V2" gate="G$1" pin="+5V"/>
<junction x="5.08" y="96.52"/>
<wire x1="5.08" y1="96.52" x2="5.08" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C1" gate="C" pin="1"/>
<wire x1="5.08" y1="109.22" x2="5.08" y2="114.3" width="0.1524" layer="91"/>
<wire x1="5.08" y1="109.22" x2="10.16" y2="109.22" width="0.1524" layer="91"/>
<junction x="5.08" y="109.22"/>
</segment>
<segment>
<pinref part="LCD" gate="G$1" pin="VDD"/>
<wire x1="152.4" y1="86.36" x2="152.4" y2="66.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="66.04" x2="149.86" y2="66.04" width="0.1524" layer="91"/>
<wire x1="149.86" y1="66.04" x2="137.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="137.16" y1="66.04" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="E"/>
<wire x1="149.86" y1="60.96" x2="149.86" y2="66.04" width="0.1524" layer="91"/>
<junction x="149.86" y="66.04"/>
<pinref part="+5V3" gate="G$1" pin="+5V"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="+5V5" gate="G$1" pin="+5V"/>
<wire x1="185.42" y1="68.58" x2="175.26" y2="68.58" width="0.1524" layer="91"/>
<wire x1="175.26" y1="68.58" x2="175.26" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="+5V6" gate="G$1" pin="+5V"/>
<wire x1="73.66" y1="86.36" x2="73.66" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="73.66" y1="88.9" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="66.04" y1="88.9" x2="73.66" y2="88.9" width="0.1524" layer="91"/>
<junction x="73.66" y="88.9"/>
</segment>
</net>
<net name="MCLR" class="0">
<segment>
<pinref part="BOT_ICSP" gate="J" pin="/MCLR"/>
<wire x1="15.24" y1="-7.62" x2="25.4" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-7.62" x2="15.24" y2="0" width="0.1524" layer="91"/>
<wire x1="15.24" y1="0" x2="50.8" y2="0" width="0.1524" layer="91"/>
<wire x1="50.8" y1="0" x2="50.8" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="MASTER" gate="G$1" pin="/MCLR"/>
<wire x1="50.8" y1="-7.62" x2="58.42" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BUT_1" class="0">
<segment>
<pinref part="EXP-IO" gate="G$1" pin="5"/>
<wire x1="104.14" y1="81.28" x2="111.76" y2="81.28" width="0.1524" layer="91"/>
<label x="114.3" y="81.28" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="BUT_2" class="0">
<segment>
<pinref part="EXP-IO" gate="G$1" pin="7"/>
<wire x1="104.14" y1="78.74" x2="111.76" y2="78.74" width="0.1524" layer="91"/>
<label x="114.3" y="78.74" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="EXP-IO" gate="G$1" pin="6"/>
<wire x1="88.9" y1="81.28" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
<label x="88.9" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="88.9" y1="-10.16" x2="96.52" y2="-10.16" width="0.1524" layer="91"/>
<label x="91.44" y="-10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="EXP-IO" gate="G$1" pin="4"/>
<wire x1="88.9" y1="83.82" x2="96.52" y2="83.82" width="0.1524" layer="91"/>
<label x="88.9" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="88.9" y1="-2.54" x2="96.52" y2="-2.54" width="0.1524" layer="91"/>
<label x="91.44" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="PGD/A0" class="0">
<segment>
<pinref part="BOT_ICSP" gate="J" pin="PGD"/>
<wire x1="17.78" y1="-15.24" x2="25.4" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-15.24" x2="17.78" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-25.4" x2="50.8" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-25.4" x2="50.8" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="MASTER" gate="G$1" pin="PGD"/>
<wire x1="50.8" y1="-15.24" x2="58.42" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PGC/A1" class="0">
<segment>
<pinref part="BOT_ICSP" gate="J" pin="PGC"/>
<wire x1="20.32" y1="-17.78" x2="25.4" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-17.78" x2="20.32" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-22.86" x2="53.34" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-22.86" x2="53.34" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="MASTER" gate="G$1" pin="PGC"/>
<wire x1="53.34" y1="-17.78" x2="58.42" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LCD_PWM" class="0">
<segment>
<pinref part="EXP-IO" gate="G$1" pin="9"/>
<wire x1="121.92" y1="76.2" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
<label x="106.68" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="172.72" y1="45.72" x2="167.64" y2="45.72" width="0.1524" layer="91"/>
<label x="160.02" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCU_RST" class="0">
<segment>
<pinref part="EXP-IO" gate="G$1" pin="3"/>
<wire x1="104.14" y1="83.82" x2="111.76" y2="83.82" width="0.1524" layer="91"/>
<label x="119.38" y="83.82" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VPP/MCLR/RA3"/>
<pinref part="SLAVE" gate="G$1" pin="/MCLR"/>
<wire x1="-12.7" y1="88.9" x2="-33.02" y2="88.9" width="0.1524" layer="91"/>
<label x="-25.4" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="EXP-IO" gate="G$1" pin="8"/>
<pinref part="U1" gate="G$1" pin="SDA/RC1/AN5"/>
<wire x1="96.52" y1="78.74" x2="66.04" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="66.04" y1="78.74" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<junction x="66.04" y="78.74"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCL/RC0/AN4"/>
<pinref part="EXP-IO" gate="G$1" pin="10"/>
<wire x1="40.64" y1="76.2" x2="73.66" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="73.66" y1="76.2" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<junction x="73.66" y="76.2"/>
</segment>
</net>
<net name="BL_BUT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="C1OUT/T0CKI/INT/AN2/RA2"/>
<wire x1="-12.7" y1="81.28" x2="-20.32" y2="81.28" width="0.1524" layer="91"/>
<label x="-22.86" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BACKLIGHT" gate="G$1" pin="BUTTON"/>
<wire x1="-15.24" y1="50.8" x2="-10.16" y2="50.8" width="0.1524" layer="91"/>
<label x="-12.7" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="CHAN_BUT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RC2/AN6"/>
<wire x1="40.64" y1="81.28" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<label x="40.64" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CHANNEL" gate="G$1" pin="BUTTON"/>
<wire x1="25.4" y1="50.8" x2="33.02" y2="50.8" width="0.1524" layer="91"/>
<label x="27.94" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="AUTO/MAN_BUT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM2/RC3/AN7"/>
<wire x1="40.64" y1="83.82" x2="48.26" y2="83.82" width="0.1524" layer="91"/>
<label x="40.64" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="AUTO/MAN" gate="G$1" pin="BUTTON"/>
<wire x1="71.12" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<label x="73.66" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LCD" gate="G$1" pin="A"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="185.42" y1="78.74" x2="185.42" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="LCD" gate="G$1" pin="VO"/>
<wire x1="154.94" y1="86.36" x2="154.94" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="S"/>
</segment>
</net>
<net name="RS" class="0">
<segment>
<pinref part="LCD" gate="G$1" pin="RS"/>
<wire x1="157.48" y1="86.36" x2="157.48" y2="81.28" width="0.1524" layer="91"/>
<label x="157.48" y="81.28" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="C1IN+/ICSPDAT/AN0/RA0"/>
<wire x1="-12.7" y1="76.2" x2="-25.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="76.2" x2="-25.4" y2="81.28" width="0.1524" layer="91"/>
<pinref part="SLAVE" gate="G$1" pin="PGD"/>
<wire x1="-25.4" y1="81.28" x2="-33.02" y2="81.28" width="0.1524" layer="91"/>
<label x="-20.32" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="EN" class="0">
<segment>
<pinref part="LCD" gate="G$1" pin="E"/>
<wire x1="162.56" y1="86.36" x2="162.56" y2="81.28" width="0.1524" layer="91"/>
<label x="162.56" y="81.28" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SLAVE" gate="G$1" pin="PGC"/>
<pinref part="U1" gate="G$1" pin="C1IN0-/VREF/ICSPCLK/AN1/RA1"/>
<wire x1="-33.02" y1="78.74" x2="-12.7" y2="78.74" width="0.1524" layer="91"/>
<label x="-20.32" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="LCD" gate="G$1" pin="K"/>
<pinref part="U2" gate="G$1" pin="C"/>
<wire x1="187.96" y1="86.36" x2="187.96" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="B"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="OSC1/CLKIN/T1CKI/RA5"/>
<wire x1="-12.7" y1="86.36" x2="-20.32" y2="86.36" width="0.1524" layer="91"/>
<label x="-20.32" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LCD" gate="G$1" pin="DB4"/>
<wire x1="175.26" y1="86.36" x2="175.26" y2="81.28" width="0.1524" layer="91"/>
<label x="175.26" y="81.28" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="T1G/OSC2/CLKOUT/AN3/RA4"/>
<wire x1="-12.7" y1="83.82" x2="-20.32" y2="83.82" width="0.1524" layer="91"/>
<label x="-20.32" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LCD" gate="G$1" pin="DB5"/>
<wire x1="177.8" y1="86.36" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
<label x="177.8" y="81.28" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PWM1/RC5"/>
<wire x1="40.64" y1="88.9" x2="48.26" y2="88.9" width="0.1524" layer="91"/>
<label x="43.18" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LCD" gate="G$1" pin="DB6"/>
<wire x1="180.34" y1="86.36" x2="180.34" y2="81.28" width="0.1524" layer="91"/>
<label x="180.34" y="81.28" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RC4"/>
<wire x1="40.64" y1="86.36" x2="48.26" y2="86.36" width="0.1524" layer="91"/>
<label x="43.18" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LCD" gate="G$1" pin="DB7"/>
<wire x1="182.88" y1="86.36" x2="182.88" y2="81.28" width="0.1524" layer="91"/>
<label x="182.88" y="81.28" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
